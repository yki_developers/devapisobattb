<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
//$route['api/example/users/(:num)'] = 'api/example/users/id/$1'; // Example 4
//$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; // Example 8
/*$route['api/pasien'] = 'getdatapasien/pasien';
$route['api/fase'] = 'getdatapasien/fasepengobatan';
$route['api/auth'] = 'auth/token';
$route['api/pasienAll'] = 'getdatapasien/pasienAll';
$route['api/absensi/(:any)'] = 'getdatapasien/absensi/$1';
$route['api/listpasien']['POST'] = 'getpasien/listpasien';
$route['api/labid'] = 'unittb/laboratory';
$route['api/xpert']['POST'] = 'unittb/exam';
$route['api/xpert']['PUT'] = 'unittb/exam';
$route['api/faskes'] = 'faskes/datafaskes';
$route['api/lab/(:any)'] = 'unittb/labid/$1';
*/
$route['api/users/login']['POST'] = 'login/auth';
$route['api/users/token']['GET'] = 'login/token';
$route['api/users/getpassword']['POST'] = 'login/getpassword';
$route['api/users/register']['POST'] = 'login/selfregistration';
$route['api/users/updateprofile']['PUT'] = 'login/updateprofile';
$route['api/users/chpassword']['PUT'] = 'login/changepassword';
$route['api/users/chemail']['PUT'] = 'login/changeemail';
$route['api/users/chfoto']['PUT'] = 'login/fotoprofile';
$route['api/users/chbio/(:any)']['PUT'] = 'login/profile/$1';
$route['api/users/username']['POST'] = "login/usernamevalidation";
$route['api/users/email']['POST'] = "login/emailvalidation";
$route['api/users/nohp']['POST'] = "login/nohpvalidation";
$route['api/users/profile']['POST'] = 'login/profile';
$route['api/users/faskes/(:any)']['GET'] = 'login/faskesdomisili/$1';
$route['api/users/otpcheck']['POST'] = 'login/otp';
$route['api/users/resendotp']['POST'] = 'login/resend';

$route['api/users/reset/(:any)']['GET'] = 'login/getuser/$1';
$route['api/users/playerid/remove']['PUT'] = 'login/remove';
$route['api/users/playerid/update']['PUT'] = 'login/pid';

$route['api/fasyankes/list/(:any)/(:any)/(:any)/(:any)']['GET']= 'fasyankes/fasyankes/$1/$2/$3/$4';
$route['api/fasyankes/detail/(:any)']['GET']= 'fasyankes/detailFasyankes/$1';
$route['api/fasyankes/add']['POST'] = 'fasyankes/fasyankes';
$route['api/fasyankes/nearme']['POST'] = 'fasyankes/nearme';
$route['api/fasyankes/search']['POST'] = 'fasyankes/search';

$route['api/fasyankes/edit/(:any)']['PUT'] = 'fasyankes/fasyankes/$1';
$route['api/fasyankes/del/(:any)']['DELETE']='fasyankes/fasyankes/$1';

$route['api/fasyankes/update/(:any)']['PUT'] = 'fasyankes/profile/$1';





$route['api/propinsi/add']['POST']='mpropinsi/index';
$route['api/propinsi/list']['GET'] = 'mpropinsi/index';
$route['api/propinsi/search']['POST'] = 'mpropinsi/search';

$route['api/propinsi/edit/(:any)']['PUT'] = 'mpropinsi/index/$1';
$route['api/propinsi/del/(:any)']['DELETE'] = 'mpropinsi/index/$1';
$route['api/propinsi/detail/(:any)']['GET'] = 'mpropinsi/detail/$1';


$route['api/kabupaten/add']['POST'] = 'mkabupaten/index';
$route['api/kabupaten/list/(:any)']['GET']= 'mkabupaten/index/$1';
$route['api/kabupaten/edit/(:any)']['PUT']= 'mkabupaten/index/$1';
$route['api/kabupaten/del/(:any)']['DELETE']= 'mkabupaten/index/$1';
$route['api/kabupaten/detail/(:any)']['GET']= 'mkabupaten/detail/$1';
$route['api/kabupaten/listall/(:any)/(:any)']['GET']= 'mkabupaten/listall/$1/$2';
$route['api/kabupaten/search']['POST']= 'mkabupaten/search';

$route['api/kecamatan/add']['POST']='mkecamatan/index';
$route['api/kecamatan/list/(:any)']['GET']='mkecamatan/index/$1';
$route['api/kecamatan/edit/(:any)']['PUT']='mkecamatan/index/$1';
$route['api/kecamatan/del/(:any)']['DELETE']='mkecamatan/index/$1';
$route['api/kecamatan/detail/(:any)']['GET']='mkecamatan/detail/$1';
$route['api/kecamatan/listall/(:any)/(:any)']['GET']='mkecamatan/listall/$1/$2';
$route['api/kecamatan/search']['POST']= 'mkecamatan/search';


$route['api/kelurahan/add']['POST']='mkelurahan/index';
$route['api/kelurahan/list/(:any)']['GET']='mkelurahan/index/$1';
$route['api/kelurahan/edit/(:any)']['PUT']='mkelurahan/index/$1';
$route['api/kelurahan/del/(:any)']['DELETE']='mkelurahan/index/$1';
$route['api/kelurahan/detail/(:any)']['GET']='mkelurahan/detail/$1';
$route['api/kelurahan/listall/(:any)/(:any)']['GET']='mkelurahan/listall/$1/$2';
$route['api/kelurahan/search']['POST']= 'mkelurahan/search';


$route['api/infotb/kategori/add']['POST'] = "infotb/kategori";
$route['api/infotb/kategori/list']['GET'] = "infotb/kategori";
$route['api/infotb/kategori/edit/(:any)']['PUT'] = "infotb/kategori/$1";
$route['api/infotb/kategori/del/(:any)']['DELETE'] = "infotb/kategori/$1";
$route['api/infotb/kategori/detail/(:any)']['GET'] = "infotb/infokategori/$1";

$route['api/infotb/add']['POST'] = "infotb/index";
$route['api/infotb/list/(:any)/(:any)/(:any)']['GET'] = "infotb/index/$1/$2/$3";
$route['api/infotb/edit/(:any)']['PUT'] = "infotb/index/$1";
$route['api/infotb/del/(:any)']['DELETE'] = "infotb/index/$1";
$route['api/infotb/detail/(:any)']['GET'] = "infotb/detail/$1";
$route['api/infotb/search']['POST'] = "infotb/search";

$route['api/organisasi/add']['POST']="Morganisasi/index";
$route['api/organisasi/list/(:any)/(:any)/(:any)']['GET']="Morganisasi/index/$1/$2/$3";
$route['api/organisasi/edit/(:any)']['PUT']="Morganisasi/index/$1";
$route['api/organisasi/del/(:any)']['DELETE']="Morganisasi/index/$1";
$route['api/organisasi/detail/(:any)']['GET']="Morganisasi/detail/$1";

$route['api/forum/kategori/add']['POST']="forum/kategori";
$route['api/forum/kategori/list']['GET']="forum/kategori";
$route['api/forum/kategori/edit/(:any)']['PUT']="forum/kategori/$1";
$route['api/forum/kategori/del/(:any)']['DELETE']="forum/kategori/$1";
$route['api/forum/kategori/detail/(:any)']['GET']="forum/detailkategori/$1";

//$route['api/forum/follow']['POST'] = ''

$route['api/forum/add']['POST']="forum/index";
$route['api/forum/list/(:any)/(:any)']['GET']="forum/index/$1/$2";
$route['api/forum/edit/(:any)']['PUT']="forum/index/$1";
$route['api/forum/del/(:any)']['DELETE']="forum/index/$1";
$route['api/forum/detail/(:any)']['GET']="forum/detail/$1";
$route['api/forum/total/(:any)']['GET']="forum/totalthread/$1";
$route['api/forum/userthread/(:any)']['GET']="forum/mythread/$1";

$route['api/forum/org/list/bydate/(:any)/(:any)/(:any)']['GET']="forum/orglistbydate/$1/$2/$3";
$route['api/forum/org/list/bycomment/(:any)/(:any)/(:any)']['GET']="forum/orglistbycommet/$1/$2/$3";
$route['api/forum/org/list/allbydate/(:any)/(:any)/(:any)']['GET']="forum/orglistallbydate/$1/$2/$3";


$route['api/forum/reply/add']['POST']="forum/reply";
$route['api/forum/reply/list/(:any)/(:any)/(:any)/(:any)']['GET']="forum/reply/$1/$2/$3/$4";
$route['api/forum/reply/edit/(:any)']['PUT']="forum/reply/$1";
$route['api/forum/reply/del/(:any)']['DELETE']="forum/reply/$1";
$route['api/forum/reply/detail/(:any)']['GET']="forum/detailreply/$1";



$route['api/feedback/add']['POST'] = "feedback/index";
$route['api/feedback/list/(:any)']['GET'] = "feedback/approved/$1";
$route['api/feedback/detail/(:any)']['GET'] = "feedback/detail/$1";

$route['api/feedback/listall/(:any)/(:any)']['GET'] = "feedback/index/$1/$2";
$route['api/feedback/edit/(:any)']['PUT'] = "feedback/index/$1";
$route['api/feedback/del/(:any)']['DELETE'] = "feedback/index/$1";
$route['api/feedback/bintang/(:any)']['GET'] = "feedback/totalbintang/$1";
$route['api/feedback/approve/(:any)']['PUT'] = "feedback/approval/$1";
$route['api/feedback/perfaskes/(:any)']['GET'] = "feedback/listperfaskes/$1";

$route['api/feedback/reply/add']['POST'] = "feedback/reply";
$route['api/feedback/reply/list/(:any)']['GET'] = "feedback/reply/$1";
$route['api/feedback/reply/del/(:any)']['DELETE'] = "feedback/reply/$1";

$route['api/bookmark/artikel/add']['POST'] = "bookmark/index";
$route['api/bookmark/artikel/list/(:any)']['GET'] = "bookmark/index/$1";
$route['api/bookmark/artikel/listfull/(:any)']['GET'] = "bookmark/listfull/$1";
$route['api/bookmark/artikel/del/(:any)/(:any)']['DELETE'] = "bookmark/index/$1/$2";


$route['api/bookmark/forum/add']['POST'] = "bookmark/add";
$route['api/bookmark/forum/list/(:any)/(:any)']['GET'] = "bookmark/list/$1/$2";
$route['api/bookmark/forum/del/(:any)/(:any)']['DELETE'] = "bookmark/del/$1/$2";
$route['api/bookmark/forum/listall/(:any)/(:any)']['GET'] = "bookmark/threadlist/$1/$2";
$route['api/bookmark/forum/listbyuser/(:any)']['GET'] = "bookmark/listbyuser/$1";

$route['api/follow/org/add']['POST'] = "follow/index";
$route['api/follow/org/list/(:any)']['GET'] = "follow/index/$1";
$route['api/follow/org/status/(:any)/(:any)']['GET'] = "follow/followstatus/$1/$2";
$route['api/follow/org/del/(:any)/(:any)']['DELETE'] = "follow/index/$1/$2";
$route['api/follow/org/follower/(:any)']['GET'] = "follow/totalfollower/$1";
$route['api/follow/org/following/(:any)']['GET'] = "follow/user/$1";

$route['api/admin/xkey/auth']['GET'] = "admin/auth/index";
$route['api/admin/xkey/add']['POST'] = "admin/auth/add";
$route['api/admin/xkey/list']['GET'] = "admin/auth/list";
$route['api/admin/xkey/del/(:any)']['DELETE'] = "admin/auth/index/$1";

$route['api/admin/login']['POST'] = "admin/login";
$route['api/admin/users/add']['POST'] = 'admin/users/index';
$route['api/admin/users/edit']['PUT'] = 'admin/users/index';
$route['api/admin/users/list/(:any)/(:any)']['GET'] = 'admin/users/index/$1/$2';
$route['api/admin/users/del/(:any)']['DELETE'] = 'admin/users/index/$1';
$route['api/admin/users/detail/(:any)']['GET'] = 'admin/users/detail/$1';
$route['api/admin/users/admingroup']['GET'] = 'admin/users/admingroup';
$route['api/admin/users/adduser']['POST'] = 'admin/users/adduseradmin';
$route['api/admin/users/update/(:any)']['PUT'] = 'admin/users/updateuseradmin/$1';
$route['api/admin/users/chpass/(:any)']['PUT'] = 'admin/users/chpassword/$1';





$route['api/admin/users/org/list/(:any)']['GET'] = 'admin/users/orglist/$1';
$route['api/admin/users/fasyankes/list/(:any)']['GET'] = 'admin/users/fasyankeslist/$1';

$route['api/admin/forum/list/(:any)']['GET'] = 'admin/forum/index/$1';
$route['api/admin/forum/approved/(:any)']['PUT'] = 'admin/forum/index/$1';
$route['api/admin/forum/reply/(:any)']['GET'] = 'admin/forum/reply/$1';

$route['api/admin/faskes/search']['POST'] = 'admin/fasyankes/search';
$route['api/admin/faskes/detail/(:any)']['GET'] = 'admin/fasyankes/detail/$1';
$route['api/admin/faskes/update/(:any)']['PUT'] = 'admin/fasyankes/index/$1';
$route['api/admin/faskes/list/(:any)']['GET'] = 'admin/fasyankes/index/$1';
$route['api/fasyankes/listall/(:any)/(:any)']['GET'] = 'fasyankes/listall/$1/$2';

$route['api/admin/faskes/kecamatan/(:any)']['GET'] = 'admin/fasyankes/perkecamatan/$1';

$route['api/admin/faskes/filter']['POST'] = 'admin/fasyankes/faskeslist';


$route['api/admin/users/adminfaskes/(:any)/(:any)/(:num)/(:num)']['GET'] = 'admin/users/adminfasyankes/$1/$2/$3/$4';
$route['api/admin/users/adminkomunitas/(:any)/(:any)']['GET'] = 'admin/users/adminkomunitas/$1/$2';
$route['api/admin/users/dinkesprop/(:any)/(:num)/(:num)']['GET'] = 'admin/users/dinkesprop/$1/$2/$3';
$route['api/admin/users/dinkeskab/(:any)/(:num)/(:num)']['GET'] = 'admin/users/dinkeskab/$1/$2/$3';



$route['api/notification/add']['POST'] = 'notification/index';
$route['api/notification/list/(:any)']['GET'] = 'notification/index/$1';
$route['api/notification/update/(:any)']['PUT'] = 'notification/index/$1';
$route['api/notification/list/(:any)']['POST'] = 'notification/notifikasi/$1';

$route['api/masterdata/jenisfaskes']['GET'] = 'admin/masterdata/jnfaskes';
$route['api/masterdata/addjenis']['POST'] = 'admin/masterdata/jnsfaskes';
$route['api/masterdata/deljenis/(:any)']['delete'] = 'admin/masterdata/jnsfaskes/$1';

$route['api/masterdata/kepemilikan']['GET'] = 'admin/masterdata/kepemilikan';
$route['api/masterdata/addkepemilikan']['POST'] = 'admin/masterdata/kepemilikan';
$route['api/masterdata/delkepemilikan/(:any)']['DELETE'] = 'admin/masterdata/kepemilikan/$1';

$route['api/about']['GET'] = 'about/index';

$route['api/admin/organisasi/add']['POST'] = "admin/organisasi/index";
$route['api/admin/organisasi/list']['GET'] = "admin/organisasi/index";
$route['api/admin/organisasi/edit/(:any)']['PUT'] = "admin/organisasi/index/$1";
$route['api/admin/organisasi/del/(:any)']['DELETE'] = "admin/organisasi/index/$1";
$route['api/admin/organisasi/detail/(:any)']['GET'] = "admin/organisasi/detail/$1";
$route['api/admin/organisasi/search']['POST'] = "admin/organisasi/search";


$route['api/screening/new']['POST'] = "screening/index";
$route['api/screening/update/(:any)']['PUT'] = "screening/index/$1";
$route['api/screening/last/(:any)']['GET'] = "screening/getlast/$1";

$route['api/screening/list/(:any)']['GET'] = "screening/index/$1";

$route['api/screening/hasil/faskes/(:any)']['GET'] = "screening/hasilfaskes/$1";
$route['api/screening/hasil/kabupaten/(:any)']['GET'] = "screening/hasilkab/$1";
$route['api/screening/hasil/propinsi/(:any)']['GET'] = "screening/hasilprop/$1";
$route['api/screening/hasil/filter']['POST'] = 'screening/filter';

$route['api/appointment/add']['POST'] = "appointment/index";
$route['api/appointment/update/(:any)']['PUT'] = "appointment/index/$1";
$route['api/appointment/list/faskes/(:any)']['GET'] = "appointment/index/$1";
$route['api/appointment/list/kab/(:any)']['GET'] = "appointment/kabupaten/$1";
$route['api/appointment/list/prop/(:any)']['GET'] = "appointment/propinsi/$1";
$route['api/appointment/list/user/(:any)']['GET'] = "appointment/userlist/$1";
$route['api/appointment/filter']['POST'] = "appointment/filter";


$route['api/openforum/add']['POST'] = 'openforum/index';
$route['api/openforum/update/(:any)']['PUT'] = 'openforum/index/$1';
$route['api/openforum/list']['GET'] = 'openforum/index';
$route['api/openforum/mythread/(:any)']['GET'] = 'openforum/mythread/$1';
$route['api/openforum/del/(:any)']['DELETE'] = 'openforum/index/$1';
$route['api/openforum/detail/(:any)']['GET'] = 'openforum/detail/$1';

$route['api/comment/forum/add']['POST'] = 'openforum/comment';
$route['api/comment/forum/list/(:any)']['GET'] = 'openforum/comment/$1';
$route['api/comment/forum/update/(:any)']['PUT'] = 'openforum/comment/$1';
$route['api/comment/forum/del/(:any)']['DELETE'] = 'openforum/comment/$1';
$route['api/comment/forum/detail/(:any)']['GET'] = 'openforum/commentdetail/$1';
$route['api/comment/reply/list/(:any)']['GET'] = 'openforum/reply/$1';


$route['api/podcast/add']['POST'] = 'podcast/index';
$route['api/podcast/list']['GET'] = 'podcast/index';
$route['api/podcast/detail/(:any)']['GET'] = 'podcast/detail/$1';
$route['api/podcast/delete/(:any)']['DELETE'] = 'podcast/index/$1';
$route['api/podcast/update/(:any)']['PUT'] = 'podcast/index/$1';

$route['api/themes/add']['POST'] = 'podcast/themes';
$route['api/themes/list']['GET'] = 'podcast/themes';
$route['api/themes/detail/(:any)']['GET'] = 'podcast/themesdetail/$1';
$route['api/themes/del/(:any)']['DELETE'] = 'podcast/themes/$1';
$route['api/themes/update/(:any)']['PUT'] = 'podcast/themes/$1';

$route['api/podcast/episode']['GET'] = 'podcast/episodelist';

$route['api/comment/podcast/add']['POST'] = 'podcast/comment';
$route['api/comment/podcast/list/(:any)']['GET'] = 'podcast/comment/$1';
$route['api/comment/podcast/update/(:any)']['PUT'] = 'podcast/comment/$1';
$route['api/comment/podcast/del/(:any)']['DELETE'] = 'podcast/comment/$1';
$route['api/comment/podcast/detail/(:any)']['GET'] = 'podcast/commentdetail/$1';

$route['api/laporan/hasil/faskes/(:any)']['GET'] = "screening/reportfaskes/$1";
$route['api/laporan/hasil/kab/(:any)']['GET'] = "screening/reportkabupaten/$1";
$route['api/laporan/hasil/prop/(:any)']['GET'] = "screening/reportpropinsi/$1";

$route['api/admin/screening/add']['POST'] = "admin/screening/index";
$route['api/admin/screening/list']['GET'] = "admin/screening/index";
$route['api/admin/screening/del/(:any)']['DELETE'] = "admin/screening/index/$1";
$route['api/admin/screening/update/(:any)']['PUT'] = "admin/screening/index/$1";
$route['api/admin/screening/detail/(:any)']['GET'] = "admin/screening/detail/$1";

$route['api/admin/screening/getdata/(:any)']['GET'] = "admin/screening/getdata/$1";
$route['api/admin/screening/tokenlist/(:any)']['GET'] = "admin/screening/listtoken/$1";

$route['api/admin/screening/report/(:any)']['POST'] = "admin/screening/report/$1";


$route['api/newscreening/gettoken']['POST'] = 'newscreening/checktoken';
$route['api/newscreening/pekerja']['POST'] = 'newscreening/pekerja';
$route['api/newscreening/sekolahdewasa']['POST'] = 'newscreening/sekolahdewasa';
$route['api/newscreening/sekolahanak']['POST'] = 'newscreening/sekolahanak';
$route['api/newscreening/anak']['POST'] = 'newscreening/anak';
$route['api/newscreening/umum']['POST'] = 'newscreening/umum';

$route['api/newscreening/datadasar/umum']['GET'] = 'newscreening/getdatadasar/umum';
$route['api/newscreening/datadasar/pekerja']['GET'] = 'newscreening/getdatadasar/pekerja';
$route['api/newscreening/datadasar/anak']['GET'] = 'newscreening/getdatadasar/anak';
$route['api/newscreening/history/(:any)']['GET'] = 'newscreening/gethistory/$1';


$route['api/newscreening/umum/update/(:any)']['PUT'] = 'newscreening/umum/$1';
$route['api/newscreening/anak/update/(:any)']['PUT'] = 'newscreening/anak/$1';
$route['api/newscreening/pekerja/update/(:any)']['PUT'] = 'newscreening/pekerja/$1';
$route['api/newscreening/sekolahdewasa/update/(:any)']['PUT'] = 'newscreening/sekolahdewasa/$1';
$route['api/newscreening/sekolahanak/update/(:any)']['PUT'] = 'newscreening/sekolahanak/$1';



$route['api/admin/lembaga/add']['POST'] = "admin/lembaga/index";
$route['api/admin/lembaga/detail/(:any)']['GET'] = 'admin/lembaga/index/$1';
$route['api/admin/lembaga/list']['POST'] = 'admin/lembaga/getlist';
$route['api/admin/lembaga/update/(:any)']['PUT'] = 'admin/lembaga/index/$1';
$route['api/admin/lembaga/del/(:any)']['DELETE'] = 'admin/lembaga/index/$1';

$route['api/admin/lembaga/adduser']['POST'] = "admin/lembaga/adduser";
$route['api/admin/lembaga/listuser']['POST'] = 'admin/lembaga/listuser';




$route['api/admin/kader/add']['POST'] = "admin/kader/index";
$route['api/admin/kader/list']['POST'] = "admin/kader/listkader";
$route['api/admin/kader/detail/(:any)']['GET'] = "admin/kader/index/$1";
$route['api/admin/kader/update/(:any)']['PUT'] = "admin/kader/index/$1";
$route['api/admin/kader/del/(:any)']['DELETE'] = "admin/kader/index/$1";

$route['api/admin/kader/chpassword/(:any)']['PUT'] = "admin/kader/chpassword/$1";

$route['api/ikonscreening/add']['POST'] ='ikonscreening/index';
$route['api/ikonscreening/update/(:any)']['PUT'] ='ikonscreening/index/$1';

$route['api/ikonscreening/feedback/(:any)/(:any)']['PUT'] ='ikonscreening/feedback/$1/$2';

$route['api/ikonscreening/indeks/add']['POST'] ='ikonscreening/indekscase';
$route['api/ikonscreening/indeks/user/(:any)']['GET'] ='ikonscreening/indekslist/$1';
$route['api/ikonscreening/indeks/kader/(:any)']['GET'] ='ikonscreening/indeksbykader/$1';
$route['api/ikonscreening/indeks/faskes/(:any)']['GET'] ='ikonscreening/indeksbyfaskes/$1';
$route['api/ikonscreening/indeks/list']['POST'] ='ikonscreening/indekslist';


$route['api/ikonscreening/history/byindeks/(:any)']['GET'] ='ikonscreening/ikonlist/$1';
$route['api/ikonscreening/history/byindeksbyuser/(:any)/(:any)']['GET'] ='ikonscreening/ikonlistuser/$1/$2';

$route['api/rekap/hasil']['POST'] = 'rekap/index';
$route['api/rekap/hasil/perfaskes/(:any)']['GET'] = 'rekap/index/$1';
$route['api/rekap/hasil/tb16/(:any)']['GET'] = 'rekap/rekaptb16/$1';

$route['api/rekap/hasil/tb16rk/(:any)']['GET'] = 'rekap/rekaptb16rk/$1';
$route['api/poin/topten']['GET'] = 'poin/index';
$route['api/poin/mypoin/(:any)']['GET'] = 'poin/mypoin/$1';

$route['api/admin/screening/datalist']['POST'] = "admin/screening/datalist";

$route['api/poin/checkin']['POST'] =  'poin/checkin';

$route['api/infotb/newdetail/(:any)/(:any)']['GET'] = "infotb/newdetail/$1/$2";
$route['api/podcast/newdetail/(:any)/(:any)']['GET'] = 'podcast/newdetail/$1/$2';
$route['api/sitb/getindeks']['POST'] = 'sys/interoperability/index';
$route['api/rekap/hasil/kontaklist']['POST'] = 'rekap/kontaklist';
$route['api/poin/reward/list']['GET'] = 'poin/reward';

$route['api/users/webregister']['POST'] = 'login/webregistration';
$route['api/poin/arsip/(:num)/(:num)']['GET'] = 'poin/arsip/$1/$2';
$route['api/poin/share/(:any)/(:any)']['GET'] = 'poin/sharepoin/$1/$2';
$route['api/sys/tentang']['GET'] = 'about/tentang';

$route['api/ik/kontaklist/list/(:any)']['GET'] = 'rekap/kontaklist/$1';
$route['api/users/check']['POST'] = 'login/activated';

$route['api/ikonscreening/indeks/update/(:any)']['PUT'] ='ikonscreening/indeks/$1';
$route['api/ikonscreening/indeks/detail/(:any)']['GET'] ='ikonscreening/indeks/$1';
$route['api/ikonscreening/indeks/del/(:any)']['DELETE'] ='ikonscreening/indeks/$1';

$route['api/ikonscreening/datadasar/update/(:any)']['PUT'] ='ikonscreening/datadasar/$1';
$route['api/ikonscreening/datadasar/detail/(:any)']['GET'] ='ikonscreening/datadasar/$1';
$route['api/ikonscreening/datadasar/del/(:any)']['DELETE'] ='ikonscreening/datadasar/$1';



$route['api/ikonscreening/indeks/user/(:any)']['POST'] ='ikonscreening/indekslistbyuser/$1';
$route['api/ikonscreening/indeks/kader/(:any)']['POST'] ='ikonscreening/indeksbykader/$1';
$route['api/ikonscreening/indeks/faskes/(:any)']['POST'] ='ikonscreening/indeksbyfaskes/$1';
$route['api/users/checkpassword']['POST'] = "login/validasi";


$route['api/screening/list/(:any)']['POST'] = "screening/indexlist/$1";

$route['api/screening/hasil/faskes/(:any)']['POST'] = "screening/hasilfaskes/$1";


$route['api/kader/list']['POST'] = "kader/index";

$route['api/ik/detailkontak/(:any)']['GET'] = 'rekap/detailkontak/$1';



$route['api/dashboard/capaian/nasional']['POST'] = "dashboard/dashboardnasional/index";
$route['api/dashboard/capaian/propinsi']['POST'] = "dashboard/dashboardnasional/propinsi";
$route['api/dashboard/capaian/kabupaten']['POST'] = "dashboard/dashboardnasional/kabupaten";
$route['api/dashboard/pie/(:any)']['POST'] = "dashboard/dashboardnasional/piedata/$1";
$route['api/dashboard/cascade/(:any)']['POST'] = "dashboard/dashboardnasional/cascade/$1";


$route['api/ikonscreening/history/byindeks/(:any)']['POST'] ='ikonscreening/ikonlist/$1';

$route['api/appointment/list/faskes/(:any)']['POST'] = "appointment/faskes/$1";
$route['api/appointment/list/user/(:any)']['POSt'] = "appointment/userlist/$1";
$route['api/about/kontak']['POST'] = "about/kontak";
$route['api/admin/users/skrining/(:any)']['GET'] ='admin/users/skrining/$1';

//$route['api/mail/invoice']['POST'] = 'sys/mailsender/index';
$route['api/mail/sending/(:any)']['POST'] = 'sys/mailsender/sendmail/$1';
$route['api/mail/invoice/(:any)']['POST'] = 'sys/mailsender/invoice/$1';

$route['api/sys/sso']['POST'] = 'sys/multilogin/index';








