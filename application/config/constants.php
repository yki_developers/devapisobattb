<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


define('DB_MASTER_FASKES','mst_fasyankes');
define('DB_MASTER_KABUPATEN','mst_kabupaten');
define('DB_MASTER_PROPINSI','mst_propinsi');
define('DB_MASTER_KECAMATAN','mst_kecamatan');
define('DB_MASTER_KELURAHAN','mst_kelurahan');
define('DB_DATA_SCREENING','hp_data_screening');
define('DB_DATA_APPOINTMENT','hp_data_appointment');
define('DBVIEW_DATA_APPOINTMENT',"view_appointment_list");
define("DB_VIEW_HASIL_SCREENING","view_hasil_screening");
define("DB_VIEW_DETAIL_SCREENING","view_detail_hasil_screening");
define('DB_USER_FASKES','ref_user_fasyankes');
define('DB_USER_MOBILE','tbl_mobile_users');
define('DB_DATA_OPENFORUM','hp_data_openforum');
define('DB_DATA_COMMENT_OPENFORUM','hp_data_openforum_comment');
define('DB_DATA_PODCAST','hp_data_podcast');
define('DB_DATA_PODCAST_COMMENT','hp_data_podcast_comment');
define('DB_DATA_PODCAST_THEME','hp_data_podcast_theme');
define('DB_DATA_NOTIFKASI','hp_data_notifikasi');
define("DB_VIEW_OPENFORUM_PLAYERID","view_openforum_playerid");
define("DB_MASTER_KERJASAMA","hp_master_program_kerjasama");
define("DB_DATADASAR_UMUM","hp_data_screening_datadasar_umum");
define("DB_DATADASAR_KERJASAMA","hp_data_screening_datadasar_pekerja");
define("DB_DATADASAR_SEKOLAH","hp_data_screening_datadasar");

define("DB_DATASCREENING_UMUM","hp_data_screening_umum");
define("DB_DATASCREENING_ANAKSEKOLAH","hp_data_screening_anak_sekolah");
define("DB_DATASCREENING_DEWASASEKOLAH","hp_data_screening_dewasa_sekolah");
define("DB_DATASCREENING_ANAK","hp_data_screening_anak");
define("DB_DATASCREENING_PEKERJA","hp_data_screening_pekerja");
define("DBVIEW_SCREENING_PEKERJA","view_datascreening_pekerja");
define("DBVIEW_SCREENING_DEWASA_SEKOLAH","view_datascreening_dewasa_sekolah");
define("DBVIEW_SCREENING_ANAK_SEKOLAH","view_datascreening_anak_sekolah");
define("DBVIEW_SCREENING_ANAK","view_datascreening_umum_anak");
define("DBVIEW_SCREENING_UMUM","view_datascreening_umum");

define("DBVIEW_SCREENING_HASIL_PEKERJA","view_hasil_screening_pekerja");
define("DBVIEW_SCREENING_HASIL_DEWASASEKOLAH","view_hasil_screening_sekolah_dewasa");
define("DBVIEW_SCREENING_HASIL_UMUM","view_hasil_screening_umum");
define("DBVIEW_SCREENING_HASIL_ANAKSEKOLAH","view_hasil_screening_sekolah_anak");
define("DBVIEW_SCREENING_HASIL_SEKOLAH","view_hasil_screening_sekolah");
define("DBVIEW_SCREENING_HASIL_ANAK","view_hasil_screening_umum_anak");
define("DBVIEW_SCREENING_INPUT_HISTORY","view_history_screening_input");
define("DBVIEW_SCREENING_HASIL_PERFASKES","view_hasil_perfaskes");
define("DBVIEW_OPENFORUM_LIST","view_openforum_list");


define("DB_MASTER_CSO","hp_master_cso");
define("DB_DATA_IKON_KADER","hp_data_ikon_kader");
define("DB_DATA_IKON_INDEKS","hp_data_ikon_indeks");
define("DB_DATA_IKON_DATADASAR","hp_data_ikon_datadasar");
define("DB_DATA_IKON_SCREENING_DEWASA","hp_data_ikon_screening_dewasa");
define("DB_DATA_IKON_SCREENING_ANAK","hp_data_ikon_screening_anak");
define("DB_DATA_IKON_SCREENING_BALITA","hp_data_ikon_screening_balita");
define("DB_DATA_IKON_BRIDGE_INDEKS","hp_data_bridging_indeks_user");


define("DB_USER_LEMBAGA","ref_user_cso");
define("DB_USER_ADMIN","tbl_user_admin");
define("DBVIEW_HASIL_SCREENING_GABUNGAN","view_hasil_screening_ik_gabungan");
define("DBVIEW_HASIL_SCREENING_DEWASA","view_hasil_screening_ik_dewasa");
define("DBVIEW_HASIL_SCREENING_ANAK","view_hasil_screening_ik_anak");

define("DBVIEW_DATA_SCREENING_DEWASA","view_data_screening_ik_dewasa");
define("DBVIEW_DATA_SCREENING_ANAK","view_data_screening_ik_anak");
define("DBVIEW_DATA_SCREENING_BALITA","view_data_screening_ik_balita");

define("DBVIEW_LIST_INDEKS","view_ik_indeks_byuser");
define("DBVIEW_LIST_HISTORY_IKON","view_history_screening_ikon");

define("DBVIEW_LIST_REKAP_HASIL","view_rekap_hasil_screening_ik");
define("DBVIEW_REKAP_FORMAT_TB16","view_rekap_laporan_tb16");
define("DBVIEW_DATA_INDEKS_PERKADER","view_data_indeks_perkader");

define("DB_DATA_VIEWER_ARTIKEL","hp_data_view_artikel");
define("DBVIEW_DATA_VIEWER_ARTIKEL","dataview_viewer_artikel");

define("DB_SYS_VERSION","hp_sys_version");

define("DBVIEW_REKAP_TB16RK","view_rekap_laporan_tb16rk");
define("DBVIEW_INDKES_KADER","view_data_indeks_perkader");
define("DBVIEW_DATA_KADER_LINK","view_data_kader_link");

define("DB_MASTER_FASKES_PDP","hp_master_faskes_pdp");
define("REF_USER_PDP","ref_user_faskes_pdp");
define("DB_DATA_HASIL_PERFASKES","hp_data_hasil_perfaskes");
define("DBVIEW_LIST_USERADMIN","view_list_useradmin");
define("DBVIEW_REKAP_POIN","view_rekap_poin_kader");

define("DB_DATA_POIN_DAILY_CHECKIN","hp_data_poin_daily");
define("DB_DATA_POIN_ARTIKEL","hp_data_poin_artikel");
define("DB_DATA_POIN_PODCAST","hp_data_poin_podcast");
define("DB_DATA_POIN_THREAD","hp_data_poin_thread");
define("DBVIEW_POIN_REKAP_TOTAL","view_poin_rekap_total");

define("DB_DATA_POIN_REWARD","hp_data_poin_reward");
define("DBVIEW_SITB_KONTAK_LIST","view_sitb_kontak_list");
define("DB_DATA_POIN_ARSIP_BULANAN","hp_data_poin_lock");
define("DB_DATA_POIN_SHARED","hp_data_poin_share_artikel");
define("DBVIEW_LAST_VIEW_ARTIKEL","view_poin_last_view_artikel");
define("DBVIEW_LAST_SHARE_ARTIKEL","view_poin_last_share_artikel");
define("DBVIEW_SITB_DETAIL_KONTAK","view_sitb_detail_kontak");

define("DBVIEW_DASHBOARD_CAPAIAN","view_dashboard_rekap_capaian");
define("DBVIEW_DASHBOARD_PIE_IK","view_dashboard_pie_hasil_ik");
define("DBVIEW_DASHBOARD_PIE_UMUM","view_dashboard_pie_hasil_umum");
define("DBVIEW_DASHBOARD_PIE_PEKERJA","view_dashboard_pie_hasil_pekerja");
define("DBVIEW_DASHBOARD_PIE_SEKOLAH","view_dashboard_pie_hasil_sekolah");
define("DBVIEW_DASHBOARD_PIE_ANAK","view_dashboard_pie_hasil_anak");
define("DBVIEW_DASHBOARD_CASCADE_MERGING","view_dashboard_cascade_merging");


define("DB_SYS_HELPER","hp_sys_helper");
define("DB_SYS_KONTAKME","hp_sys_kontakme");