<?php
Class M_propinsi extends CI_Model{
    public function __construct(){
        parent::__construct();
        
    }

    private function mstPropinsi(){
        return "mst_propinsi";
    }

    public function idCheckPropinsi($id){
        $this->db->select("idpropinsi");
        $this->db->where("idpropinsi",$id);
        $q = $this->db->get($this->mstPropinsi());
        if($q->num_rows()>0){
            return FALSE;
        }else{
            return TRUE;
        }
    }

    public function addPropinsi($rdata){
            return  $this->db->insert($this->mstPropinsi(),$rdata);
    }

    public function listPropinsi(){
        $this->db->select("*");
        $q = $this->db->get($this->mstPropinsi());
        return $q->result();

    }

    public function editPropinsi($rdata,$id){
        if($id!=$rdata['idpropinsi']){
        $d = $this->idCheckPropinsi($rdata['idpropinsi']);
        }else{
            $d= true;
        }
        if($d==true){
        if($this->db->update($this->mstPropinsi(),$rdata,array("idpropinsi"=>$id))){
        $result = array("success"=>true,"info"=>"data berhasil diupdate");
        }else{
        $result=array("success"=>true,"info"=>"data gagal diupdate");
        }
        
    
    }else{
            $result = array("success"=>false,"info"=>"idpropinsi ".$rdata['idpropinsi']." sudah ada");
        }

        return $result;

    }


    public function delPropinsi($id){
        if($this->db->delete($this->mstPropinsi(),array("idpropinsi"=>$id))){
            $result = array("success"=>true,"info"=>"data berhasil dihapus");

        }else{
            $result = array("success"=>false,"info"=>"data gagal dihapus");
        }
        return $result;
    }

    public function detailPropinsi($id){
        $this->db->select("*");
        $this->db->where('idpropinsi',$id);
        return $this->db->get($this->mstPropinsi())->result();
    }

    
    public function searchPropinsi($keyword){
        $this->db->select("*");
        $this->db->like('idpropinsi',$keyword);
        $this->db->or_like('nama_propinsi',$keyword);

        $q = $this->db->get($this->mstPropinsi());
        return $q->result();

    }



}