<?php
Class M_ikonscreening extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getPuskesmasIndeks($indeks_id){
        $this->db->where("indeks_id",$indeks_id);
        $this->db->select("indeks_puskesmas");
        return $this->db->get(DB_DATA_IKON_INDEKS)->row();
    }

    public function checkindeks($person_id=null){

       // $this->db->where("indeks_tb03register",$tb03);
        if($person_id !=null){
            $this->db->where($person_id);
        }
        $this->db->select("indeks_id");
        return $this->db->get(DB_DATA_IKON_INDEKS)->row();

    }

    public function checkBridge($rdata){
        $this->db->where($rdata);
        $q = $this->db->get(DB_DATA_IKON_BRIDGE_INDEKS);
        return $q->num_rows();
    }

    public function getScreeningType($id){
        $this->db->where("ikon_dd_id",$id);
        $q = $this->db->get(DB_DATA_IKON_DATADASAR)->row();
        return $q->ikon_dd_screening;
    }

    public function update($rdata,$id){
        $n = $this->getScreeningType($id);
        if($n=='balita'){
            return $this->db->update(DB_DATA_IKON_SCREENING_BALITA,$rdata,array("ikb_dd_id"=>$id));
        }elseif($n=='anak'){
            return $this->db->update(DB_DATA_IKON_SCREENING_ANAK,$rdata,array("ika_dd_id"=>$id));
        }else{
            return $this->db->update(DB_DATA_IKON_SCREENING_DEWASA,$rdata,array("ikd_dd_id"=>$id));
        }
    }

    public function checkFaskes($id,$kode){
        $this->db->where("ikon_dd_puskesmas",$kode);
        $this->db->where("ikon_dd_id",$id);
        $q = $this->db->get(DB_DATA_IKON_DATADASAR);
        return $q->num_rows();
    }
    public function insertIndeks($rdata){
        $this->db->insert(DB_DATA_IKON_INDEKS,$rdata);
        return $this->db->insert_id();
    }

   




    public function insertBrigde($rdata){
        return $this->db->insert(DB_DATA_IKON_BRIDGE_INDEKS,$rdata);
    }

    public function insertDatadasar($rdata){
        $this->db->insert(DB_DATA_IKON_DATADASAR,$rdata);
        return $this->db->insert_id();
    }

    public function insertScreeningDewasa($rdata){
        return $this->db->insert(DB_DATA_IKON_SCREENING_DEWASA,$rdata);
    }
    public function insertScreeningAnak($rdata){
        return $this->db->insert(DB_DATA_IKON_SCREENING_ANAK,$rdata);
    }

    public function insertScreeningBalita($rdata){
        return $this->db->insert(DB_DATA_IKON_SCREENING_BALITA,$rdata);
    }

    public function getHasilScreening($id){
        $this->db->where('id_dd',$id);
        return $this->db->get(DBVIEW_HASIL_SCREENING_GABUNGAN)->row();
        //return $q->hasil_screening;

    }

    public function getHasilScreeningHiv($id){
        $this->db->where('id_dd',$id);
        $q = $this->db->get(DBVIEW_HASIL_SCREENING_GABUNGAN)->row();
        return $q->hasil_screening_hiv;

    }

    

    public function getListIndexByUser($username){
        $this->db->where("input_user",$username);
        return $this->db->get(DBVIEW_LIST_INDEKS)->result();
    }

    public function getListIndexByKader($kader){
        $this->db->where("indeks_kader",$kader);
        return $this->db->get(DBVIEW_DATA_INDEKS_PERKADER)->result();
    }


    public function getListIndexByFaskes($idfaskes){
        $this->db->where("indeks_puskesmas",$idfaskes);
        return $this->db->get(DBVIEW_DATA_INDEKS_PERKADER)->result();
    }

    public function getListIndex($rdata=null){
        if($rdata!=null){
            $this->db->where($rdata);
        }
        return $this->db->get(DBVIEW_DATA_INDEKS_PERKADER)->result();
    }

 


    public function getListIkonByIndeks($indeks_id){
        $this->db->where("id_indeks",$indeks_id);
        $this->db->order_by("ikon_dd_id","DESC");
        return $this->db->get(DBVIEW_LIST_HISTORY_IKON)->result();
    }


    public function getListIkonByIndeksByUser($indeks_id,$user){
        $this->db->where("id_indeks",$indeks_id);
        $this->db->where("input_user",$user);
        $this->db->order_by("ikon_dd_id","DESC");
        return $this->db->get(DBVIEW_LIST_HISTORY_IKON)->result();
    }


    public function getKontakDetail($id){
        $this->db->where("ikon_dd_id",$id);
        return $this->db->get(DB_DATA_IKON_DATADASAR)->row();
    }

    public function getPlayerId($idfaskes){
        $this->db->select('playerid,username');
        $this->db->where("idfaskes",$idfaskes);
        $this->db->where("usergroup","3");
        $this->db->where("playerid IS NOT NULL");
        return $this->db->get(DB_USER_MOBILE)->result();

    }

    public function insertNotifikasi($rdata){
        return $this->db->insert(DB_DATA_NOTIFKASI,$rdata);
    }





    public function updateIndeks($rdata,$id){
        return $this->db->update(DB_DATA_IKON_INDEKS,$rdata,array("indeks_id"=>$id));
    }

    public function getDetailIndeks($id){
        $this->db->where("indeks_id",$id);
        return $this->db->get(DB_DATA_IKON_INDEKS)->row();
    }

    public function deleteIndeks($id){
        return $this->db->delete(DB_DATA_IKON_INDEKS,array("indeks_id"=>$id));
    }

    public function updateDataDasar($rdata,$id){
        return $this->db->update(DB_DATA_IKON_DATADASAR,$rdata,array("ikon_dd_id"=>$id));
    }

    public function getDetailDatadasar($id){
        $this->db->where("ikon_dd_id",$id);
        return $this->db->get(DB_DATA_IKON_DATADASAR)->row();
    }

    public function deleteDatadasar($id){
        return $this->db->delete(DB_DATA_IKON_DATADASAR,array("ikon_dd_id"=>$id));
    }

    public function searchIndeks($rdata){
        $this->db->group_start();
        $this->db->where('indeks_name',$rdata['search']);
        $this->db->or_where("indeks_person_id",$rdata['search']);
        $this->db->or_where("indeks_tb03register",$rdata['search']);
        $this->db->group_end();
        unset($rdata['search']);
        $this->db->where($rdata);
        return $this->db->get(DBVIEW_DATA_INDEKS_PERKADER)->result();
    }


    public function searchListIndexByUser($username,$rdata){

        $start = ($rdata['page']-1)*$rdata['size'];
        $this->db->group_start();
        $this->db->like('indeks_name',$rdata['search'],'both');
        $this->db->or_like("indeks_person_id",$rdata['search']);
        $this->db->or_like("indeks_tb03register",$rdata['search']);
       
        $this->db->group_end();
        $this->db->where("input_user",$username);
        $this->db->limit($rdata['size'],$start);
        return $this->db->get(DBVIEW_LIST_INDEKS)->result();
    }

    public function searchListIndexByKader($kader,$rdata){
        $start = ($rdata['page']-1)*$rdata['size'];
        $this->db->group_start();
        $this->db->like('indeks_name',$rdata['search'],'both');
        $this->db->or_like("indeks_person_id",$rdata['search']);
        $this->db->or_like("indeks_tb03register",$rdata['search']);
   
        $this->db->group_end();
     
        $this->db->where("indeks_kader",$kader);
        $this->db->limit($rdata['size'],$start);
        return $this->db->get(DBVIEW_DATA_INDEKS_PERKADER)->result();
    }


    public function searchListIndexByFaskes($idfaskes,$rdata){
        $start = ($rdata['page']-1)*$rdata['size'];
        $this->db->group_start();
        $this->db->like('indeks_name',$rdata['search'],'both');
        $this->db->or_like("indeks_person_id",$rdata['search']);
        $this->db->or_like("indeks_tb03register",$rdata['search']);
     
        $this->db->group_end();
      
        $this->db->where("indeks_puskesmas",$idfaskes);
        $this->db->limit($rdata['size'],$start);
        return $this->db->get(DBVIEW_DATA_INDEKS_PERKADER)->result();
    }


    public function searchListIkonByIndeks($indeks_id,$rdata){
        $start = ($rdata['page']-1)*$rdata['size'];
        $this->db->group_start();
        $this->db->like('ikon_dd_nama',$rdata['search'],'both');
        $this->db->or_like("hasil_screening",$rdata['search'],'both');
        $this->db->group_end();
        $this->db->where("id_indeks",$indeks_id);
        $this->db->order_by("ikon_dd_id","DESC");
        $this->db->limit($rdata['size'],$start);
        return $this->db->get(DBVIEW_LIST_HISTORY_IKON)->result();
    }

    public function newListIkonByIndeks($indeks_id,$page,$size){
        $start = ($page-1)*$size;
        $this->db->where("id_indeks",$indeks_id);
        $this->db->order_by("ikon_dd_id","DESC");
        $this->db->limit($size,$start);
        return $this->db->get(DBVIEW_LIST_HISTORY_IKON)->result();

    }



    public function newListIndexByFaskes($idfaskes,$page,$size){
        $start = ($page-1)*$size;
        $this->db->where("indeks_puskesmas",$idfaskes);
        $this->db->limit($size,$start);
        //return $this->db->get_compiled_select(DBVIEW_DATA_INDEKS_PERKADER);
        return $this->db->get(DBVIEW_DATA_INDEKS_PERKADER)->result();
    }


    public function newListIndexByKader($kader,$page,$size){
        $start = ($page-1)*$size;
        $this->db->where("indeks_kader",$kader);
        $this->db->limit($size,$start);
        return $this->db->get(DBVIEW_DATA_INDEKS_PERKADER)->result();
    }


    public function newListIndexByUser($username,$page,$size){

        $start = ($page-1)*$size;
        $this->db->limit($size,$start);
        $this->db->where("input_user",$username);
        return $this->db->get(DBVIEW_LIST_INDEKS)->result();
    }



}