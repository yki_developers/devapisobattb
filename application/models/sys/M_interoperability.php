<?php
Class M_interoperability extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function checkPersonId($id){
        $this->db->select(DB_DATA_IKON_INDEKS.".*");
        $this->db->select("nama_propinsi as propinsi");
        $this->db->select("nama_kabupaten as kabupaten");
        $this->db->select("nama_kecamatan as kecamatan");
        $this->db->where("indeks_person_id",$id);

        $this->db->join(DB_MASTER_PROPINSI,"idpropinsi=indeks_propinsi");
        $this->db->join(DB_MASTER_KABUPATEN,"idkabupaten=indeks_kabupaten");
        $this->db->join(DB_MASTER_KECAMATAN,"idkecamatan=indeks_kecamatan");
        return $this->db->get(DB_DATA_IKON_INDEKS)->row();
    }

    public function getKodeKecamatan($rdata){
        $this->db->where($rdata);
        return $this->db->get(DB_MASTER_KECAMATAN)->row();
    }
}