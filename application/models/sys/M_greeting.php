<?php
Class M_greeting extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }
    public function getHtml(){
        $this->db->order_by("id","DESC");
        $this->db->limit("1","0");
        $q = $this->db->get("hp_sys_greeting")->row();
        return $q->html_code;
    }

    public function getTemplate($id){
        $this->db->where("template_id",$id);
        $this->db->select("template_apps,template_subject,template_text");
        $this->db->select("template_username as username,FROM_BASE64(template_password) as pwd");
        return $this->db->get("hp_sys_email_template")->row();

    }
}