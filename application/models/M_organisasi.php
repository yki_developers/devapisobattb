<?php
Class M_organisasi extends CI_Model{
    public function __construct(){
        parent::__construct();

    }

    private function mstOrganisasi($alias=null){
        return "mst_organisasi ".$alias;
    }

    public function addOrganisasi($rdata){
        if($this->db->insert($this->mstOrganisasi(),$rdata)){
            return array("success"=>true,"info"=>"Data Organisasi berhasil disimpan");
        }else{
            return array("success"=>false,"info"=>"Data Organisasi Gagal disimpan");
        }

    }

    public function getOrganisasi($page=1,$size=100,$order="ASC"){
        $start = ($page-1)*$size;
        $this->db->select("a.*");
        $this->db->select("b.nama_kabupaten");
        $this->db->select("c.nama_propinsi");
        $this->db->select("(SELECT count(idthread) as thread From tbl_forum_thread tf where tf.idorganisasi=a.idorganisasi && tf.status_thread='open' group by tf.idorganisasi) as total_thread");
        $this->db->from($this->mstOrganisasi("a"));
        $this->db->join("mst_kabupaten b","a.idkabupaten=b.idkabupaten");
        $this->db->join("mst_propinsi c","c.idpropinsi=a.idpropinsi");
        $this->db->order_by("a.idorganisasi","ASC");
        $this->db->limit($size,$start);
       //return $this->db->get_compiled_select();
        return  $this->db->get()->result();

    }

    public function getThread($idorganisasi){
       
        $this->db->where('idorganisasi',$idorganisasi);
        $this->db->where("status_thread","open");
        return $this->db->count_all_results("tbl_forum_thread");
    }

    public function updateOrganisasi($rdata,$id){
        if($this->db->update($this->mstOrganisasi(),$rdata,array("idorganisasi"=>$id))){
            return array("success"=>true,"info"=>"Data Organisasi berhasil diubah");

        }else{
            return array("success"=>false,"info"=>"Data Organisasi gagal diubah");

        }

    }
    public function deleteOrganisasi($id){
        if($this->db->delete($this->mstOrganisasi(),array("idorganisasi"=>$id))){
            return array("success"=>true,"info"=>"Data Organisasi berhasil dihapus");

        }else{
            return array("success"=>false,"info"=>"Data Organisasi gagal dihapus");

        }

    }

    public function userOrganisasi($id){
        $this->db->select("b.playerid");
        $this->db->from("ref_user_organisasi a");
        $this->db->join("tbl_mobile_users b","a.username=b.username","left");
        $this->db->where("a.idorganisasi",$id);
        $this->db->where("a.superuser","1");
        $q = $this->db->get();
        return $q->result();
    }

    public function detailOrganisasi($id){
        $this->db->select("a.*");
        $this->db->select("b.nama_kabupaten");
        $this->db->select("c.nama_propinsi");
        $this->db->from($this->mstOrganisasi("a"));
        $this->db->join("mst_kabupaten b","a.idkabupaten=b.idkabupaten");
        $this->db->join("mst_propinsi c","c.idpropinsi=a.idpropinsi");
        $this->db->where("a.idorganisasi",$id);
      // return $this->db->get_compiled_select();
         return  $this->db->get()->row();

    }


    

}