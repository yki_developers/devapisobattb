<?php
Class M_dashboard extends CI_model{
    public function __construct()
    {
        parent::__construct();
    }


    public function cascadeSkriningPusat($rdata=null){
        $this->db->select("
        CASE skrining
        WHEN 'umum' THEN 'Skrining Umum'
        WHEN 'anak' THEN 'Skrining Anak'
        WHEN 'pekerja' THEN 'Skrining Pekerja'
        WHEN 'sekolah' THEN 'Skrining Sekolah'
        END
        as jenis_skrining"
    );

    $this->db->select_sum('total_skrining','total_skrining');
    $this->db->select_sum('terduga_tbc_so','terduga_tbc_so');
    $this->db->select_sum('terduga_tbc_ro','terduga_tbc_ro');
    $this->db->select_sum('diperiksa_tbc','diperiksa_tbc');
    $this->db->select_sum('sakit_tbc','sakit_tbc');
    $this->db->select_sum('mendapat_tpt','mendapat_tpt');
    $this->db->group_by('skrining');
    if($rdata!=null){
        $this->db->where($rdata);
    }else{
        $this->db->where("YEAR(tanggal_skrining)",date("Y"));
    }
    return $this->db->get(DBVIEW_DASHBOARD_CASCADE_MERGING)->result();
    }

    public function cascadeSkriningPropinsi($rdata=null,$propinsi){
        $this->db->select("
        CASE skrining
        WHEN 'umum' THEN 'Skrining Umum'
        WHEN 'anak' THEN 'Skrining Anak'
        WHEN 'pekerja' THEN 'Skrining Pekerja'
        WHEN 'sekolah' THEN 'Skrining Sekolah'
        END
        as jenis_skrining"
    );

    $this->db->select_sum('total_skrining','total_skrining');
    $this->db->select_sum('terduga_tbc_so','terduga_tbc_so');
    $this->db->select_sum('terduga_tbc_ro','terduga_tbc_ro');
    $this->db->select_sum('diperiksa_tbc','diperiksa_tbc');
    $this->db->select_sum('sakit_tbc','sakit_tbc');
    $this->db->select_sum('mendapat_tpt','mendapat_tpt');
    $this->db->group_by('skrining,propinsi');
    if($rdata!=null){
        $this->db->where($rdata);
    }else{
        $this->db->where("propinsi",$propinsi);
        $this->db->where("YEAR(tanggal_skrining)",date("Y"));
    }
   // return $this->db->get_compiled_select(DBVIEW_DASHBOARD_CASCADE_MERGING);
    return $this->db->get(DBVIEW_DASHBOARD_CASCADE_MERGING)->result();
    }

    public function cascadeSkriningKabupaten($rdata=null,$kabupaten){
        $this->db->select("
        CASE skrining
        WHEN 'umum' THEN 'Skrining Umum'
        WHEN 'anak' THEN 'Skrining Anak'
        WHEN 'pekerja' THEN 'Skrining Pekerja'
        WHEN 'sekolah' THEN 'Skrining Sekolah'
        END
        as jenis_skrining"
    );

    $this->db->select_sum('total_skrining','total_skrining');
    $this->db->select_sum('terduga_tbc_so','terduga_tbc_so');
    $this->db->select_sum('terduga_tbc_ro','terduga_tbc_ro');
    $this->db->select_sum('diperiksa_tbc','diperiksa_tbc');
    $this->db->select_sum('sakit_tbc','sakit_tbc');
    $this->db->select_sum('mendapat_tpt','mendapat_tpt');
    $this->db->group_by('skrining,kabupaten');
    if($rdata!=null){
        $this->db->where($rdata);
    }else{
        $this->db->where("kabupaten",$kabupaten);
        $this->db->where("YEAR(tanggal_skrining)",date("Y"));
    }
   // return $this->db->get_compiled_select(DBVIEW_DASHBOARD_CASCADE_MERGING);
    return $this->db->get(DBVIEW_DASHBOARD_CASCADE_MERGING)->result();
    }


    public function cascadeSkriningFasyankes($rdata=null,$fasyankes){
        $this->db->select("
        CASE skrining
        WHEN 'umum' THEN 'Skrining Umum'
        WHEN 'anak' THEN 'Skrining Anak'
        WHEN 'pekerja' THEN 'Skrining Pekerja'
        WHEN 'sekolah' THEN 'Skrining Sekolah'
        END
        as jenis_skrining"
    );

    $this->db->select_sum('total_skrining','total_skrining');
    $this->db->select_sum('terduga_tbc_so','terduga_tbc_so');
    $this->db->select_sum('terduga_tbc_ro','terduga_tbc_ro');
    $this->db->select_sum('diperiksa_tbc','diperiksa_tbc');
    $this->db->select_sum('sakit_tbc','sakit_tbc');
    $this->db->select_sum('mendapat_tpt','mendapat_tpt');
    $this->db->group_by('skrining,fasyankes');
    if($rdata!=null){
        $this->db->where($rdata);
    }else{
        $this->db->where("fasyankes",$fasyankes);
        $this->db->where("YEAR(tanggal_skrining)",date("Y"));
    }
   // return $this->db->get_compiled_select(DBVIEW_DASHBOARD_CASCADE_MERGING);
    return $this->db->get(DBVIEW_DASHBOARD_CASCADE_MERGING)->result();
    }

    public function CapaianNasional($rdata=null){

        $this->db->select('propinsi,kategori');
        $this->db->select("nama_propinsi");
        $this->db->select_sum('total_skrining','jumlah_skrining');
       
        $this->db->group_by('propinsi,kategori');
        if($rdata!=null){
            $this->db->where($rdata);
        }else{
            $this->db->where("YEAR(tanggal_skrining)",date("Y"));
        }
        $this->db->join(DB_MASTER_PROPINSI,"propinsi=idpropinsi");
        return $this->db->get(DBVIEW_DASHBOARD_CAPAIAN)->result();
    }

    public function CapaianPropinsi($rdata=null){

        $this->db->select('kabupaten,kategori');
        $this->db->select("nama_kabupaten");
        $this->db->select_sum('total_skrining','jumlah_skrining');
       
        $this->db->group_by('kabupaten,kategori');
        if($rdata!=null){
            $this->db->where($rdata);
        }else{
            $this->db->where("YEAR(tanggal_skrining)",date("Y"));
        }
        $this->db->join(DB_MASTER_KABUPATEN,"kabupaten=idkabupaten");
        return $this->db->get(DBVIEW_DASHBOARD_CAPAIAN)->result();
    }

    public function CapaianKabupaten($rdata=null){

        $this->db->select('fasyankes');
        $this->db->select("nama_fasyankes,kategori");
        $this->db->select_sum('total_skrining','jumlah_skrining');
       
        $this->db->group_by('fasyankes,kategori');
        if($rdata!=null){
            $this->db->where($rdata);
        }else{
            $this->db->where("YEAR(tanggal_skrining)",date("Y"));
        }
        $this->db->join(DB_MASTER_FASKES,"fasyankes=kdfasyankes");
        return $this->db->get(DBVIEW_DASHBOARD_CAPAIAN)->result();
    }


    public function pieNasional($type,$rdata=null){
        $this->db->select("hasil");
        $this->db->select_sum("total","total");
        $this->db->group_by("hasil");
        if($rdata!=null){
            $this->db->where($rdata);
        }else{
            $this->db->where("YEAR(tanggal_skrining)",date("Y"));
        }
        if($type=="umum"){
        return $this->db->get(DBVIEW_DASHBOARD_PIE_UMUM)->result();
        }elseif($type=="anak"){

            return $this->db->get(DBVIEW_DASHBOARD_PIE_ANAK)->result();
        }elseif($type=="sekolah"){
            return $this->db->get(DBVIEW_DASHBOARD_PIE_SEKOLAH)->result();

        }elseif($type=="pekerja"){
            return $this->db->get(DBVIEW_DASHBOARD_PIE_PEKERJA)->result();

        }elseif($type=="kontak"){
            return $this->db->get(DBVIEW_DASHBOARD_PIE_IK)->result();

        }

    }




    public function piePropinsi($type,$rdata=null){
        $this->db->select("hasil");
        $this->db->select_sum("total","total");
        $this->db->group_by("hasil");
        if($rdata!=null){
            $this->db->where($rdata);
        }else{
            $this->db->where("YEAR(tanggal_skrining)",date("Y"));
        }
        if($type=="umum"){
        return $this->db->get(DBVIEW_DASHBOARD_PIE_UMUM)->result();
        }elseif($type=="anak"){

            return $this->db->get(DBVIEW_DASHBOARD_PIE_ANAK)->result();
        }elseif($type=="sekolah"){
            return $this->db->get(DBVIEW_DASHBOARD_PIE_SEKOLAH)->result();

        }elseif($type=="pekerja"){
            return $this->db->get(DBVIEW_DASHBOARD_PIE_PEKERJA)->result();

        }elseif($type=="kontak"){
            return $this->db->get(DBVIEW_DASHBOARD_PIE_IK)->result();

        }

    }
}