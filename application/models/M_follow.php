<?php
Class M_follow extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    private function tblFollow($id=null){
        return "tbl_follower_organisasi ".$id;
    }
    private function tblUserAdmin($id=null){
        return "tbl_mobile_users ".$id;
    }
    private function mstOrganisasi($id=null){
        return "mst_organisasi ".$id;
    }
    public function followOrganisasi($rdata){
        return $this->db->insert($this->tblFollow(),$rdata);
    }

    public function getFollower($idorganisasi){
        $this->db->select("a.username");
        $this->db->select("b.nama_pengguna");
        $this->db->select("a.idorganisasi");
        $this->db->select("c.nama_organisasi");
        $this->db->select("b.foto");
        $this->db->select("b.email");
        $this->db->from($this->tblFollow("a"));
        $this->db->join($this->tblUserAdmin("b"),"a.username=b.username");
        $this->db->join($this->mstOrganisasi("c"),"a.idorganisasi=c.idorganisasi");
        $this->db->where("a.idorganisasi",$idorganisasi);
        //return $this->db->get_compiled_select();//
        return $this->db->get()->result();
    }

    public function getFollowing($username){
        $this->db->select("a.username");
        $this->db->select("b.nama_pengguna");
        $this->db->select("a.idorganisasi");
        $this->db->select("c.nama_organisasi");
        $this->db->select("b.foto");
        $this->db->select("b.email");
        $this->db->from($this->tblFollow("a"));
        $this->db->join($this->tblUserAdmin("b"),"a.username=b.username");
        $this->db->join($this->mstOrganisasi("c"),"a.idorganisasi=c.idorganisasi");
        $this->db->where("a.username",$username);
        return $this->db->get()->result();
    }

    public function getStatusFollow($username,$idorganisasi){
        //$this->db->select("a.idorganisasi");
        $this->db->where("username",$username);
        $this->db->where("idorganisasi",$idorganisasi);
        $q = $this->db->get($this->tblFollow());
        if($q->num_rows()==0){
            return true;
        }else{
            return false;
        }

    }


    public function getTotalFollower($idorganisasi){
        $this->db->select("*");
        $this->db->where("idorganisasi",$idorganisasi);
        $this->db->from($this->tblFollow());
        return $this->db->count_all_results();
    }

    public function unFollowOrganisasi($idorganisasi,$username){
        $this->db->where("idorganisasi",$idorganisasi);
        $this->db->where("username",$username);
        return $this->db->delete($this->tblFollow());
    }


    private function tblThreadFollower($id=null){
        return "tbl_follower_forum ".$id;
    }

    private function tblThread($id=null){
        return "tbl_forum_thread ".$id;
    }

    public function followThread($rdata){
        return $this->db->insert($this->tblThreadFollower(),$rdata);
    }

    public function unFollowThread($idthread,$username){
        $this->db->where("idthread",$idthread);
        $this->db->where("username",$username);
        return $this->db->delete($this->tblThreadFollower());
    }

    public function getThreadFollowing($username){
        $this->db->select("a.*");
        $this->db->select("b.judul_thread");
        $this->db->from($this->tblThreadFollower("a"));
        $this->db->join($this->tblThread("b"),"a.idthread=b.idthread");
        $this->db->where("a.username",$username);
        return $this->db->get()->result();
    }

    public function getTotalThreadFollower($idthread){
        $this->db->where("idthread",$idthread);
        return $this->db->count_all_results($this->tblThreadFollower());
    }

    public function getTotalThreadFollow($username){
        $this->db->where("username",$username);
       return  $this->db->count_all_results($this->tblThreadFollower());
    }


}