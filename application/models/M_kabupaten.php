<?php
Class M_kabupaten extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    private function mstKabupaten(){
        return "mst_kabupaten";
    }

    private function idCheckKabupaten($id){
        $this->db->select("idkabupaten");
        $this->db->where("idkabupaten",$id);
        $q=$this->db->get($this->mstKabupaten());
        if($q->num_rows()>0){
            return false;
        }else{
            return true;
        }
    }

    public function addKabupaten($rdata){

        $d = $this->idCheckKabupaten($rdata['idkabupaten']);
        //return $d;
        
        if($d=="true"){
            if($this->db->insert($this->mstKabupaten(),$rdata)){
                $result = array("success"=>true,"info"=>"data berhasil disimpan");

            }else{
                $result = array("success"=>false,"info"=>"data gagal disimpan");    
            }
        }else{
            $result = array("success"=>false,"info"=>"kode kabupaten sudah ada");
        }

        return $result;
        

    }

    public function getKabupaten($idpropinsi){
        $this->db->select("a.idkabupaten");
        $this->db->select("a.nama_kabupaten");
        $this->db->select("b.nama_propinsi");
        $this->db->from($this->mstKabupaten()." a");
        $this->db->where("a.idpropinsi",$idpropinsi);
        $this->db->join("mst_propinsi b","a.idpropinsi=b.idpropinsi");
        return $this->db->get()->result();
    }


    public function getKabupatenAll($page,$size){
        $start=($page-1)*$size;
        $this->db->select("a.idkabupaten");
        $this->db->select("a.nama_kabupaten");
        $this->db->select("b.nama_propinsi");
        $this->db->from($this->mstKabupaten()." a");
        //$this->db->where("a.idpropinsi",$idpropinsi);
        $this->db->join("mst_propinsi b","a.idpropinsi=b.idpropinsi");
        $this->db->limit($size,$start);
        return $this->db->get()->result();
        //return $this->db->get_compiled_select();
    }


    public function searchKabupaten($keyword){
        $this->db->select("a.idkabupaten");
        $this->db->select("a.nama_kabupaten");
        $this->db->select("b.nama_propinsi");
        $this->db->from($this->mstKabupaten()." a");
        //$this->db->where("a.idpropinsi",$idpropinsi);
        $this->db->join("mst_propinsi b","a.idpropinsi=b.idpropinsi");
        $this->db->like('nama_kabupaten',$keyword);
        $this->db->or_like('idkabupaten',$keyword,'none');
        $this->db->limit('50','0');
        return $this->db->get()->result();
        //return $this->db->get_compiled_select();
    }



    public function countAll(){
        return $this->db->get($this->mstKabupaten())->num_rows();
    }
    
    public function updateKabupaten($rdata,$id){
        if($rdata['idkabupaten']!=$id){
            $d = $this->idCheckKabupaten($rdata['idkabupaten']);
        }else{
            $d = TRUE;
        }
        if($d==TRUE){
        if($this->db->update($this->mstKabupaten(),$rdata,array("idkabupaten"=>$id))){
            $result = array("success"=>true,"info"=>"Data Kabupaten berhasil diupdate");

        }else{
            $result= array("success"=>true,"info"=>"Update data kabupaten Gagal");
        }

        }else{
            $result = array("success"=>FALSE,"info"=>"Kode Kabupaten Sudah Digunakan");
        }

        return $result;

    }

    public function deleteKabupaten($id){
        if($this->db->delete($this->mstKabupaten(),array("idkabupaten"=>$id))){
            $result = array("success"=>true,"info"=>"Data Berhasil dihapus");

        }else{
            $result = array("success"=>false,"info"=>"Data Gagal dihapus");

        }
        return $result;
    }

    public function detailKabupaten($id){
        $this->db->select("*");
        $this->db->where("idkabupaten",$id);
        return $this->db->get($this->mstKabupaten())->row();
    }


}