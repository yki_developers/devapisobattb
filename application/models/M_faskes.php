<?php
Class M_faskes extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	private function tableFaskes($id=null){
		return "mst_fasyankes ".$id;
	}

	public function searchFasyankes($search,$page=1,$size=100,$order="DESC"){
		$start = ($page-1)*$size;
		$this->db->select("a.kdfasyankes");
		$this->db->select("a.kdfasyankes");
		$this->db->select("a.nama_fasyankes");
		$this->db->select("c.jenis_fasyankes");
		$this->db->select("d.nama_kecamatan");
		$this->db->select("b.nama_kabupaten");
		$this->db->select("a.jam_buka");
		$this->db->select("a.bpjs");
		$this->db->select("a.thumbnails as foto");
		$this->db->select("(SELECT round(AVG(jml_bintang),2)  FROM tbl_penilaian_faskes pf WHERE pf.kd_fasyankes=a.kdfasyankes && pf.status_penilaian='approve') as jml_bintang");
		$this->db->from($this->tableFaskes("a"));
		$this->db->join("mst_kabupaten b","a.idkabupaten=b.idkabupaten");
		$this->db->join("mst_jenis_fasyankes c","c.idjenis=a.jenis_fasyankes");
		$this->db->join("mst_kecamatan d","d.idkecamatan=a.idkecamatan","left");
		//$this->db->where("a.idkabupaten",$idkabupaten);
		$this->db->like('a.nama_fasyankes',$search);
		$this->db->or_like('d.nama_kecamatan',$search);
		$this->db->or_like('b.nama_kabupaten',$search);
		$this->db->limit($size,$start);
		$this->db->order_by("jml_bintang",$order);
	$q = $this->db->get();
	return $q->result();	
	//return $this->db->get_compiled_select();
	}



	public function ListByKecamatan($idkecamatan){
		$this->db->select("kdfasyankes,nama_fasyankes");
		$this->db->where("idkecamatan",$idkecamatan);
		return $this->db->get(DB_MASTER_FASKES)->result();
	}
	
	public function ListFasyankes($idkabupaten,$page=1,$size=100,$order="DESC"){
		$start = ($page-1)*$size;
		$this->db->select("a.kdfasyankes");
		$this->db->select("a.kdfasyankes");
		$this->db->select("a.nama_fasyankes");
		$this->db->select("c.jenis_fasyankes");
		$this->db->select("a.jam_buka");
		$this->db->select("a.bpjs");
		$this->db->select("a.thumbnails as foto");
		$this->db->select("(SELECT round(AVG(jml_bintang),2)  FROM tbl_penilaian_faskes pf WHERE pf.kd_fasyankes=a.kdfasyankes && pf.status_penilaian='approve') as jml_bintang");
		$this->db->from($this->tableFaskes("a"));
		$this->db->join("mst_kabupaten b","a.idkabupaten=b.idkabupaten");
		$this->db->join("mst_jenis_fasyankes c","c.idjenis=a.jenis_fasyankes");
		$this->db->where("a.idkabupaten",$idkabupaten);
		$this->db->limit($size,$start);
		$this->db->order_by("idfasyankes",$order);
	$q = $this->db->get();
	return $q->result();	
	//return $this->db->get_compiled_select($this->tableFaskes());
	}


	public function ListFasyankesNearme($lat,$lon,$page=1,$size=100){
		$start = ($page-1)*$size;
		$this->db->select("round(CALCULATE_DISTANCE('".$lat."','".$lon."',a.latitude,a.longitude),2) as jarak");
		$this->db->select("a.kdfasyankes");
		$this->db->select("a.kdfasyankes");
		$this->db->select("a.nama_fasyankes");
		$this->db->select("c.jenis_fasyankes");
		$this->db->select("a.jam_buka");
		$this->db->select("a.bpjs");
		$this->db->select("a.thumbnails as foto");
		$this->db->select("(SELECT round(AVG(jml_bintang),2)  FROM tbl_penilaian_faskes pf WHERE pf.kd_fasyankes=a.kdfasyankes && pf.status_penilaian='approve') as jml_bintang");
		$this->db->from($this->tableFaskes("a"));
		$this->db->join("mst_kabupaten b","a.idkabupaten=b.idkabupaten");
		$this->db->join("mst_jenis_fasyankes c","c.idjenis=a.jenis_fasyankes");
		$this->db->where("a.latitude is not null");
		$this->db->limit($size,$start);
		$this->db->order_by("jarak","ASC");
	$q = $this->db->get();
	return $q->result();	
	//return $this->db->get_compiled_select();
	}


	public function detailFasyankes($kdfasyankes){
		$this->db->select("a.*");
		$this->db->select("e.jenis_fasyankes");
		$this->db->select("b.nama_propinsi");
		$this->db->select("c.nama_kabupaten");
		$this->db->select("d.nama_kecamatan");
		$this->db->select("(SELECT round(AVG(jml_bintang),2)  FROM tbl_penilaian_faskes pf WHERE pf.kd_fasyankes=a.kdfasyankes && pf.status_penilaian='approve') as jml_bintang");
		$this->db->from($this->tableFaskes("a"));
		$this->db->join("mst_jenis_fasyankes e","a.jenis_fasyankes=e.idjenis");
		$this->db->join("mst_propinsi b","a.idpropinsi=b.idpropinsi");
		$this->db->join("mst_kabupaten c","a.idkabupaten=c.idkabupaten");
		$this->db->join("mst_kecamatan d","a.idkecamatan=d.idkecamatan");
		
		

		$this->db->where("a.kdfasyankes",$kdfasyankes);
		$q = $this->db->get();
		return $q->row();
	}

	public function insertNew($data){
		if($this->db->insert($this->tableFaskes(),$data)){
		return true;
		}else{
			return false;
		}
	}
	
	public function updateFaskes($data,$id){
		return $this->db->update($this->tableFaskes(),$data,array("idfasyankes"=>$id));
	}

	public function hapusFaskes($id){
		return $this->db->delete($this->tableFaskes(),array("idfasyankes"=>$id));
	}



	public function ListAllFasyankes($page,$size){
		$start = ($page-1)*$size;
		$this->db->select("a.idfasyankes");
		$this->db->select("a.kdfasyankes");
		$this->db->select("a.kdfasyankes");
		$this->db->select("a.nama_fasyankes");
		$this->db->select("c.jenis_fasyankes");
		$this->db->select("a.jam_buka");
		$this->db->select("a.bpjs");
		$this->db->select("a.thumbnails as foto");
		$this->db->select("(SELECT round(AVG(jml_bintang),2)  FROM tbl_penilaian_faskes pf WHERE pf.kd_fasyankes=a.kdfasyankes && pf.status_penilaian='approve') as jml_bintang");
		$this->db->select("a.alamat_fasyankes");
		$this->db->select("d.nama_propinsi");
		$this->db->select("b.nama_kabupaten");
		$this->db->select("if(e.nama_kecamatan=null,'-',e.nama_kecamatan) as nama_kecamatan");
		$this->db->from($this->tableFaskes("a"));
		
		$this->db->join("mst_kabupaten b","a.idkabupaten=b.idkabupaten");
		$this->db->join("mst_jenis_fasyankes c","c.idjenis=a.jenis_fasyankes");
		$this->db->join("mst_propinsi d","d.idpropinsi=a.idpropinsi");
		$this->db->join("mst_kecamatan e","e.idkecamatan=a.idkecamatan","left");
		//$this->db->where("a.idkabupaten",$idkabupaten);
		$this->db->limit($size,$start);
		//$this->db->order_by("idfasyankes",$order);
	$q = $this->db->get();
	return $q->result();	
	//return $this->db->get_compiled_select($this->tableFaskes());
	}

	public function countAll(){
        return $this->db->get($this->tableFaskes())->num_rows();
    }


}