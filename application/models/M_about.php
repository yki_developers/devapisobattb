<?php
Class M_about extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    private function tblAbout(){
        return "tbl_about";
    }

    public function getAbout(){
        $q = $this->db->get($this->tblAbout());
        return $q->row();
    }

    public function updateAbout($rdata){
        return $this->db->update($this->tblAbout(),$rdata);
    }

    public function getVersion(){
        return $this->db->get(DB_SYS_VERSION)->row();
    }

    public function getTentang(){
        return $this->db->get("hp_sys_tentang")->row();
    }

    public function getHelper(){
        return $this->db->get(DB_SYS_HELPER)->result();
    }

    public function insertKontakMe($rdata){
        return $this->db->insert(DB_SYS_KONTAKME,$rdata);
    }



}