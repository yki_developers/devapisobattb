<?php
Class M_newscreening extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }
    public function getToken($id){
        $this->db->where("program_token",$id);
        return $this->db->get(DB_MASTER_KERJASAMA)->row();
    }

    public function inHasilPerfaskes($rdata){
        return $this->db->insert(DB_DATA_HASIL_PERFASKES,$rdata);
    }

    public function inDataPekerja($rdata){
        $this->db->insert(DB_DATADASAR_KERJASAMA,$rdata);
        return $this->db->insert_id(); 
    }
    public function inDataScreeningPekerja($rdata){
        $this->db->insert(DB_DATASCREENING_PEKERJA,$rdata);
        return $this->db->insert_id();
    }



    public function inDataSekolah($rdata){
        $this->db->insert(DB_DATADASAR_SEKOLAH,$rdata);
        return $this->db->insert_id();
    }

    public function inDataScreeningSekolahDewasa($rdata){
        $this->db->insert(DB_DATASCREENING_DEWASASEKOLAH,$rdata);
        return $this->db->insert_id();
    }

    public function inDataScreeningSekolahAnak($rdata){
       $this->db->insert(DB_DATASCREENING_ANAKSEKOLAH,$rdata);
       return $this->db->insert_id();
    }

    public function inDataScreeningAnak($rdata){
        $this->db->insert(DB_DATASCREENING_ANAK,$rdata);
        return $this->db->insert_id();
    }


    public function inDataDasarUmum($rdata){
        $this->db->insert(DB_DATADASAR_UMUM,$rdata);
        return $this->db->insert_id();
    }
    public function inDataScreeningUmum($rdata){
        $this->db->insert(DB_DATASCREENING_UMUM,$rdata);
        return $this->db->insert_id();
        //return $this->db->insert_id();
    }

   

    public function getDataDasarUmum(){
        return $this->db->get(DB_DATADASAR_UMUM)->result();
    }

public function getDataDasarKerjasama(){
        return $this->db->get(DB_DATADASAR_KERJASAMA)->result();
    }
    public function getDataDasarAnak(){
        return $this->db->get(DB_DATADASAR_SEKOLAH)->result();
    }

    public function getHasilPekerja($id){
        $this->db->where("sp_id",$id);
        return $this->db->get(DBVIEW_SCREENING_HASIL_PEKERJA)->row();
    }

    public function getHasilUmum($id){
        $this->db->where("sc_id",$id);
        return $this->db->get(DBVIEW_SCREENING_HASIL_UMUM)->row();
    }


    public function getHasilSekolahDewasa($id){
        $this->db->where("sds_id",$id);
        return $this->db->get(DBVIEW_SCREENING_HASIL_DEWASASEKOLAH)->row();
    }

    public function getHasilSekolahAnak($id){
        $this->db->where("sas_id",$id);
        return $this->db->get(DBVIEW_SCREENING_HASIL_ANAKSEKOLAH)->row();
    }

    public function getHasilAnak($id){
        $this->db->where("sa_id",$id);
        return $this->db->get(DBVIEW_SCREENING_HASIL_ANAK)->row();
    }

    public function getHistory($username){
        $this->db->where("input_user",$username);
        $this->db->order_by("id","DESC");
        return $this->db->get(DBVIEW_SCREENING_INPUT_HISTORY)->result();
    }


    public function updateScreeningUmum($rdata,$id){
        return $this->db->update(DB_DATASCREENING_UMUM,$rdata,array("sc_dp_id"=>$id));
    }

    public function updateScreeningAnak($rdata,$id){
        return $this->db->update(DB_DATASCREENING_ANAK,$rdata,array("sa_id"=>$id));
    }


    public function updateScreeningPekerja($rdata,$id){
        return $this->db->update(DB_DATASCREENING_PEKERJA,$rdata,array("sp_id"=>$id));
    }

    public function updateScreeningSekolahDewasa($rdata,$id){
        return $this->db->update(DB_DATASCREENING_DEWASASEKOLAH,$rdata,array("sds_id"=>$id));
    }

    public function updateScreeningSekolahAnak($rdata,$id){
        return $this->db->update(DB_DATASCREENING_ANAKSEKOLAH,$rdata,array("sas_id"=>$id));
    }

    public function delDataDasarUmum($id){
        $this->db->where("sdu_id",$id);
        return $this->db->delete(DB_DATADASAR_UMUM);
    }

 







}