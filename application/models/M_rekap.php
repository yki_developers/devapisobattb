<?php
Class M_rekap extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function listHasilScreening($rdata=null){
        if($rdata!=null){
            $this->db->where($rdata);
        }
        $this->db->order_by("id","DESC");

        return $this->db->get(DBVIEW_LIST_REKAP_HASIL)->result();
    }

    public function listHasilPerfaskes($idfaskes){
        $this->db->where("kode_puskesmas",$idfaskes);
        $this->db->order_by("id","DESC");
        return $this->db->get(DBVIEW_LIST_REKAP_HASIL)->result();
    }

    public function listReportTB16($indeks){
        $this->db->where("id_indeks",$indeks);
        return $this->db->get(DBVIEW_REKAP_FORMAT_TB16)->result();

    }

    public function listReportTB16RK($idkader){
        $this->db->where("id_kader",$idkader);
        return $this->db->get(DBVIEW_REKAP_TB16RK)->result();
    }

    public function listKontak($rdata=null){
        if($rdata!=null){
            $this->db->where($rdata);
        }
        $this->db->where("person_id IS NOT NULL");
        //return $this->db->get_compiled_select();

        return $this->db->get(DBVIEW_SITB_KONTAK_LIST)->result();
    }

    public function getlistKontak($q){
        $xstring = explode("~",$q);
        for($i=0;$i<sizeof($xstring);$i++){
            $st = explode(":",$xstring[$i]);
            if(stripos($st[1],"_b_")==""){
            $this->db->where($st[0],$st[1]);
            }else{
                $range = explode("_b_",$st[1]);
                $this->db->where($st[0]." BETWEEN '$range[0]' AND '$range[1]'");
            }
        }
        $this->db->where("person_id IS NOT NULL");
        //return $this->db->get_compiled_select();

        return $this->db->get(DBVIEW_SITB_KONTAK_LIST)->result();
    }
    


    public function getDetailKontak($q){
        $xstring = explode("~",$q);
        for($i=0;$i<sizeof($xstring);$i++){
            $st = explode(":",$xstring[$i]);
            if(stripos($st[1],"_b_")==""){
            $this->db->where($st[0],$st[1]);
            }else{
                $range = explode("_b_",$st[1]);
                $this->db->where($st[0]." BETWEEN '$range[0]' AND '$range[1]'");
            }
        }
        $this->db->where("person_id IS NOT NULL");
        //return $this->db->get_compiled_select();

        return $this->db->get(DBVIEW_SITB_DETAIL_KONTAK)->result();
    }

    


}