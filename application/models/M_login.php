<?php
Class M_login extends CI_Model{
    public function __construct(){
        parent::__construct();
    }
    private function userTable(){
        return "tbl_user_admin";
    }
    private function mobileUser($id=null){
        return "tbl_mobile_users ".$id;
    }

    public function updatePlayerid($rdata,$username){
        return $this->db->update(DB_USER_MOBILE,$rdata,array("email"=>$username));
    }
    public function tokenGet($xkey){
        $this->db->select("CONCAT_WS('_',id,user_id,UUID(),'".$xkey."',level) as token");
        $this->db->where("key",$xkey);
       $q = $this->db->get('rest_keys');
     return $q->row();
       //return $this->db->get_compiled_select();
    }

    public function getUUID(){
        $this->db->select("(select LEFT(UUID(),8) as uid ) as uid");
        return $this->db->get(DB_USER_MOBILE)->row();

    }

public function chUser($email){
    $this->db->select("a.*");
    $this->db->select("'true' as status");
    $this->db->select("(SELECT count(td.idthread) as jml FROM tbl_forum_thread td where td.userid=a.username group by td.userid) as total_thread");
    $this->db->where('email',$email);
    $q = $this->db->get(DB_USER_MOBILE." a");
    return $q->row();  
}


    public function authUser($auth){
        $this->db->select("a.*");
        $this->db->select("'true' as status");
        $this->db->select("(SELECT count(td.idthread) as jml FROM tbl_forum_thread td where td.userid=a.username group by td.userid) as total_thread");
        $this->db->where("(username='".$auth['username']."' or email='".$auth['username']."') and userpassword='".$auth['userpassword']."'");
        $q = $this->db->get(DB_USER_MOBILE." a");
        return $q->row();
       // $ndata = array("playerid"=>$auth['playerid']);



       
        /*
        $update = $this->updatePlayerid($ndata,$data->email);
        if($update){
            return $this->chUser($data->email);
        }else{
            return false;
        }
        */

    }

    public function getKaderId($username){
        $this->db->where("kader_username",$username);
        $q = $this->db->get(DB_DATA_IKON_KADER)->row();
        return $q->kader_id;
    }

    public function getFaskes($username){
        $this->db->where("username",$username);
        $q = $this->db->get(DB_USER_FASKES)->row();
        return $q->kd_fasyankes;
    }

    



    public function getPassword($email){
        $this->db->select("email");
        $this->db->select("left(UUID(),8) as uuid");
        $this->db->select("username");
        $this->db->select("nama_pengguna");
        $this->db->select("userpassword");
        $this->db->select("addtime(now(),'01:00:00') as expired");
        $this->db->where("email",$email);
        $q = $this->db->get(DB_USER_MOBILE);
        if($q->num_rows()>0){
            $user = $q->row();
           $this->db->update(DB_USER_MOBILE,array("req_reset"=>$user->uuid,"expired_req"=>$user->expired),array('email'=>$email));
           return $user;
        }else{
            return false;
        }
    }

    public function getProfile($uuid){
        $this->db->select("email");
        $this->db->select("username");
        $this->db->select("req_reset as uuid");
        $this->db->select("if(expired_req>now(),0,1) as exp");
        $this->db->where("req_reset",$uuid);
        
        $q = $this->db->get(DB_USER_MOBILE);
        if($q->num_rows()>0){
            return $q->row();
        }else{
            return $q= array("email"=>null,"username"=>null,"exp"=>null,"uuid"=>"invalid");
        }
    }

    private function deleteEmail($email){
        return $this->db->delete(DB_USER_MOBILE,array("email"=>$email));
    }

    
    public function email_checking($email){
        $this->db->where("email",$email);
        $q = $this->db->get(DB_USER_MOBILE);
        return $q->num_rows();
    }

    public function username_checking($username){
        $this->db->where("username",$username);
        $q = $this->db->get(DB_USER_MOBILE);
        return $q->num_rows();
    }

    public function email_validation($email){
        $this->db->select("*");
        $this->db->where("email",$email);
        $this->db->where('active','1');
        $q = $this->db->get(DB_USER_MOBILE);
        if($q->num_rows()==0){
            $this->deleteEmail($email);
            return "1";
        }else{
            return "0";
        }
    }

    public function getAuthUser($auth){
        $this->db->group_start();
        $this->db->where("a.username",$auth['username']);
        $this->db->or_where("a.email",$auth['username']);
        $this->db->group_end();
        $this->db->where("a.userpassword",$auth['userpassword']);
        $this->db->select("a.*");
        $this->db->select("c.nama_propinsi,b.nama_kabupaten,d.nama_kecamatan,e.nama_kelurahan,f.nama_fasyankes,g.kd_fasyankes as kdfasyankes");
        $this->db->select("(SELECT count(td.idthread) as jml FROM tbl_forum_thread td where td.userid=a.username group by td.userid) as total_thread");
        $this->db->from(DB_USER_MOBILE." a");
        $this->db->join("mst_kabupaten b","a.idkabupaten=b.idkabupaten","left");
    $this->db->join("mst_propinsi c","a.idpropinsi=c.idpropinsi","left");
    $this->db->join("mst_kecamatan d","a.idkecamatan=d.idkecamatan","left");
    $this->db->join("mst_kelurahan e","a.idkelurahan=e.idkelurahan","left");
    $this->db->join("mst_fasyankes f","a.idfaskes=f.kdfasyankes","left");
    $this->db->join(DB_USER_FASKES." g","a.username=g.username",'left');
        
return $this->db->get()->row();

    }


    public function getAuthUserSSO($auth){
       
        $this->db->where("a.username",$auth['username']);
      
        $this->db->where("a.email",$auth['email']);
        $this->db->select("a.*");
        $this->db->select("c.nama_propinsi,b.nama_kabupaten,d.nama_kecamatan,e.nama_kelurahan,f.nama_fasyankes,g.kd_fasyankes as kdfasyankes");
        $this->db->select("(SELECT count(td.idthread) as jml FROM tbl_forum_thread td where td.userid=a.username group by td.userid) as total_thread");
        $this->db->from(DB_USER_MOBILE." a");
        $this->db->join("mst_kabupaten b","a.idkabupaten=b.idkabupaten","left");
    $this->db->join("mst_propinsi c","a.idpropinsi=c.idpropinsi","left");
    $this->db->join("mst_kecamatan d","a.idkecamatan=d.idkecamatan","left");
    $this->db->join("mst_kelurahan e","a.idkelurahan=e.idkelurahan","left");
    $this->db->join("mst_fasyankes f","a.idfaskes=f.kdfasyankes","left");
    $this->db->join(DB_USER_FASKES." g","a.username=g.username",'left');
        
return //$this->db->get_compiled_select();

$this->db->get()->row();

    }

    
public function detailProfile($username){
    $this->db->select("a.*");
    $this->db->select("c.nama_propinsi,b.nama_kabupaten,d.nama_kecamatan,e.nama_kelurahan,f.nama_fasyankes,g.kd_fasyankes as kdfasyankes,h.kader_id");
    $this->db->select("(SELECT count(td.idthread) as jml FROM tbl_forum_thread td where td.userid=a.username group by td.userid) as total_thread");
    $this->db->from(DB_USER_MOBILE." a");
    $this->db->join("mst_kabupaten b","a.idkabupaten=b.idkabupaten","left");
    $this->db->join("mst_propinsi c","a.idpropinsi=c.idpropinsi","left");
    $this->db->join("mst_kecamatan d","a.idkecamatan=d.idkecamatan","left");
    $this->db->join("mst_kelurahan e","a.idkelurahan=e.idkelurahan","left");
    $this->db->join("mst_fasyankes f","a.idfaskes=f.kdfasyankes","left");
    $this->db->join(DB_USER_FASKES." g","a.username=g.username",'left');
    $this->db->join(DB_DATA_IKON_KADER." h","h.kader_username=a.username","left");
    //$this->db->where("a.username",$username);
    $this->db->where("a.email",$username);
    $q  = $this->db->get();
    return $q->result();
}
    public function selfRegisterUser($data){
        return $this->db->insert(DB_USER_MOBILE,$data);
    }

    public function updateProfile($data,$email){
        $this->db->where('email',$email);
        $this->db->or_where('username',$email);
        return $this->db->update(DB_USER_MOBILE,$data);

    }

   




public function getUserData($username){
    $this->db->where("username",$username);
    $this->db->or_where("email",$username);
    return $this->db->get(DB_USER_MOBILE)->row();
}


    public function username_validation($username){
        $this->db->select("*");
        $this->db->where("username",$username);
        $this->db->or_where("email",$username);
        $q = $this->db->get(DB_USER_MOBILE);
        if($q->num_rows()==0){
            return true;
        }else{
            return false;
        }
    }

    public function nohp_validation($nohp){
        $this->db->select("*");
        $this->db->where("nohp",$nohp);
        $q = $this->db->get($this->userTable());
        if($q->num_rows()==0){
            return true;
        }else{
            return false;
        }
    }

    public function updateOTP($rdata){
        $this->db->where($rdata);
        return $this->db->update(DB_USER_MOBILE,array("otp"=>mt_rand(100000,999999)));
    }

 

    public function registerUser($data){
        
        return $this->db->insert($this->userTable(),$data);
		
    }

    public function checkOTP($rdata){
        $this->db->where("username",$rdata['username']);
        $this->db->or_where("email",$rdata['username']);
        $q = $this->db->get(DB_USER_MOBILE);
        if($q->num_rows()==0){
            return false;
        }else{
            return true;
        }
    }



    public function aktifasi($rdata){
        $this->db->where("username",$rdata['username']);
        $this->db->or_where("email",$rdata['username']);
        return $this->db->update(DB_USER_MOBILE,array("active"=>"1"));
    }

    public function activated($dt){
        $this->db->where("kode_aktifasi",$dt);
        return $this->db->update(DB_USER_MOBILE,array("active"=>"1","web_reg_status"=>"1","kode_aktifasi"=>md5($dt)));
    }

    

    public function getOTP($rdata){
        $this->db->where($rdata);
        return $this->db->get(DB_USER_MOBILE)->row();
    }

    public function statusActive($username){
        $this->db->where("username",$username);
        $this->db->select("active");
        $q = $this->db->get(DB_USER_MOBILE)->row();
        return $q->active;
    }

    public function checkPassword($username,$password){
        $this->db->where("username",$username);
        $this->db->where("userpassword",$password);
        $q = $this->db->get(DB_USER_MOBILE);

        return $q->num_rows();
    }


}