<?php
Class M_podcast extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($rdata){
    return $this->db->insert(DB_DATA_PODCAST,$rdata);
    }


    public function getList(){
        //$this->db->select("podcast_id,podcast_date,podcast_title,podcast_description,podcast_creator");
        $this->db->order_by('podcast_id','DESC');
        return $this->db->get(DB_DATA_PODCAST)->result();
    }

    public function getListByTheme(){
        //$this->db->select("podcast_id,podcast_date,podcast_title,podcast_description,podcast_creator");
        $this->db->order_by('podcast_id','DESC');
       // $this->db->where("podcast_theme_id",$id);
       $this->db->from(DB_DATA_PODCAST);
       $this->db->join(DB_DATA_PODCAST_THEME,"theme_id=podcast_theme_id");
        return $this->db->get()->result();
    }


    public function getDetail($id){
        $this->db->where("podcast_id",$id);
        return $this->db->get(DB_DATA_PODCAST)->row();
    }

    public function checkUserView($username,$id){
        $this->db->where("dpp_username",$username);
        $this->db->where("dpp_podcast_id",$id);
        $this->db->where("date(dpp_date)=CURDATE()");
        $q = $this->db->get(DB_DATA_POIN_PODCAST);
        return $q->num_rows();

    }

    public function updatePoin($rdata){
        return $this->db->insert(DB_DATA_POIN_PODCAST,$rdata);
    }

    public function update($rdata,$id){
        return $this->db->update(DB_DATA_PODCAST,$rdata,array("podcast_id"=>$id));
    }
    public function delete($id){
        return $this->db->delete(DB_DATA_PODCAST,array("podcast_id"=>$id));
    }


    public function totalEpisode($id){
        $this->db->where("podcast_theme_id",$id);
        $q = $this->db->get(DB_DATA_PODCAST);
        return $q->num_rows();
    }
    public function insertTheme($rdata){
        return $this->db->insert(DB_DATA_PODCAST_THEME,$rdata);
    }

    public function getListTheme(){
        return $this->db->get(DB_DATA_PODCAST_THEME)->result();
    }

    public function getDetailTheme($id){
        $this->db->where("theme_id",$id);
        return $this->db->get(DB_DATA_PODCAST_THEME)->row();
    }

    public function updateTheme($rdata,$id){
        return $this->db->update(DB_DATA_PODCAST_THEME,$rdata,array("theme_id"=>$id));
    }

    public function deleteTheme($id){
        return $this->db->delete(DB_DATA_PODCAST_THEME,array("theme_id"=>$id));
    }


    public function insertComment($rdata){
        return $this->db->insert(DB_DATA_PODCAST_COMMENT,$rdata);
    }

    public function getComment($id){
        $this->db->where("comment_pc_podcast_id",$id);
        $this->db->order_by("comment_pc_id","DESC");
        return $this->db->get(DB_DATA_PODCAST_COMMENT)->result();
    }

    public function deleteComment($id){
        return $this->db->delete(DB_DATA_PODCAST_COMMENT,array("comment_pc_id"=>$id));
    }

    public function detailComment($id){
        $this->db->where("comment_pc_id",$id);
        return $this->db->get(DB_DATA_PODCAST_COMMENT)->row();
    }


    public function totalComment($id){
        $this->db->where("comment_pc_podcast_id",$id);
        $q = $this->db->get(DB_DATA_PODCAST_COMMENT);
        return $q->num_rows();
    }


}