<?php
Class M_auth extends CI_Model{
    private $tblXkey;
    
    public function __construct(){
        parent::__construct();
        $this->tblXkey = "rest_keys";
    }

    public function getXkey(){
        $this->db->select("LEFT(UUID(),8) as xkey");
        $q = $this->db->get($this->tblXkey);
        return $q->row();
    }

    public function newXkey($rdata){
        return $this->db->insert($this->tblXkey,$rdata);
    }

    public function deleteXkey($xkey){
        return $this->db->delete($this->tblXkey,array("key"=>$xkey));
    }

    public function getListXkey($xkey){
        $this->db->where("key !=",$xkey);
        return $this->db->get($this->tblXkey)->result();
    }

    public function getToken($xkey){
        $this->db->select("CONCAT_WS('_',id,user_id,UUID(),'".$xkey."',level) as token");
        $this->db->where("key",$xkey);
       $q = $this->db->get('rest_keys');
     return $q->row();
    }

    
}