<?php
Class M_fasyankes extends CI_Model{
    public function __construct(){
        parent::__construct();
    }
	private function tableFaskes($id=null){
		return "mst_fasyankes ".$id;
	}


    public function detailFasyankes($idfasyankes){
		$this->db->select("a.*");
        $this->db->from($this->tableFaskes("a"));
		$this->db->where("a.idfasyankes",$idfasyankes);
		$q = $this->db->get();
		return $q->row();
	}
	
	public function listFasyankes($idkabupaten){
		
		$this->db->select("a.*");
		$this->db->from($this->tableFaskes("a"));
		$this->db->where("a.idkabupaten",$idkabupaten);
		//$this->db->limit('50',$start);
	$q = $this->db->get();
	return $q->result();	
	}
    

	public function filterFasyankes($rdata=null){
		if($rdata!=null){
			$this->db->where($rdata);
		}
		$this->db->select(DB_MASTER_FASKES.".*");
		$this->db->select("nama_propinsi,nama_kabupaten,nama_kecamatan");
		$this->db->join(DB_MASTER_PROPINSI,DB_MASTER_FASKES.".idpropinsi=".DB_MASTER_PROPINSI.".idpropinsi");
		$this->db->join(DB_MASTER_KABUPATEN,DB_MASTER_FASKES.".idkabupaten=".DB_MASTER_KABUPATEN.".idkabupaten");
		$this->db->join(DB_MASTER_KECAMATAN,DB_MASTER_FASKES.".idkecamatan=".DB_MASTER_KECAMATAN.".idkecamatan");
		return $this->db->get(DB_MASTER_FASKES)->result();
	}

	public function listFasyankesByKecamatan($idkec){
		
		$this->db->where("idkecamatan",$idkec);
		//$this->db->limit('50',$start);
	$q = $this->db->get(DB_MASTER_FASKES);
	return $q->result();	
	}


    public function searchFasyankes($search,$page=1){
		$start = ($page-1)*50;
		$this->db->select("a.*");
		$this->db->select("c.jenis_fasyankes");
		$this->db->select("d.nama_kecamatan");
		$this->db->select("b.nama_kabupaten");
		$this->db->select("e.nama_propinsi");
		$this->db->from($this->tableFaskes("a"));
		$this->db->join("mst_kabupaten b","a.idkabupaten=b.idkabupaten");
		$this->db->join("mst_jenis_fasyankes c","c.idjenis=a.jenis_fasyankes");
		$this->db->join("mst_kecamatan d","d.idkecamatan=a.idkecamatan","left");
		$this->db->join("mst_propinsi e","e.idpropinsi=a.idpropinsi");
		//$this->db->where("a.idkabupaten",$idkabupaten);
		$this->db->like('a.nama_fasyankes',$search);
		$this->db->or_like('d.nama_kecamatan',$search);
		$this->db->or_like('b.nama_kabupaten',$search);
		$this->db->or_like('a.kdfasyankes',$search);
		$this->db->limit('50',$start);
	$q = $this->db->get();
	return $q->result();	
	//return $this->db->get_compiled_select();
    }

    public function updateFaskes($data,$id){
		return $this->db->update($this->tableFaskes(),$data,array("idfasyankes"=>$id));
    }
    
    
}