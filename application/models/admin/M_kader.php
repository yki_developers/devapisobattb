<?php
Class M_kader extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($rdata){
         $this->db->insert(DB_DATA_IKON_KADER,$rdata);
         return $this->db->insert_id();
    }

    public function insertUserMobile($rdata){
        return $this->db->insert(DB_USER_MOBILE,$rdata);
    }
    public function checkUsername($username){
        $this->db->where("username",$username);
        $q = $this->db->get(DB_USER_MOBILE);
        return $q->num_rows();
    }

    public function checkEmail($email){
        $this->db->where("email",$email);
        $q = $this->db->get(DB_USER_MOBILE);
        return $q->num_rows();

    }


    public function getListKader($rdata=null){
        if($rdata!=null){
            $this->db->where($rdata);
        }
        
        $this->db->where("kader_aktifasi","1");
        $this->db->from(DB_DATA_IKON_KADER);
        $this->db->join(DB_MASTER_FASKES." faskes","kader_puskesmas=faskes.kdfasyankes");
        $this->db->join(DB_MASTER_PROPINSI." prop",'prop.idpropinsi=kader_propinsi','left');
        $this->db->join(DB_MASTER_KABUPATEN." kab","kab.idkabupaten=kader_kabupaten","left");
        $this->db->join(DBVIEW_DATA_KADER_LINK,"link_kader_id=kader_id");

        return $this->db->get()->result();
    }



    
    public function deleteMobileAkun($username){
        return $this->db->delete(DB_USER_MOBILE,array("username"=>$username));
    }

    public function deleteUser($id){
        return $this->db->delete(DB_DATA_IKON_KADER,array("kader_id"=>$id));
    }

    public function detail($id){
        $this->db->where("kader_id",$id);
        return $this->db->get(DB_DATA_IKON_KADER)->row();
    }

    public function update($rdata,$id){
        return $this->db->update(DB_DATA_IKON_KADER,$rdata,array("kader_id"=>$id));
    }

    public function updatePassword($rdata,$username){
        $this->db->where("username",$username);
        return $this->db->update(DB_USER_MOBILE,$rdata);
    }

    public function getComboListKader($rdata=null){

        if($rdata!=null){
            $this->db->where($rdata);
        }

    $this->db->select("kader_id,kader_name,kader_username");
    return $this->db->get(DB_DATA_IKON_KADER)->result();

    }






}