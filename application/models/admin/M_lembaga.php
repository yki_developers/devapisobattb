<?php
Class M_lembaga extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($rdata){
        return $this->db->insert(DB_MASTER_CSO,$rdata);
    }

    public function getListLembaga($rdata=null){
        
        if($rdata!=null){
            $this->db->where($rdata);
        }
        $this->db->order_by("cso_id","DESC");
        $this->db->from(DB_MASTER_CSO);
        $this->db->join(DB_MASTER_PROPINSI,'idpropinsi=cso_propinsi','left');
        $this->db->join(DB_MASTER_KABUPATEN,"idkabupaten=cso_kabupaten","left");
        return $this->db->get()->result();
    }

    public function detail($id){
        $this->db->where("cso_id",$id);
        return $this->db->get(DB_MASTER_CSO)->row();
    }

    public function update($rdata,$id){
        return $this->db->update(DB_MASTER_CSO,$rdata,array("cso_id"=>$id));
    }

    public function delete($idlembaga){
        return $this->db->delete(DB_MASTER_CSO,array("cso_id"=>$idlembaga));
    }


public function userLembaga($rdata){
    return $this->db->insert(DB_USER_LEMBAGA,$rdata);
}
    public function addUser($rdata){
        return $this->db->insert(DB_USER_ADMIN,$rdata);
    }

    public function delUser($username){
        return $this->db->delete(DB_USER_ADMIN,array("username"=>$username));
    }


    public function listUser($rdata=null){
       // $this->db->select("adm.*,prop.nama_propinsi,kab.nama_kabupaten");
        if($rdata!=null){
            $this->db->where($rdata);
        }    


        /*$this->db->from(DB_USER_ADMIN." adm");
        $this->db->join(DB_USER_LEMBAGA,"cso_username=username");
       // $this->db->join(DB_MASTER_CSO,"cso_id=cso_master_id");
        $this->db->join(DB_MASTER_PROPINSI." prop",'prop.idpropinsi=adm.idpropinsi','left');
        $this->db->join(DB_MASTER_KABUPATEN." kab","kab.idkabupaten=adm.idkabupaten","left");
        */
        $this->db->join(DB_USER_LEMBAGA,"cso_username=username");
        return $this->db->get(DBVIEW_LIST_USERADMIN)->result();
    }




    
}