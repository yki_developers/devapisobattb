<?php
Class M_organisasi extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    private function mstOrganisasi($id=null){
        return "mst_organisasi ".$id;
    }
    private function mstPropinsi($id=null){
        return "mst_propinsi ".$id;
    }
    private function mstKabupaten($id=null){
        return "mst_kabupaten ".$id;
    }

    public function addOrganisasi($rdata){
        return $this->db->insert($this->mstOrganisasi(),$rdata);
    }

    public function listOrganisasi(){
        $this->db->select("a.*");
        $this->db->select("b.nama_kabupaten");
        $this->db->select("c.nama_propinsi");
        $this->db->from($this->mstOrganisasi("a"));
        $this->db->join($this->mstKabupaten("b"),"a.idkabupaten=b.idkabupaten");
        $this->db->join($this->mstPropinsi("c"),"a.idpropinsi=c.idpropinsi");
        return $this->db->get()->result();
    }

    public function updateOrganisasi($rdata,$id){
        return $this->db->update($this->mstOrganisasi(),$rdata,array("idorganisasi"=>$id));

    }

    public function deleteOrganisasi($id){
        return $this->db->delete($this->mstOrganisasi(),array("idorganisasi"=>$id));

    }

    public function detailOrganisasi($id){
        $this->db->select("a.*");
        $this->db->select("b.nama_kabupaten");
        $this->db->select("c.nama_propinsi");
        $this->db->from($this->mstOrganisasi("a"));
        $this->db->join($this->mstKabupaten("b"),"a.idkabupaten=b.idkabupaten");
        $this->db->join($this->mstPropinsi("c"),"a.idpropinsi=c.idpropinsi");
        $this->db->where("idorganisasi",$id);
        return $this->db->get()->row();
    }

    public function searchOrganisasi($id){
        $this->db->select("a.*");
        $this->db->select("b.nama_kabupaten");
        $this->db->select("c.nama_propinsi");
        $this->db->from($this->mstOrganisasi("a"));
        $this->db->join($this->mstKabupaten("b"),"a.idkabupaten=b.idkabupaten");
        $this->db->join($this->mstPropinsi("c"),"a.idpropinsi=c.idpropinsi");
        $this->db->like("a.nama_organisasi",$id);
        $this->db->or_like("a.nama_pendek",$id);
        return $this->db->get()->result();
    }
}