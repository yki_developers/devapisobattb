<?php
Class M_login extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    private function userTable($id=null){
        return "tbl_user_admin ".$id;
    }

    private function userOrganisasi($id=null){
        return "ref_user_organisasi ".$id;
    }

    private function userFasyankes($id=null){
        return "ref_user_fasyankes ".$id;
    }

    private function mstOrganisasi($id=null){
        return "mst_organisasi ".$id;
    }
    private function mstFasyankes($id=null){
        return "mst_fasyankes ".$id;
    }

    public function getLembaga($userid){
        $this->db->select("*");
        $this->db->from(DB_USER_ADMIN);
        $this->db->where("username",$userid);
        $this->db->join(DB_USER_LEMBAGA,"username=cso_username");
        $this->db->join(DB_MASTER_CSO,"cso_master_id=cso_id");
        return $this->db->get()->row();
    }

    public function getOrganisasi($userid){
        $this->db->select("a.*");
        $this->db->select("b.superuser");
        $this->db->select("c.idorganisasi");
        $this->db->select("c.nama_organisasi");
        $this->db->from($this->userTable("a"));
        $this->db->where("a.username",$userid);
        $this->db->join($this->userOrganisasi("b"),"a.username=b.username");
        $this->db->join($this->mstOrganisasi("c"),"b.idorganisasi=c.idorganisasi");
        return $this->db->get()->row();
    }

    public function getFasyankes($userid){
        $this->db->select("a.*");
        $this->db->select("b.superuser");
        $this->db->select("c.kdfasyankes,c.idfasyankes");
        $this->db->select("c.nama_fasyankes");
        $this->db->where("a.username",$userid);
        $this->db->from($this->userTable("a"));
        $this->db->join($this->userFasyankes("b"),"a.username=b.username");
        $this->db->join($this->mstFasyankes("c"),"b.kd_fasyankes=c.kdfasyankes");
        return $this->db->get()->row();
    }

    public function getUserPdp($userid){
        $this->db->where("username_pdp",$userid);
        
       // $this->db->join(REF_USER_PDP,"username_pdp=username");
        $this->db->join(DB_MASTER_FASKES_PDP,"kode_pdp=pdp_id");
        $this->db->join(DB_USER_ADMIN,"username=username_pdp");
        return $this->db->get(REF_USER_PDP)->row();
    }

    public function getUserID($auth){
        $this->db->select("username");
        $this->db->select("usergroup");
        $this->db->where("username='".$auth['username']."'  and userpassword='".$auth['userpassword']."'");
        //return $this->db->get_compiled_select($this->userTable());
        return $this->db->get($this->userTable())->row();
    }

    public function getAdmin($userid){
        $this->db->select("*");
        $this->db->where("username",$userid);
        $this->db->or_where("email",$userid);
        
        return $this->db->get($this->userTable())->row();

    }

    public function getDataKerjasama($userid){
        $this->db->where("program_token",$userid);
        $this->db->join(DB_USER_ADMIN,"username=program_token");
        return $this->db->get(DB_MASTER_KERJASAMA)->row();
    }

    public function getLogin($authUSer){
        if($q = $this->getUserID($authUSer)){
        //return $authUSer;
        
        if($q->usergroup=='3'){
            return $this->getFasyankes($q->username);

        }elseif($q->usergroup=='4'){
            return $this->getOrganisasi($q->username);

        }elseif($q->usergroup=='2' OR $q->usergroup=='1' OR $q->usergroup=='6'){
            return $this->getAdmin($q->username);
        }elseif($q->usergroup=='7' || $q->usergroup=='8' || $q->usergroup=='9'){
            return $this->getLembaga($q->username);
        }elseif($q->usergroup=='11'){
            return $this->getUserPdp($q->username);
        }elseif($q->usergroup=='12'){
            return $this->getDataKerjasama($q->username);
        }
    }else{
        return false;
    }
    }




}