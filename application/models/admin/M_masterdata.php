<?php
Class M_masterdata extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    private function mstJenisFasyankes($id=null){
        return "mst_jenis_fasyankes ".$id;
    }

    private function mstKepemilikan(){
        return "mst_jenis_kepemilikan";
    }

    public function getKepemilikan(){
        $q = $this->db->get($this->mstKepemilikan());
        return $q->result();
    }

    public function addKepemilikan($rdata){
        return $this->db->insert($this->mstKepemilikan(),$rdata);
    }

    public function delKepemilikan($rdata){
        return $this->db->delete($this->mstKepemilikan(),array("idkepemilikan"=>$rdata));
    }





    public function getJenisFaskes(){
        $q=$this->db->get($this->mstJenisFasyankes());
        return $q->result();

    }

    public function addJenisFaskes($rdata){
        return $this->db->insert($this->mstJenisFasyankes(),$rdata);
    }

    public function delJenisFaskes($rdata){
        return $this->db->delete($this->mstJenisFasyankes(),array("idjenis"=>$rdata));
    }






}