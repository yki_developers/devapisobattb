<?php
Class M_users extends CI_Model{
    public function __construct(){
        parent::__construct();
        
    }
    

    private function userTable($id=null){
        return "tbl_user_admin ".$id;
    }
    private function userGroup($id=null){
        return "tbl_user_group ".$id;
    }

    private function userMobile($id=null){
        return "tbl_mobile_users ".$id;
    }

    private function mstPropinsi($id=null){
        return "mst_propinsi ".$id;
    }

    private function mstKabupaten($id=null){
        return "mst_kabupaten ".$id;
    }

    private function mstKecamatan($id=null){
        return "mst_kecamatan ".$id;
    }

    private function mstKelurahan($id=null){
        return "mst_kelurahan ".$id;
    }

    private function userFasyankes($id=null){
        return "ref_user_fasyankes ".$id;
    }

    private function userOrganisasi($id=null){
        return "ref_user_organisasi ".$id;
    }

    
    public function checkUsername($username){
        $this->db->where("username",$username);
        $q = $this->db->get(DB_USER_MOBILE);
        return $q->num_rows();
    }

    public function checkEmail($email){
        $this->db->where("email",$email);
        $q = $this->db->get(DB_USER_MOBILE);
        return $q->num_rows();

    }


    public function addUserOrganisasi($userdata){
        return $this->db->insert($this->userOrganisasi(),$userdata);
    }

    public function addUserFasyankes($userdata){
        return $this->db->insert($this->userFasyankes(),$userdata);
    }

    public function addUserMobile($userdata){
        return $this->db->insert($this->userMobile(),$userdata);
    }


    public function countAll(){
        $this->db->where('usergroup','2');
        $this->db->or_where('usergroup','1');
        $this->db->or_where('usergroup','6');
        return $this->db->get($this->userTable())->num_rows();
    }


    public function getuserAdmin(){
        $this->db->select("a.*");
        $this->db->where('usergroup','1');
        $this->db->or_where('usergroup','2');
        $this->db->or_where('usergroup','6');
        $this->db->order_by("a.tglInput","DESC");
        $q = $this->db->get($this->userTable("a"));
        return $q->result();
    }

    public function adminGroup(){
        $this->db->select("a.*");
        $this->db->where('a.idgroup','1');
        $this->db->or_where('a.idgroup','2');
        $this->db->or_where('a.idgroup','6');
        $q = $this->db->get($this->userGroup("a"));
        return $q->result();
    }


    public function email_validation($email){
        $this->db->select("*");
        $this->db->where("email",$email);
        $q = $this->db->get($this->userTable());
        if($q->num_rows()==0){
            return true;
        }else{
            return false;
        }
    }



    public function username_validation($username){
        $this->db->select("*");
        $this->db->where("username",$username);
        $q = $this->db->get($this->userTable());
        if($q->num_rows()==0){
            return true;
        }else{
            return false;
        }
    }




    

    public function email_mobile($email){
        $this->db->select("*");
        $this->db->where("email",$email);
        $q = $this->db->get($this->userMobile());
        if($q->num_rows()==0){
            return true;
        }else{
            return false;
        }
    }
  

    public function addUser($rdata){
        return $this->db->insert($this->userTable(),$rdata);
    }

  

  
    public function listUsers($page="1",$size="50",$order="ASC"){
        $start = ($page-1)*$size;
        $this->db->select("a.*,b.nama_propinsi,c.nama_kabupaten");
        $this->db->where('usergroup','1');
        $this->db->or_where('usergroup','2');
        $this->db->or_where('usergroup','6');
        $this->db->limit($size,$start);
        $this->db->order_by("a.tglInput",$order);
        $this->db->join(DB_MASTER_PROPINSI." b","b.idpropinsi=a.idpropinsi","left");
        $this->db->join(DB_MASTER_KABUPATEN." c","c.idkabupaten=a.idkabupaten","left");
        $q=$this->db->get($this->userTable("a"));
        return $q->result();
    }


    public function listUsersProp($prop,$page="1",$size="50",$order="ASC"){
        $start = ($page-1)*$size;
        $this->db->select("a.*,b.nama_propinsi,c.nama_kabupaten");


        $this->db->group_start();
        $this->db->where('usergroup','2');
        $this->db->or_where('usergroup','6');
        $this->db->group_end();
        $this->db->where("a.idpropinsi",$prop);
        $this->db->limit($size,$start);
        $this->db->order_by("a.tglInput",$order);
        $this->db->join(DB_MASTER_PROPINSI." b","b.idpropinsi=a.idpropinsi","left");
        $this->db->join(DB_MASTER_KABUPATEN." c","c.idkabupaten=a.idkabupaten","left");
        $q=$this->db->get($this->userTable("a"));
        return $q->result();
    }

    public function countProp($prop){
        $this->db->where("idpropinsi",$prop);
        $this->db->where('usergroup','2');
        $this->db->or_where('usergroup','6');
        return $this->db->get($this->userTable())->num_rows();
    }


    public function listUsersKab($kab,$page="1",$size="50",$order="ASC"){
        $start = ($page-1)*$size;
        $this->db->select("a.*,b.nama_propinsi,c.nama_kabupaten");
        $this->db->where('usergroup','6');
        $this->db->where("c.idkabupaten",$kab);
        $this->db->limit($size,$start);
        $this->db->order_by("a.tglInput",$order);
        $this->db->join(DB_MASTER_PROPINSI." b","b.idpropinsi=a.idpropinsi","left");
        $this->db->join(DB_MASTER_KABUPATEN." c","c.idkabupaten=a.idkabupaten","left");
        $q=$this->db->get($this->userTable("a"));
        return $q->result();
    }

    public function countKab($kab){
        $this->db->where("idkabupaten",$kab);
        $this->db->where('usergroup','6');
        return $this->db->get($this->userTable())->num_rows();
    }


    public function updateUser($rdata,$username){
        return $this->db->update($this->userTable(),$rdata,$username);


    }

    public function delUser($username){
        if($this->db->delete($this->userTable(),$username)){
            return true;
        }else{
            return false;
        }

    }


    public function detailUser($username){
        $this->db->select("a.*");
        //$this->db->select("b.*");
        //$this->db->select("c.*");
        $this->db->where("a.username",$username);
        $this->db->from($this->userTable("a"));
        $this->db->join($this->userFasyankes("b"),"a.username=b.username","left");
        $this->db->join($this->userOrganisasi("c"),"c.username=a.username","left");
        return $this->db->get()->row();
    }

    public function orgUser($id){
        $this->db->select("a.*");
        $this->db->select("c.*");
        $this->db->where($id);
        $this->db->order_by("a.tglInput","DESC");
        $this->db->from($this->userTable("a"));
        $this->db->join($this->userOrganisasi("c"),"c.username=a.username");
        return $this->db->get()->result();
    }
    
    public function faskesUser($id){
        $this->db->select("a.*");
        $this->db->select("c.*");
        $this->db->select("d.nama_fasyankes,d.idfasyankes");
        $this->db->where($id);
        $this->db->order_by("a.tglInput","DESC");
        $this->db->from($this->userTable("a"));
        $this->db->join($this->userFasyankes("c"),"c.username=a.username");
        $this->db->join(DB_MASTER_FASKES." d","c.kd_fasyankes=d.kdfasyankes");
        return $this->db->get()->result();
    }


    public function adminFaskes($idpropinsi,$idkabupaten,$page='1',$size='50'){
        $start = ($page-1)*$size;
        $this->db->select("a.*");
        $this->db->select("c.*");
        $this->db->select("d.nama_fasyankes,d.idfasyankes,e.nama_propinsi,f.nama_kabupaten");
        if($idpropinsi!='all' && $idkabupaten=='all'){
            $this->db->where("a.idpropinsi",$idpropinsi);
        }elseif($idpropinsi!='all' && $idkabupaten!='all'){
            $this->db->where("a.idkabupaten",$idkabupaten);
            $this->db->where("a.idpropinsi",$idpropinsi);
        }
        $this->db->where('usergroup','3');
        $this->db->where('c.superuser','1');
        $this->db->order_by("a.tglInput","DESC");
        $this->db->from($this->userTable("a"));
        $this->db->join($this->userFasyankes("c"),"c.username=a.username");
        $this->db->join(DB_MASTER_FASKES." d","c.kd_fasyankes=d.kdfasyankes");
        $this->db->join(DB_MASTER_PROPINSI." e","e.idpropinsi=a.idpropinsi","left");
        $this->db->join(DB_MASTER_KABUPATEN." f","f.idkabupaten=a.idkabupaten","left");
        $this->db->limit($size,$start);
        return $this->db->get()->result();
    }

    public function countAdmFaskes($idpropinsi,$idkabupaten){
        $this->db->where('usergroup','3');
        if($idpropinsi!='all' && $idkabupaten=='all'){
            $this->db->where("a.idpropinsi",$idpropinsi);
        }elseif($idpropinsi!='all' && $idkabupaten!='all'){
            $this->db->where("a.idkabupaten",$idkabupaten);
            $this->db->where("a.idpropinsi",$idpropinsi);
        }
        $this->db->where('c.superuser','1');
        $this->db->join($this->userFasyankes("c"),"c.username=a.username");
        return $this->db->get($this->userTable("a"))->num_rows();
    }




    public function adminKomunitas($page='1',$size='50'){
        $start = ($page-1)*$size;
        $this->db->select("a.*");
        $this->db->select("c.*");
        $this->db->select("e.nama_propinsi,f.nama_kabupaten");
       /* if($idpropinsi!='all' && $idkabupaten=='all'){
            $this->db->where("a.idpropinsi",$idpropinsi);
        }elseif($idpropinsi!='all' && $idkabupaten!='all'){
            $this->db->where("a.idkabupaten",$idkabupaten);
            $this->db->where("a.idpropinsi",$idpropinsi);
        }*/
        $this->db->where('usergroup','4');
        $this->db->where('c.superuser','1');
        $this->db->order_by("a.tglInput","DESC");
        $this->db->from($this->userTable("a"));
        $this->db->join($this->userOrganisasi("c"),"c.username=a.username");
        $this->db->join(DB_MASTER_PROPINSI." e","e.idpropinsi=a.idpropinsi","left");
        $this->db->join(DB_MASTER_KABUPATEN." f","f.idkabupaten=a.idkabupaten","left");
        $this->db->limit($size,$start);
        return $this->db->get()->result();
    }

    public function countKomunitas(){
        $this->db->where('usergroup','4');
        $this->db->where('c.superuser','1');
        $this->db->join($this->userOrganisasi("c"),"c.username=a.username");
        return $this->db->get($this->userTable("a"))->num_rows();
    }

    public function checkUserSkrining($username){
        $this->db->select("count(sdu_id) as skrining");
        $this->db->select("max(sc_date) as last_date");
        $this->db->where("input_user",$username);
        return $this->db->get(DBVIEW_SCREENING_UMUM)->row();
    }



}