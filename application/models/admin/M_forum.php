<?php
Class M_forum extends CI_Model{

    public function __construct(){
        parent::__construct();
    }
    private function tblKategori($alias=null){
        return "tbl_forum_kategori ".$alias;
    }
    private function tblForum($alias=null){
        return "tbl_forum_thread ".$alias;
    }

    private function tblReply($alias=null){
        return "tbl_forum_balasan_thread ".$alias;
    }


    public function getForum($idorganisasi){

      
        $this->db->select("a.*");
        $this->db->select("b.kategori");
        $this->db->select("c.nama_organisasi");
        $this->db->from($this->tblForum("a"));
        $this->db->join($this->tblKategori("b"),"a.idkategori=b.idkategori");
        $this->db->join("mst_organisasi c","c.idorganisasi=a.idorganisasi");
        $this->db->where("a.idorganisasi",$idorganisasi);
        $this->db->order_by("a.idthread","DESC");
        return $this->db->get()->result();

    }


    public function getReply($idthread){
        $this->db->select("*");
        $this->db->where("idthread",$idthread);
        return $this->db->get($this->tblReply())->result();
        //return $this->db->get_compiled_select($this->tblReply());
    }



    public function updateForum($rdata,$idthread){
        return $this->db->update($this->tblForum(),$rdata,array("idthread"=>$idthread));
    }

}
