<?php
Class M_monitoring extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        define("DB_DATA_ARTIKEL","tbl_cont_infotb");
        define("DB_REKAP_PENGGUNA","view_rekap_pengguna_mobile");
    }

    public function getTanggal()
    {
        $this->db->select("view_tanggal");
        $this->db->select("DATE_FORMAT(view_tanggal,'%Y%m%d') as tgl_indeks");
        $this->db->where("view_tanggal between DATE_SUB(CURDATE(), INTERVAL 30 DAY) and CURDATE()");
        $this->db->group_by("view_tanggal");
        return $this->db->get(DB_DATA_VIEWER_ARTIKEL)->result();
    }

    public function getListArtikel(){
        $this->db->select("idcontent");
        $this->db->select("judul_artikel");
        $this->db->order_by("idcontent","DESC");
        $this->db->limit(100,0);
        return $this->db->get(DB_DATA_ARTIKEL)->result();
    }

    public function getListViewArtikel($tgl){
        $this->db->where("view_tanggal",$tgl);
        $this->db->select("view_tanggal,view_artikel_id");
        $this->db->select_sum("view_jumlah","jumlah_view");
        $this->db->group_by("view_artikel_id");
        $this->db->from(DB_DATA_VIEWER_ARTIKEL);   
        return $this->db->get()->result();
    }

    public function getRekapPengguna(){
        return $this->db->get(DB_REKAP_PENGGUNA)->result();
    }



}