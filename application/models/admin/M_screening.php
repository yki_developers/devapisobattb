<?php
Class M_screening extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($rdata)
    {
         return $this->db->insert(DB_MASTER_KERJASAMA,$rdata);
    }

    public function addUserKerjasama($rdata){
        return $this->db->insert(DB_USER_ADMIN,$rdata);
    }



    public function getList(){
        return $this->db->get(DB_MASTER_KERJASAMA)->result();
    }

    public function getListByUser($username){
        $this->db->where("program_users",$username);
        return $this->db->get(DB_MASTER_KERJASAMA)->result();
    }

    public function getToken(){
        return $this->db->select("left(UUID(),8) as token")->get()->row();
    }

    public function detail($id){
        $this->db->where("program_token",$id);
        return $this->db->get(DB_MASTER_KERJASAMA)->row();
    }

    public function update($rdata,$id){
        $this->db->where("program_token",$id);
        return $this->db->update(DB_MASTER_KERJASAMA,$rdata);
    }

    public function delete($id){
        return $this->db->delete(DB_MASTER_KERJASAMA,array("program_token"=>$id));
    }





    public function getdataScreeningPekerja($token=null){
        if($token!=null){
            $this->db->where("sp_token",$token);
        }
        return $this->db->get(DBVIEW_SCREENING_PEKERJA)->result();
    }

    public function getdataScreeningDewasaSekolah($token=null){
        if($token!=null){
            $this->db->where("sds_token",$token);
        }
        return $this->db->get(DBVIEW_SCREENING_DEWASA_SEKOLAH)->result();
    }


    public function getdataScreeningAnakSekolah($token=null){
        if($token!=null){
            $this->db->where("sas_token",$token);
        }
        return $this->db->get(DBVIEW_SCREENING_ANAK_SEKOLAH)->result();
    }

    public function getdataHasilScreeningPekerja($token=null){
        if($token!=null){
            if(isset($token['select_or'])){
                $rdata = $token['select_or'];
                $this->db->group_start();
                $this->db->where($rdata);
                $this->db->group_end();
                unset($token['select_or']);
            }
   
   
           
            
   
               $this->db->where($token);
               
           }
        return $this->db->get(DBVIEW_SCREENING_HASIL_PEKERJA)->result();
    }


    public function getdataHasilScreeningAnakSekolah($token=null){
        if($token!=null){
            if(isset($token['select_or'])){
                $rdata = $token['select_or'];
                $this->db->group_start();
                $this->db->where($rdata);
                $this->db->group_end();
                unset($token['select_or']);
            }
   
   
           
            
   
               $this->db->where($token);
               
           }
        return $this->db->get(DBVIEW_SCREENING_HASIL_ANAKSEKOLAH)->result();
    }


    public function getdataHasilScreeningDewasaSekolah($token=null){
        if($token!=null){
            if(isset($token['select_or'])){
                $rdata = $token['select_or'];
                $this->db->group_start();
                $this->db->where($rdata);
                $this->db->group_end();
                unset($token['select_or']);
            }
   
   
           
            
   
               $this->db->where($token);
               
           }
        return $this->db->get(DBVIEW_SCREENING_HASIL_DEWASASEKOLAH)->result();
    }


    public function getdataHasilScreeningAnak($token=null){
        if($token!=null){
            if(isset($token['select_or'])){
                $rdata = $token['select_or'];
                $this->db->group_start();
                $this->db->where($rdata);
                $this->db->group_end();
                unset($token['select_or']);
            }
   
   
           
            
   
               $this->db->where($token);
               
           }
        return $this->db->get(DBVIEW_SCREENING_HASIL_ANAK)->result();
    }

    public function getdataHasilScreeningUmum($token=null){
        if($token!=null){
         if(isset($token['select_or'])){
             $rdata = $token['select_or'];
             $this->db->group_start();
             $this->db->where($rdata);
             $this->db->group_end();
             unset($token['select_or']);
         }


        
         

            $this->db->where($token);
            
        }
        $this->db->where("kode_pdp IS NULL");
        return $this->db->get(DBVIEW_SCREENING_HASIL_UMUM)->result();
    }

    public function getdataHasilScreeningODHIV($token=null){
        if($token!=null){
            $this->db->where($token);
        }
        $this->db->where("kode_pdp IS NOT NULL");
        $this->db->join(DB_MASTER_FASKES_PDP,"pdp_id=kode_pdp");
        return $this->db->get(DBVIEW_SCREENING_HASIL_UMUM)->result();
    }



    public function getdataHasilScreeningSekolah($token=null){
        if($token!=null){
            if(isset($token['select_or'])){
                $rdata = $token['select_or'];
                $this->db->group_start();
                $this->db->where($rdata);
                $this->db->group_end();
                unset($token['select_or']);
            }
   
   
           
            
   
               $this->db->where($token);
               
           }
        return $this->db->get(DBVIEW_SCREENING_HASIL_SEKOLAH)->result();
    }

    public function getDataFilter($rdata){
        if($rdata!=null){
            $this->db->where($rdata);
        }
        $this->db->select(DB_MASTER_KERJASAMA.".*,".DB_MASTER_FASKES.".nama_fasyankes");
        $this->db->from(DB_MASTER_KERJASAMA);
        $this->db->join(DB_MASTER_FASKES,"program_puskesmas=kdfasyankes","left");
        return $this->db->get()->result();
    }

    
   
    
}