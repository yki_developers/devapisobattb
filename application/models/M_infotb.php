<?php
Class M_infotb extends CI_Model{
    public function __construct(){
       
     parent::__construct();
    }


    private function mstKategori($id=null){
        return "mst_cont_kategori ".$id;
    }
    private function tblInfotb($id=null){
        return "tbl_cont_infotb ".$id;

    }
    private function tblViewer($id=null){
        return "tbl_cont_viewer ".$id;
    }
    private function addViewer($count,$id){
        return $this->db->update($this->tblViewer(),$count,array("idcontent"=>$id));
    }

    private function tblRelated($id=null){
        return "tbl_cont_related ".$id;
    }

    private function checkSlug($text){
        $this->db->select("MAX(IF(INSTR(slug,'_'),SUBSTRING_INDEX(slug,'_',-1),0))+1 as n");
        $this->db->like("slug",$text,'after');
        $q = $this->db->get($this->tblInfotb());
        return $q->row();

    }

    public function slug_validation($text){
        $this->db->select("slug");
        $this->db->like("slug",$text,'after');
        $q = $this->db->get($this->tblInfotb());
        $val = $q->num_rows();
        if($val==0){
            return $text;
        }else{
            $n = $this->checkSlug($text);
            return $text."_".$n->n;
        }
    }
    public function addInfotb($rdata){
        if($this->db->insert($this->tblInfotb(),$rdata)){
            $lastid = $this->db->insert_id();
            $this->db->insert(DB_DATA_VIEWER_ARTIKEL,array(
                "view_artikel_id"=>$lastid,
                "view_tanggal"=>date("Y-m-d"),
                "view_jumlah"=>"0"
            ));
            $this->db->insert($this->tblViewer(),array("idcontent"=>$lastid,"jml_viewer"=>"0"));
            return $lastid;
        }else{
            return false;
        }
        
    }

    public function getCountInfotb(){
        return $this->db->count_all_results($this->tblInfotb());
    }

    public function getInfotb($page=1,$size=10,$order = "DESC")
    {
        
        //$tot = $this->getCountInfotb()
        $start = ($page-1)*$size;
        $this->db->select("a.idcontent,a.idcategori,c.kategori,a.judul_artikel,a.headlines,date_format(a.tgl_terbit,'%d/%m/%Y') as tgl_terbit");
        $this->db->select("a.author");
        $this->db->select("a.reviewer");
        $this->db->select("date_format(a.last_edit,'%d/%m/%Y') as last_edit");
        $this->db->select("a.slug");
        $this->db->select("a.foto,a.foto_landscape");
        //$this->db->select("if(isnull(b.jml_viewer),0,b.jml_viewer) as jml_viewer");
        $this->db->select("jumlah as jml_viewer");
        $this->db->from($this->tblInfotb("a"));
        $this->db->join(DBVIEW_DATA_VIEWER_ARTIKEL,"a.idcontent=id","left");
        $this->db->join($this->mstKategori("c"),"a.idcategori=c.idkategori");
        $this->db->limit($size,$start);
        $this->db->order_by("a.tgl_terbit",$order);
        //return $this->db->get_compiled_select();
        return $this->db->get()->result();
    }


    public function getCountSearch($search){
        $this->db->select("idcontent");
        $this->db->like("judul_artikel",$search);
        $this->db->or_like("headlines",$search);
        $q = $this->db->get($this->tblInfotb());
        return $q->num_rows();
    }

    

    public function searchInfotb($search,$page=1,$size=10,$order = "DESC")
    {
        
        //$tot = $this->getCountInfotb()
        $start = ($page-1)*$size;
        $this->db->select("a.idcontent,a.idcategori,c.kategori,a.judul_artikel,a.headlines,date_format(a.tgl_terbit,'%d/%m/%Y') as tgl_terbit,a.slug");
        $this->db->select("a.author");
        $this->db->select("a.reviewer");
        $this->db->select("date_format(a.last_edit,'%d/%m/%Y') as last_edit");
        //$this->db->select("a.isi_artikel");
        $this->db->select("a.foto,a.foto_landscape");
        $this->db->select("jumlah as jml_viwer");

        $this->db->from($this->tblInfotb("a"));
       $this->db->join(DBVIEW_DATA_VIEWER_ARTIKEL,"a.idcontent=id");
        $this->db->join($this->mstKategori("c"),"a.idcategori=c.idkategori");
        $this->db->like("a.judul_artikel",$search);
        $this->db->or_like("a.headlines",$search);
        $this->db->limit($size,$start);
        $this->db->order_by("a.tgl_terbit",$order);
        //return $this->db->get_compiled_select();
        return $this->db->get()->result();
    }




    public function deleteInfotb($id){
        return $this->db->delete($this->tblInfotb(),array("idcontent"=>$id));
    }

    public function updateInfotb($rdata,$id){
    return    $this->db->update($this->tblInfotb(),$rdata,array("idcontent"=>$id));
    
        }

    public function getRelatedInfo($id){
        $this->db->select("b.idcontent");
        $this->db->select("b.judul_artikel");
        $this->db->select("b.slug");
        $this->db->from($this->tblRelated("a"));
        $this->db->join($this->tblInfotb("b"),"a.idrelated=b.idcontent");
        $this->db->where("a.idcontent",$id);
        return $this->db->get()->result();

    }

    public function getNewestInfo(){
        $this->db->select("idcontent");
        $this->db->select("judul_artikel");
        $this->db->select("slug");
        $this->db->order_by("idcontent","DESC");
        $this->db->limit(5,0);
        $q = $this->db->get($this->tblInfotb());
        return $q->result();

    }

    public function counterAddPerdate($id){
        $this->db->select("view_jumlah");
        $this->db->where("view_tanggal",date("Y-m-d"));
        $this->db->where("view_artikel_id",$id);
        $q = $this->db->get(DB_DATA_VIEWER_ARTIKEL)->row();
        $count = $q->view_jumlah+1;
        return  $this->db->update(DB_DATA_VIEWER_ARTIKEL,array("view_jumlah"=>$count),array("view_artikel_id"=>$id,"view_tanggal"=>date("Y-m-d")));

    }

    public function detailInfotb($id){
        $this->db->select("a.*");
        $this->db->select("jumlah as jml_viewer");
        //$this->db->select("if(isnull(b.jml_viewer),0,b.jml_viewer) as jml_viewer");
        $this->db->from($this->tblInfotb("a"));
        $this->db->join(DBVIEW_DATA_VIEWER_ARTIKEL,"a.idcontent=id");

       // $this->db->join($this->tblViewer("b"),"a.idcontent=b.idcontent","left");
        $this->db->where("a.idcontent",$id);
        $this->db->or_where("a.slug",$id);
        //return $this->db->get_compiled_select();
       $q = $this->db->get()->row();

       
     //  $v = $q->jumlahr+1;
       if($this->counterAddPerdate($id)){
       return $q;
       }else{
           return false;
       }
    
    }

    public function checkUserView($username,$id){
        $this->db->where("dpa_username",$username);
        $this->db->where("date(dpa_date)=CURDATE()");
        $this->db->where("dpa_artikel_id",$id);
        $q = $this->db->get(DB_DATA_POIN_ARTIKEL);
        return $q->num_rows();
    }

    public function checkTimeView($username){
        $this->db->where("dpa_username",$username);
        return $this->db->get(DBVIEW_LAST_VIEW_ARTIKEL)->row();
        
    }

    public function updatePoin($rdata){
        return $this->db->insert(DB_DATA_POIN_ARTIKEL,$rdata);
    }
    
    public function addKategori($rdata){
        return $this->db->insert($this->mstKategori(),$rdata);
    }

    public function getKategori(){
        $q = $this->db->get($this->mstKategori());
        return $q->result();
    }

    public function updateKategori($rdata,$id){
        return  $this->db->update($this->mstKategori(),$rdata,array("idkategori"=>$id));
    }

    public function deleteKategori($id){
        return $this->db->delete($this->mstKategori(),array("idkategori"=>$id));
    }
    public function detailKategori($id){
        $this->db->where("idkategori",$id);
        $q = $this->db->get($this->mstKategori());
        return $q->row();
    }
}