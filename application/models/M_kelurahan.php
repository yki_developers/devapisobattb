<?php
Class M_kelurahan extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    private function mstKelurahan($alias=null){
        return "mst_kelurahan ".$alias;

    }
    private function idCheckKelurahan($id){
        $this->db->select("idkelurahan");
        $this->db->where("idkelurahan",$id);
        $q = $this->db->get($this->mstKelurahan());
        if($q->num_rows()=="0"){
            return true;
        }else{
            return false;
        }

    }
    public function addKelurahan($rdata){
        $d = $this->idCheckKelurahan($rdata['idkelurahan']);
        if($d==true){
            if($this->db->insert($this->mstKelurahan(),$rdata)){
                $result = array("success"=>true,"info"=>"Data Kelurahan berhasil disimpan");
            }else{
                $result = array("success"=>false,"info"=>"Data Kelurahan Gagal disimpan");
            }

        }else{
            $result = array("success"=>false,"info"=>"Kode Kelurahan Sudah Terdaftar");
        }

        return $result;

    }
    public function getKelurahan($id){
        $this->db->select("a.*");
        $this->db->select("b.nama_kecamatan");
        $this->db->select("c.nama_kabupaten");
        $this->db->select("d.nama_propinsi");
        $this->db->where("a.idkecamatan",$id);
        $this->db->from($this->mstKelurahan("a"));
        $this->db->join("mst_kecamatan b","a.idkecamatan=b.idkecamatan");
        $this->db->join("mst_kabupaten c","a.idkabupaten=c.idkabupaten");
        $this->db->join("mst_propinsi d","a.idpropinsi=d.idpropinsi");
        $q = $this->db->get();
        return $q->result();

    }


    public function getAllKelurahan($page,$size){
        $start = ($page-1)*$size;
        $this->db->select("a.*");
        $this->db->select("b.nama_kecamatan");
        $this->db->select("c.nama_kabupaten");
        $this->db->select("d.nama_propinsi");
        $this->db->from($this->mstKelurahan("a"));
        $this->db->join("mst_kecamatan b","a.idkecamatan=b.idkecamatan");
        $this->db->join("mst_kabupaten c","a.idkabupaten=c.idkabupaten");
        $this->db->join("mst_propinsi d","a.idpropinsi=d.idpropinsi");
        $this->db->limit($size,$start);
        $q = $this->db->get();
        return $q->result();

    }


    public function searchKelurahan($keyword){
        
        $this->db->select("a.*");
        $this->db->select("b.nama_kecamatan");
        $this->db->select("c.nama_kabupaten");
        $this->db->select("d.nama_propinsi");
        $this->db->from($this->mstKelurahan("a"));
        $this->db->join("mst_kecamatan b","a.idkecamatan=b.idkecamatan");
        $this->db->join("mst_kabupaten c","a.idkabupaten=c.idkabupaten");
        $this->db->join("mst_propinsi d","a.idpropinsi=d.idpropinsi");
      $this->db->like('nama_kelurahan',$keyword);
      $this->db->or_like('idkelurahan',$keyword,'none');
      $this->db->limit('50','0');
        $q = $this->db->get();
        return $q->result();

    }




    public function countAll(){
        return $this->db->get($this->mstKelurahan())->num_rows();
    }



    public function updateKelurahan($rdata,$id){
    
        if($rdata['idkelurahan']!=$id){
        $d = $this->idCheckKelurahan($rdata['idkelurahan']);
        }else{
            $d = true;
        }

        if($d==true){

            if($this->db->update($this->mstKelurahan(),$rdata,array("idkelurahan"=>$id))){
                $result = array("success"=>true,"info"=>"Update Data Kelurahan Berhasil");

            }else{
                $result = array("success"=>false,"info"=>"Update Data Kelurahan Gagal");

            }

        }else{
            $result = array("success"=>false,"info"=>"Kode Kelurahan Sudah Terdaftar");
        }
        return $result;

//return $this->db->set($rdata)->where("idkelurahan",$id)->get_compiled_update($this->mstKelurahan());

    }

    public function deleteKelurahan($id){

        if($this->db->delete($this->mstKelurahan(),array("idkelurahan"=>$id))){
            $result = array("success"=>true,"info"=>"Data Kelurahan Berhasil Dihapus");
        }else{
            $result = array("success"=>false,"info"=>"Data Kelurahan Gagal Dihapus");


        }
return $result;
    }

    public function infoKelurahan($id){
        $this->db->select("a.*");
        $this->db->select("b.nama_kecamatan");
        $this->db->select("c.nama_kabupaten");
        $this->db->select("d.nama_propinsi");
        $this->db->where("a.idkelurahan",$id);
        $this->db->from($this->mstKelurahan("a"));
        $this->db->join("mst_kecamatan b","a.idkecamatan=b.idkecamatan");
        $this->db->join("mst_kabupaten c","a.idkabupaten=c.idkabupaten");
        $this->db->join("mst_propinsi d","a.idpropinsi=d.idpropinsi");
        $q = $this->db->get();
        return $q->row();

    }

    public function detailKelurahan($id){
        $this->db->select("*");
        $this->db->where("idkelurahan",$id);
        return $this->db->get($this->mstKelurahan())->result();
    }
    
}