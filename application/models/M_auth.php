<?php
Class M_auth extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAuth($base64code){
        $this->db->where("authorization",$base64code);
        return $this->db->get("rest_keys")->row();
    }
}