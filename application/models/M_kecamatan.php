<?php
Class M_kecamatan extends CI_Model{
    public function __construct(){
        parent::__construct();
    }
    private function mstKecamatan($alias=null){
        return "mst_kecamatan ".$alias;
    }

    private function idCheckKecamatan($id){
        $this->db->select("idkecamatan");
        $this->db->where("idkecamatan",$id);
        $q = $this->db->get($this->mstKecamatan());
        if($q->num_rows()==0){
            return true;
        }else{
            return false;
        }
    }

    public function addKecamatan($rdata){
        $d = $this->idCheckKecamatan($rdata['idkecamatan']);
        if($d==true){
        if($this->db->insert($this->mstKecamatan(),$rdata)){
        $result = array("success"=>true,"info"=>"Data Kecamatan Berhasil disimpan");
        }else{
            $result = array("success"=>false,"info"=>"Data Kecamatan Gagal disimpan");
        }
        }else{
            $result = array("success"=>false,"info"=>"Kode Kecamatan Sudah Terdaftar");
        }
        return $result;
    }


    public function getKecamatan($idkab){
        $this->db->select("a.idkecamatan");
        $this->db->select("a.idkabupaten");
        $this->db->select("a.idpropinsi");
        $this->db->select("a.nama_kecamatan");
        $this->db->select("b.nama_kabupaten");
        $this->db->select("c.nama_propinsi");
        $this->db->from($this->mstKecamatan("a"));
        $this->db->join("mst_kabupaten b","a.idkabupaten=b.idkabupaten");
        $this->db->join("mst_propinsi c","a.idpropinsi=c.idpropinsi");
        $this->db->where("a.idkabupaten",$idkab);
        $q = $this->db->get();
        return $q->result();
    }


    public function getAllKecamatan($page,$size){
        $start = ($page-1)*$size;
        $this->db->select("a.idkecamatan");
        $this->db->select("a.idkabupaten");
        $this->db->select("a.idpropinsi");
        $this->db->select("a.nama_kecamatan");
        $this->db->select("b.nama_kabupaten");
        $this->db->select("c.nama_propinsi");
        $this->db->from($this->mstKecamatan("a"));
        $this->db->join("mst_kabupaten b","a.idkabupaten=b.idkabupaten");
        $this->db->join("mst_propinsi c","a.idpropinsi=c.idpropinsi");
        //$this->db->where("a.idkabupaten",$idkab);
        $this->db->limit($size,$start);
        $this->db->order_by("a.idpropinsi","ASC");
        $q = $this->db->get();
        return $q->result();
    }


    public function searchKecamatan($keyword){
        $this->db->select("a.idkecamatan");
        $this->db->select("a.idkabupaten");
        $this->db->select("a.idpropinsi");
        $this->db->select("a.nama_kecamatan");
        $this->db->select("b.nama_kabupaten");
        $this->db->select("c.nama_propinsi");
        $this->db->from($this->mstKecamatan("a"));
        $this->db->join("mst_kabupaten b","a.idkabupaten=b.idkabupaten");
        $this->db->join("mst_propinsi c","a.idpropinsi=c.idpropinsi");
        $this->db->like("a.nama_kecamatan",$keyword);
        $this->db->or_like("a.idkecamatan",$keyword,'none');
        //$this->db->where("a.idkabupaten",$idkab);
        $this->db->limit("50","0");
        $this->db->order_by("a.idpropinsi","ASC");
        $q = $this->db->get();
        return $q->result();
        //return $this->db->get_compiled_select();
    }


    public function countAll(){
        return $this->db->get($this->mstKecamatan())->num_rows();
    }


    public function updateKecamatan($rdata,$id){
        if($rdata['idkecamatan']!=$id){
            $d = $this->idCheckKecamatan($rdata['idkecamatan']);
        }else{
            $d= true;
        }
        if($d==true){
            if($this->db->update($this->mstKecamatan(),$rdata,array("idkecamatan"=>$id))){
                $result = array("success"=>true,"info"=>"Data Kecamatan berhasil di update");
            }else{
                $result = array("success"=>false,"info"=>"Update Kecamatan Gagal");
            };
        }else{
            $result = array("success"=>false,"info"=>"Kode Kecamatan Sudah terdaftar");
        }
        return $result;

    }

    public function deleteKecamatan($id){
        if($this->db->delete($this->mstKecamatan(),array("idkecamatan"=>$id))){
            $result = array("success"=>true,"info"=>"data Kecamatan berhasil dihapus");
        }else{
            $result = array("success"=>true,"info"=>"data Kecamatan berhasil dihapus");
        }
        return $result;
    }


    public function infoKecamatan($id){
        $this->db->select("a.idkecamatan");
        $this->db->select("a.idkabupaten");
        $this->db->select("a.idpropinsi");
        $this->db->select("a.nama_kecamatan");
        $this->db->select("b.nama_kabupaten");
        $this->db->select("c.nama_propinsi");
        $this->db->from($this->mstKecamatan("a"));
        $this->db->join("mst_kabupaten b","a.idkabupaten=b.idkabupaten");
        $this->db->join("mst_propinsi c","a.idpropinsi=c.idpropinsi");
        $this->db->where("idkecamatan",$id);
        $q = $this->db->get();
        return $q->row();
    }

    public function detailKecamatan($id){
        $this->db->select("*");
        $this->db->where("idkecamatan",$id);
        return $this->db->get($this->mstKecamatan())->result();
    }

}