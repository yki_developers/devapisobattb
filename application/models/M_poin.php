<?php
Class M_poin extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    
    }

    public function getTopTen()
    {
        $this->db->limit(10,0);
        $this->db->order_by("poin_total","DESC");
        return $this->db->get(DBVIEW_POIN_REKAP_TOTAL)->result();
    }


    public function checkUserShare($username,$id){
        $this->db->where("psa_username",$username);
        $this->db->where("date(psa_insert)=CURDATE()");
        $this->db->where("psa_artikel_id",$id);
        $q = $this->db->get(DB_DATA_POIN_SHARED);
        return $q->num_rows();
    }



    public function checkSharePoin($username){
$this->db->where("psa_username",$username);
return $this->db->get(DBVIEW_LAST_SHARE_ARTIKEL)->row();
    }
    public function addPoinShare($rdata){
        return $this->db->insert(DB_DATA_POIN_SHARED,$rdata);
    }


    public function getTopTenArsip($bln,$thn)
    {
        $this->db->limit(10,0);
        $this->db->where("bulan",$bln);
        $this->db->where("tahun",$thn);
        $this->db->order_by("poin_total","DESC");
        return $this->db->get(DB_DATA_POIN_ARSIP_BULANAN)->result();
    }
public function listPoin(){
    $this->db->select("poin_user");
    $this->db->order_by("poin_total","DESC");
    return $this->db->get(DBVIEW_POIN_REKAP_TOTAL)->result();
}

    public function getMyPoin($username){
        $this->db->where("poin_user",$username);
        return $this->db->get(DBVIEW_POIN_REKAP_TOTAL)->result();
    }

    public function dailyCheckin($rdata){
        return $this->db->insert(DB_DATA_POIN_DAILY_CHECKIN,$rdata);
    }

    public function checkDay($username){
        $this->db->where("dp_username",$username);
        $this->db->where("date(dp_datetime)=DATE(DATE_SUB(now(), INTERVAL '1' DAY))");
        $this->db->select("dp_checkin_day");
       // return $this->db->get_compiled_select(DB_DATA_POIN_DAILY_CHECKIN);
        return $this->db->get(DB_DATA_POIN_DAILY_CHECKIN)->row();
    }

    public function lastCheck($username){
        $this->db->where("dp_username",$username);
        $this->db->where("DATE(dp_datetime)=CURDATE()");
        $q = $this->db->get(DB_DATA_POIN_DAILY_CHECKIN);
        return $q->num_rows();
    }

    public function getListReward(){
        return $this->db->get(DB_DATA_POIN_REWARD)->result();
    }



  

}