<?php
Class M_openforum extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function total_comment($id){
        $this->db->where('cof_forum_id',$id);
        $q = $this->db->get(DB_DATA_COMMENT_OPENFORUM);
        return $q->num_rows();
    }

    public function insert($rdata){
    return $this->db->insert(DB_DATA_OPENFORUM,$rdata);

    }

    public function checkPoin($username){
        $this->db->where("dpt_username",$username);
        $this->db->where("date(dpt_date)=curdate()");
        $this->db->where("dpt_type","3");
        return $this->db->get(DB_DATA_POIN_THREAD)->num_rows();
    }

    public function checkPoinComment($username){
        $this->db->where("dpt_username",$username);
        $this->db->where("date(dpt_date)=curdate()");
        $this->db->where("dpt_type","2");
        return $this->db->get(DB_DATA_POIN_THREAD)->num_rows();
    }



    public function updatePoin($rdata){
        return $this->db->insert(DB_DATA_POIN_THREAD,$rdata);
    }

 

    public function getList(){
        $this->db->order_by('forum_id','DESC');
        return $this->db->get(DB_DATA_OPENFORUM)->result();
    }

    public function getListMyThread($username){
        $this->db->where("userid",$username);
        $this->db->order_by('id_thread','DESC');
        return $this->db->get(DBVIEW_OPENFORUM_LIST)->result();
    }


    public function getDetail($id){
        $this->db->where("forum_id",$id);
        return $this->db->get(DB_DATA_OPENFORUM)->row();
    }

    public function update($rdata,$id){
        $this->db->where('forum_id',$id);
        return $this->db->update(DB_DATA_OPENFORUM,$rdata);
    }

    public function delete($id){
        return $this->db->delete(DB_DATA_OPENFORUM,array("forum_id"=>$id));
    }

    public function insertComment($rdata){
        return $this->db->insert(DB_DATA_COMMENT_OPENFORUM,$rdata);
    }

    public function getListComment($fid){
        $this->db->where("cof_forum_id",$fid);
        $this->db->where("cof_replyto",'0');
        return $this->db->get(DB_DATA_COMMENT_OPENFORUM)->result();
    }

    public function getListReplyComment($id){
        $this->db->where("cof_replyto",$id);
        return $this->db->get(DB_DATA_COMMENT_OPENFORUM)->result();
    }

    public function updateComment($rdata,$id){
        $this->db->where("cof_id",$id);
        return $this->db->update(DB_DATA_COMMENT_OPENFORUM,$rdata);
    }

    public function deleteComment($id){
        return $this->db->delete(DB_DATA_COMMENT_OPENFORUM,array("cof_id"=>$id));
    }

    public function detailComment($id){
        $this->db->where('cof_id',$id);
        return $this->db->get(DB_DATA_COMMENT_OPENFORUM)->row();
    }

    public function getPlayerId($id){
        $this->db->where("cof_forum_id",$id);
        return $this->db->get(DB_VIEW_OPENFORUM_PLAYERID)->result();
    }

    public function getNamaPengguna($username){
        $this->db->where("username",$username);
        return $this->db->get(DB_USER_MOBILE)->row();
    }

    


}