<?php
Class M_screening extends CI_Model{
    public function __construct()
    {
        parent::__construct();

    }

  

    public function getPlayerId($idfaskes){
        $this->db->select("b.*");
        $this->db->where("a.kd_fasyankes",$idfaskes);
        $this->db->from(DB_USER_FASKES."  a");
        $this->db->join(DB_USER_MOBILE," b","a.username=b.username");
        return $this->db->get()->result();

    }

    public function insertNotifikasi($rdata){
        return $this->db->insert(DB_DATA_NOTIFKASI,$rdata);
    }
    

    public function insert($rdata){
        $this->db->insert(DB_DATA_SCREENING,$rdata);
        return $this->db->insert_id(); 
    }

    public function getHasil($id){
        $this->db->order_by("sc_dp_id","DESC");
        $this->db->where("sc_dp_id",$id);
        return $this->db->get(DB_VIEW_HASIL_SCREENING)->row();
        //return $q->kode_hasil;
    }

    public function update($rdata,$username){
        return $this->db->update(DB_DATA_SCREENING,$rdata,array("sc_dp_id"=>$username));
    }

    public function getLastScreening($username){
        $this->db->where("input_user",$username);
        $this->db->select_max('sc_date');
        $this->db->group_by("input_user");
        return $this->db->get(DBVIEW_SCREENING_HASIL_UMUM)->row();
       // return $this->db->get_compiled_select(DBVIEW_SCREENING_HASIL_UMUM);
    }

    public function getHasilLast($username,$date){
        $this->db->where("input_user",$username);
        $this->db->where("screening_date",$date);
        $this->db->select("sc_id,sc_date,text_hasil");
       return $this->db->get(DBVIEW_SCREENING_HASIL_PERFASKES)->row();

      // return $this->db->get_compiled_select(DBVIEW_SCREENING_HASIL_PERFASKES);
    }
    
    public function getHasilPerfaskes($kdfaskes){
        $this->db->order_by("sc_id","DESC");
        $this->db->where("hf_code",$kdfaskes);
        $this->db->group_start();
        $this->db->where("text_hasil!='Bukan Terduga TBC'");
        $this->db->or_where("status_hiv='Positif'");
        $this->db->group_end();
        return $this->db->get(DBVIEW_SCREENING_HASIL_PERFASKES)->result();
    }

    public function getHasilPerKab($kabupaten){
        $this->db->order_by("sc_id","DESC");
        $this->db->where("district_code",$kabupaten);
        $this->db->group_start();
        $this->db->where("text_hasil!='Bukan Terduga TBC'");
        $this->db->or_where("status_hiv='Positif'");
        $this->db->group_end();
        return $this->db->get(DBVIEW_SCREENING_HASIL_PERFASKES)->result();
    }

    public function getHasilPerProp($propinsi){
        $this->db->order_by("sc_id","DESC");
        $this->db->where("province_code",$propinsi);
        $this->db->group_start();
        $this->db->where("text_hasil!='Bukan Terduga TBC'");
        $this->db->or_where("status_hiv='Positif'");
        $this->db->group_end();
        return $this->db->get(DBVIEW_SCREENING_HASIL_PERFASKES)->result();
    }

    public function getListScreening($username){
        $this->db->order_by("sc_id","DESC");
        $this->db->where("input_user",$username);
        return $this->db->get(DBVIEW_SCREENING_HASIL_PERFASKES)->result();
    }


    public function getFilterScreening($filter){
        $this->db->order_by("sc_id","DESC");
        $this->db->where($filter);
        $this->db->group_start();
        $this->db->where("text_hasil!='Bukan Terduga TBC'");
        $this->db->or_where("status_hiv='Positif'");
        $this->db->group_end();
        $this->db->order_by("sc_date","DESC");
        return $this->db->get(DBVIEW_SCREENING_HASIL_PERFASKES)->result();
    }


    public function reportHasilPerfaskes($kdfaskes){
        $this->db->order_by("sc_dp_id","DESC");
        $this->db->where("idfaskes",$kdfaskes);
        $this->db->order_by("sc_date","DESC");
        return $this->db->get(DB_VIEW_DETAIL_SCREENING)->result();
    }

    public function reportHasilPerKab($id){
        $this->db->order_by("sc_dp_id","DESC");
        $this->db->where("idkabupaten",$id);
        $this->db->order_by("sc_date","DESC");
        return $this->db->get(DB_VIEW_DETAIL_SCREENING)->result();
    }

    public function reportHasilPerProp($id){
        $this->db->order_by("sc_dp_id","DESC");
        $this->db->where("idpropinsi",$id);
        $this->db->order_by("sc_date","DESC");
        return $this->db->get(DB_VIEW_DETAIL_SCREENING)->result();
    }


    public function newHasilPerfaskes($kdfaskes,$page,$size){
        $start=($page-1)*$size;
        $this->db->order_by("sc_id","DESC");
        $this->db->where("hf_code",$kdfaskes);
        $this->db->group_start();
        $this->db->where("text_hasil!='Bukan Terduga TBC'");
        $this->db->or_where("status_hiv='Positif'");
        $this->db->group_end();
        $this->db->limit($size,$start);
        return $this->db->get(DBVIEW_SCREENING_HASIL_PERFASKES)->result();
    }

    public function newListScreening($username,$page,$size){
        $start=($page-1)*$size;
        $this->db->order_by("sc_id","DESC");
        $this->db->where("input_user",$username);
        $this->db->limit($size,$start);
        return $this->db->get(DBVIEW_SCREENING_HASIL_PERFASKES)->result();
    }



    public function searchHasilPerfaskes($kdfaskes,$search){
        $this->db->order_by("sc_id","DESC");
        $this->db->where("hf_code",$kdfaskes);
        $this->db->like("nama_pengguna",$search,"both");
    
        $this->db->group_start();
        $this->db->where("text_hasil!='Bukan Terduga TBC'");
        $this->db->or_where("status_hiv='Positif'");
        $this->db->group_end();
     
        return $this->db->get(DBVIEW_SCREENING_HASIL_PERFASKES)->result();
    }

    public function searchListScreening($username,$search,$page,$size){
        $start=($page-1)*$size;
   
        $this->db->order_by("sc_id","DESC");
        $this->db->where("input_user",$username);
        $this->db->like("nama_pengguna",$search,"both");
        $this->db->limit($size,$start);
 
        return $this->db->get(DBVIEW_SCREENING_HASIL_PERFASKES)->result();
    }

    

}