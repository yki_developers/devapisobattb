<?php
Class M_appointment extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getNamaPengguna($username){
        $this->db->where("username",$username);
        return $this->db->get(DB_USER_MOBILE)->row();
    }


    public function getPlayerId($idfaskes){
        $this->db->select("b.*");
        $this->db->where("a.kd_fasyankes",$idfaskes);
        $this->db->from(DB_USER_FASKES."  a");
        $this->db->join(DB_USER_MOBILE," b","a.username=b.username");
        return $this->db->get()->result();

    }

    public function insertNotifikasi($rdata){
        return $this->db->insert(DB_DATA_NOTIFKASI,$rdata);
    }
    


    public function insert($rdata){
        return $this->db->insert(DB_DATA_APPOINTMENT,$rdata);
    }

    public function getAppointmentDetail($id){
        $this->db->where("appointment_id",$id);
        return $this->db->get(DBVIEW_DATA_APPOINTMENT)->row();
    }

    public function update($rdata,$id){
        $this->db->set($rdata);
        $this->db->where("appointment_id",$id);
        return $this->db->update(DB_DATA_APPOINTMENT);
    }


    public function getListAppointmentUser($username){
        $this->db->where("appointment_username",$username);
        $this->db->order_by("appointment_id","DESC");
        return $this->db->get(DBVIEW_DATA_APPOINTMENT)->result();
    }


    public function getListAppointment($faskes){
        $this->db->where("appointment_faskes",$faskes);
        $this->db->order_by("appointment_id","DESC");
        $this->db->from(DBVIEW_DATA_APPOINTMENT);
        //$this->db->join(DBVIEW_SCREENING_HASIL_PERFASKES,'appointment_screening_id=sc_id');
        return $this->db->get()->result();
    }


    public function getListAppointmentKab($kab){
        $this->db->where(DB_MASTER_FASKES.".idkabupaten",$kab);
        $this->db->order_by("appointment_id","DESC");
        return $this->db->get(DBVIEW_DATA_APPOINTMENT)->result();
    }



    public function getListAppointmentProp($prop){
        $this->db->where(DB_MASTER_FASKES.".idpropinsi",$prop);
        $this->db->order_by("appointment_id","DESC");
        return $this->db->get(DBVIEW_DATA_APPOINTMENT)->result();
    }



    public function getFilter($rdata){
        $this->db->where($rdata);
        $this->db->order_by("appointment_id","DESC");
        $this->db->from(DB_DATA_APPOINTMENT);
        $this->db->join(DB_VIEW_DETAIL_SCREENING,'appointment_screening_id=sc_dp_id');
        return $this->db->get()->result();
    }

    public function searchListAppointment($faskes,$rdata){
        $start = ($rdata['page']-1)*$rdata['size'];
        $this->db->group_start();
        $this->db->like("appointment_nama",$rdata['search'],'both');
        $this->db->group_end();
        $this->db->where("appointment_faskes",$faskes);
        $this->db->order_by("appointment_id","DESC");
        $this->db->limit($rdata['size'],$start);
        $this->db->from(DBVIEW_DATA_APPOINTMENT);

        //$this->db->join(DBVIEW_SCREENING_HASIL_PERFASKES,'appointment_screening_id=sc_id');
        return $this->db->get()->result();
    }


    public function searchListAppointmentUser($username,$rdata){
        $start = ($rdata['page']-1)*$rdata['size'];
        $this->db->group_start();
        $this->db->like("appointment_nama",$rdata['search'],'both');
        $this->db->group_end();
        $this->db->where("appointment_username",$username);
        $this->db->order_by("appointment_id","DESC");
        $this->db->limit($rdata['size'],$start);
        return $this->db->get(DBVIEW_DATA_APPOINTMENT)->result();
    }





    public function newListAppointment($faskes,$rdata){
        $start = ($rdata['page']-1)*$rdata['size'];
        $this->db->where("appointment_faskes",$faskes);
        $this->db->order_by("appointment_id","DESC");
        $this->db->limit($rdata['size'],$start);
        $this->db->from(DBVIEW_DATA_APPOINTMENT);
        //$this->db->join(DBVIEW_SCREENING_HASIL_PERFASKES,'appointment_screening_id=sc_id');
        return $this->db->get()->result();
    }

    public function newListAppointmentUser($username,$rdata){
        $start = ($rdata['page']-1)*$rdata['size'];
        $this->db->where("appointment_username",$username);
        $this->db->order_by("appointment_id","DESC");
        $this->db->limit($rdata['size'],$start);
        return $this->db->get(DBVIEW_DATA_APPOINTMENT)->result();
    }





    
}