<?php
Class M_forum extends CI_Model{
    public function __construct(){
        parent::__construct();
    }
    

    private function tblKategori($alias=null){
        return "tbl_forum_kategori ".$alias;
    }
    private function tblForum($alias=null){
        return "tbl_forum_thread ".$alias;
    }

    private function tblReply($alias=null){
        return "tbl_forum_balasan_thread ".$alias;
    }


    public function getTotalThread($idorganisasi){
        $this->db->select("count(idthread) as total");
        $this->db->where("idorganisasi",$idorganisasi);
        $this->db->where("status_thread","open");
        $this->db->group_by("idorganisasi");
        $q=$this->db->get($this->tblForum());
        return $q->result();

    }

    public function addReply($rdata){
        return $this->db->insert($this->tblReply(),$rdata);
    }

   

    public function getChildReply($idparent){
        $q = $this->db->get($this->tblReply(),array("reply_to"=>$idparent));
        return $q->result();

    }
    public function getReply($idthread,$page="1",$size="10",$order="DESC"){
        $start = ($page-1)*$size;
        $this->db->select("a.idbalasan");
        $this->db->select("a.idthread");
        $this->db->select("date_format(a.tgl_balasan,'%d/%m/%Y') as tgl_post");
        $this->db->select('b.nama_pengguna');
        $this->db->select("b.foto");
        $this->db->select("a.userid");
        $this->db->select("a.isi_balasan");
        $this->db->select("b.playerid");
        $this->db->from($this->tblReply("a"));
        $this->db->join("tbl_mobile_users b","a.userid=b.username");
        $this->db->where("a.idthread",$idthread);
        $this->db->limit($size,$start);
        $this->db->order_by("a.idbalasan",$order);
        $q = $this->db->get();
        return $q->result();

    }

    public function updateReply($rdata,$id){
        return $this->db->update($this->tblReply(),$rdata,array("idbalasan"=>$id));

    }

    public function deleteReply($id){
        return $this->db->delete($this->tblReply(),array("idbalasan"=>$id));

    }

    public function detailReply($id){

    }

    public function addForum($rdata){
        if($this->db->insert($this->tblForum(),$rdata)){
            return array("success"=>true,"info"=>"Thread berhasil disimpan");
        }else{
            return array("success"=>false,"info"=>"Thread Gagal disimpan");
        }

    }

    public function getForum($page="1",$size="10",$order="DESC",$idorganisasi=null){

        $start = ($page-1)*$size;
        $this->db->select("a.*");
        $this->db->select("b.kategori");
        $this->db->select("c.nama_organisasi");
        $this->db->from($this->tblForum("a"));
        $this->db->join($this->tblKategori("b"),"a.idkategori=b.idkategori");
        $this->db->join("mst_organisasi c","c.idorganisasi=a.idorganisasi");
        if($idorganisasi!=null){
        $this->db->where("a.idorganisasi",$idorganisasi);
        }

        $this->db->limit($size,$start);
        $this->db->order_by("a.idthread",$order);
        return $this->db->get()->result();

    }

    public function getMyThread($username){

        
        $this->db->select("a.idthread");
        $this->db->select("a.idthread");
        $this->db->select("date_format(a.tgl_Terbit,'%d/%m/%Y') as tgl_post");
        $this->db->select("a.userid");
        $this->db->select("a.idthread");
        $this->db->select("a.judul_thread");
        $this->db->select("a.isi_thread");
        $this->db->select("b.kategori");
        $this->db->select("d.foto");
        $this->db->select("d.nama_pengguna");
        $this->db->select("a.idorganisasi");
        $this->db->select("c.nama_organisasi");
        $this->db->select("(SELECT COUNT(tb.idbalasan) AS jml FROM tbl_forum_balasan_thread tb where tb.idthread=a.idthread) as total_comment");
        $this->db->select("a.status_thread");
        $this->db->from($this->tblForum("a"));
        $this->db->join($this->tblKategori("b"),"a.idkategori=b.idkategori");
        $this->db->join("mst_organisasi c","c.idorganisasi=a.idorganisasi");
        $this->db->join("tbl_mobile_users d","a.userid=d.username");
        $this->db->where("a.userid",$username);

        //$this->db->limit($size,$start);
        $this->db->order_by("a.idthread","DESC");
        return $this->db->get()->result();

    }

    public function updateForum($rdata,$id){
        return $this->db->update($this->tblForum(),$rdata,array("idthread"=>$id));
    }
    
    public function deleteForum($id){
        return $this->db->delete($this->tblForum(),array("idthread"=>$id));

    }
    public function detailForum($id){

        $this->db->select("a.idthread");
        $this->db->select("a.idkategori");
        $this->db->select("a.judul_thread");
        $this->db->select("date_format(a.tgl_terbit,'%d/%m/%Y') as tgl_post");
        $this->db->select("a.isi_thread");
        $this->db->select("a.idkategori");
        $this->db->select("b.kategori");
        $this->db->select("a.idorganisasi");
        $this->db->select("c.nama_organisasi");
        $this->db->select("a.userid");
        $this->db->select("d.nama_pengguna");
        $this->db->select("d.foto");
        $this->db->select("d.playerid");
        $this->db->select("(SELECT count(rp.idbalasan) as jml from tbl_forum_balasan_thread rp where rp.idthread=a.idthread) as total_comment");
        $this->db->from($this->tblForum("a"));
        $this->db->join($this->tblKategori("b"),"a.idkategori=b.idkategori");
        $this->db->join("mst_organisasi c","c.idorganisasi=a.idorganisasi");
        $this->db->join('tbl_mobile_users d','a.userid=d.username');
        $this->db->where("a.idthread",$id);
        return $this->db->get()->row();

    }


    public function addKategori($rdata){
        if($this->db->insert($this->tblKategori(),$rdata)){
            return array("success"=>true,"info"=>"Data Berhasil disimpan");
        }else{
            return array("success"=>false,"info"=>"Data Gagal disimpan");
        }
    }

    public function getKategori(){
        return $this->db->get($this->tblKategori())->result();
    }

    public function updateKategori($rdata,$id){
        return $this->db->update($this->tblKategori(),$rdata,array("idkategori"=>$id));
    }

    public function deleteKategori($id){
        return $this->db->delete($this->tblKategori(),array("idkategori"=>$id));
    }

    public function detailKategori($id){
        return $this->db->where("idkategori",$id)->get($this->tblKategori())->row();
    }



    

    public function getListByDate($idorganisasi,$page,$size){
        $start = ($page-1)*$size;
       $this->db->select("*");
       $this->db->where("idorganisasi",$idorganisasi);
       $this->db->where("status_thread","open");
       $this->db->limit($size,$start);
       return $this->db->get("v_list_thread")->result();
    }


    public function getListAllByDate($idorganisasi,$page,$size){
        $start = ($page-1)*$size;
       $this->db->select("*");
       $this->db->where("idorganisasi",$idorganisasi);
       //$this->db->where("status_thread","open");
       $this->db->limit($size,$start);
       return $this->db->get("v_list_thread")->result();
    }


    public function getListByComment($idorganisasi,$page,$size){
        $start = ($page-1)*$size;
        $this->db->select("*");
       $this->db->where("idorganisasi",$idorganisasi);
       $this->db->where("status_thread","open");
       $this->db->limit($size,$start);
       $this->db->order_by("comm","DESC");
       return $this->db->get("v_list_thread")->result();
        
    }


}