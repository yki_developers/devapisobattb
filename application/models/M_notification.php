<?php
Class M_notification extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    private function __tblNotif($id=null){
        return "tbl_notifikasi ".$id;
    }

    public function addNotif($rdata){
       
        foreach($rdata as $data){  
            $this->db->insert($this->__tblNotif(),$data);
      
    }
     return true;
    }


    public function getListNotifikasi($username,$rdata){
        $start = ($rdata['page']-1)*$rdata['size'];
        $this->db->where("notifikasi_username",$username);
        $this->db->limit($rdata['size'],$start);
        $this->db->order_by("notifikasi_id","DESC");
        return $this->db->get(DB_DATA_NOTIFKASI)->result();
    }

    public function updateNotifikasi($rdata,$idnotif){
        $this->db->where("notifikasi_id",$idnotif);
        return $this->db->update(DB_DATA_NOTIFKASI,$rdata);
    }


}