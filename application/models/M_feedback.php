<?php
Class M_feedback extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    private function tblFeedback($id=null){
        return "tbl_penilaian_faskes ".$id;
    }

    private function tblUserMobile($id=null){
        return "tbl_mobile_users ".$id;
    }


    

    public function writeFeedback($rdata){
        return $this->db->insert($this->tblFeedback(),$rdata);
    }

    public function getFeedbackPerFaskes($kdfasyankes){
        $this->db->select("a.*");
        $this->db->select("b.nohp");
        $this->db->from($this->tblFeedback("a"));
        $this->db->join($this->tblUserMobile("b"),"a.username=b.username");
       $this->db->where("a.kd_fasyankes",$kdfasyankes);
       $this->db->where("a.status_penilaian!='remove'");
        $this->db->order_by("a.idpenilaian","DESC");
       // return $this->db->get_compiled_select();
        $q = $this->db->get();
        return $q->result();
    }

    public function getFeedback($page=1,$size=10){
        $start = ($page-1)*$size;
       // $this->db->where("kd_fasyankes",$idfaskes);
        $this->db->where("status_penilaian='approve'");
        $this->db->limit($size,$start);
        $this->db->order_by("idpenilaian","DESC");
        $q = $this->db->get($this->tblFeedback());
        return $q->result();
    }

    public function getFeedbackApproved($idfaskes,$page=1,$size=10){
        $start = ($page-1)*$size;
        $this->db->where("kd_fasyankes",$idfaskes);
        $this->db->where("status_penilaian='approve'");
        $this->db->limit($size,$start);
        $this->db->order_by("idpenilaian","DESC");
        $q = $this->db->get($this->tblFeedback());
        return $q->result();
    }

    public function getFeedbackDetail($idfeedback){
        $q = $this->db->where("idpenilaian",$idfeedback)->get($this->tblFeedback(),array("idpenilaian"=>$idfeedback));
        //return $this->db->where("idpenilaian",$idfeedback)->get_compiled_select($this->tblFeedback());
        return $q->row();
    }


    public function getTotalbintang($idfaskes){
        $this->db->select_avg("jml_bintang","bintang");
        $this->db->where("kd_fasyankes",$idfaskes);
        $this->db->where("status_penilaian","approve");
        $this->db->group_by("kd_fasyankes");
        $q = $this->db->get($this->tblFeedback());
        return $q->result();
        //return $this->db->get_compiled_select($this->tblFeedback());
    }

    public function getPlayerId($idfeedback){
       $this->db->select("a.nama_fasyankes");
        $this->db->select("b.playerid");
        $this->db->from($this->tblFeedback("a"));
        $this->db->join("tbl_mobile_users b","a.username=b.username");
        $this->db->where("a.idpenilaian",$idfeedback);
        return $this->db->get()->row();
    }

    public function updateFeedback($rdata,$idfeedback){
        return $this->db->update($this->tblFeedback(),$rdata,array("idpenilaian"=>$idfeedback));
    }

    private function tblReplyFeedback($id=null){
        return "tbl_penilaian_faskes_reply ".$id;
    }

    public function replyFeedback($rdata){
        return $this->db->insert($this->tblReplyFeedback(),$rdata);
    }

    public function getReply($idfeedback){
        $this->db->select("username");
        $this->db->select("date_format(tglReply,'%d/%m/%Y') as tglReply");
        $this->db->select("balasan");
        return $this->db->get($this->tblReplyFeedback())->result();
    }

    public function delReply($idreply){
        return $this->db->delete($this->tblReplyFeedback(),array("idreply"=>$idreply));
    }


    public function updateReply($rdata,$idreply){
        return $this->db->update($this->tblReplyFeedback(),$rdata,array("idreply"=>$idreply));
    }

    

}