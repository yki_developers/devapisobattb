<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Mkelurahan extends REST_controller{
    public function __construct(){
        parent::__construct();
        $this->load->model("M_kelurahan");
    }

    public function index_post(){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if(!$this->input->post()){
            $rdata = array(
                "idkelurahan"=>$row['idkelurahan'],
                "idkecamatan"=>$row['idkecamatan'],
                "idkabupaten"=>$row['idkabupaten'],
                "idpropinsi"=>$row['idpropinsi'],
                "nama_kelurahan"=>$row['nama_kelurahan']
            );
        }else{
            $rdata = array(
                "idkelurahan"=>$this->input->post('idkelurahan'),
                "idkecamatan"=>$this->input->post('idkecamatan'),
                "idkabupaten"=>$this->input->post('idkabupaten'),
                "idpropinsi"=>$this->input->post('idpropinsi'),
                "nama_kelurahan"=>$this->input->post('nama_kelurahan')
            );
        }

        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

			//return $decodedToken;
    if ($decodedToken != false) {
    $xp = explode("_",$decodedToken);
if($xp[3]==$headers['Xkey']){

if($xp[4]>='3'){

    if($result=$this->M_kelurahan->addKelurahan($rdata)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
        
        }else{
            $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
            $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
           }
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Permission",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       
           
       
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
    return;	


    }

    public function index_get($idkec){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
   if($xp[4]>='1'){
   
       if($result=$this->M_kelurahan->getKelurahan($idkec)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
        
        }else{
            $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
            $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
           }
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Permission",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       
           
       
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
       return;	


    }

    public function index_put($id){

        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);

        $rdata = array(
            "idkelurahan"=>$this->PUT("idkelurahan"),
            "idpropinsi"=>$this->PUT("idpropinsi"),
            "idkabupaten"=>$this->PUT("idkabupaten"),
            "idkecamatan"=>$this->PUT("idkecamatan"),
            "nama_kelurahan"=>$this->PUT("nama_kelurahan")
        );


        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
   if($xp[4]>='3'){
   
       if($result=$this->M_kelurahan->updateKelurahan($rdata,$id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
     
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"No Permission",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	

    }

    public function index_delete($id){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
   if($xp[4]>='3'){
   
       if($result=$this->M_kelurahan->deleteKelurahan($id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
        
        }else{
            $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
            $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
           }
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Permission",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       
           
       
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
       return;	


    }

    public function detail_get($id){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
   if($xp[4]>='1'){
   
       if($result=$this->M_kelurahan->infoKelurahan($id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
        
        }else{
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"No Data");
        $this->set_response($response,REST_Controller::HTTP_OK);
           }
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Permission",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       
           
       
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
       return;	


    }




    public function listall_get($page,$size){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
   if($xp[4]>='1'){
   
       if($result=$this->M_kelurahan->getAllKelurahan($page,$size)){
        $total = $this->M_kelurahan->countAll();
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result,"total"=>$total);
        $this->set_response($response,REST_Controller::HTTP_OK);  
        
        }else{
            $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
            $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
           }
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Permission",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       
           
       
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
       return;	


    }



    public function search_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $keyword = $this->input->post('search');
        }else{
            $keyword = $row['search'];
        }

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
   if($xp[4]>='1'){
   
       if($result=$this->M_kelurahan->searchKelurahan($keyword)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
        
        }else{
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data","response"=>false);
            $this->set_response($response,REST_Controller::HTTP_OK);
           }
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Permission",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       
           
       
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
       return;	


    }


}