<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Kader extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->Model("admin/M_kader");
    }

    public function index_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        }
  


        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_kader->getComboListKader($rdata)){
                       
                    $response = array(
                        "status"=>REST_Controller::HTTP_OK,
                        "error"=>null,
                        "message"=>"data found",
                        "response"=>$id);    
            $this->set_response($response, REST_Controller::HTTP_OK);  
    
          
      
      
      
            }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "message"=>"data not found",
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
    }
}
?>