<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Follow extends REST_Controller{

        public function __construct(){
            parent::__construct();
            $this->load->Model("M_follow");
        }

        public function index_post(){
            $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = array(
               "idorgan isasi"=>$this->input->post('idorganisasi'),
                "username"=>$this->input->post('username')
            
            );
        }else{
            $rdata = array(
                "idorganisasi"=>$row['idorganisasi'],
                "username"=>$row['username']

            );
        }

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
   if($xp[4]>='1'){
   
       if($result=$this->M_follow->followOrganisasi($rdata)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
        
        }else{
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data");
            $this->set_response($response,REST_Controller::HTTP_OK);
           }
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Permission",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       
           
       
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
       return;	
        }



        public function index_delete($idorganisasi,$username){
            $headers=$this->input->request_headers();
            if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                   $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($xp[4]>='1'){
       
           if($result=$this->M_follow->unFollowOrganisasi($idorganisasi,$username)){
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
            $this->set_response($response,REST_Controller::HTTP_OK);  
            
            }else{
                $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data");
                $this->set_response($response,REST_Controller::HTTP_OK);
               }
             
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Permission",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           
               
           
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                       }
                       
                   }else{
                    $response = array(
                        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                        "error"=>"Invalid Token Authorization",
                    );
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
           
               }else{
           
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"No Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
           return;	


        }

        public function index_get($idorganisasi){
            $headers=$this->input->request_headers();
            if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                   $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($xp[4]>='1'){
       
           if($result=$this->M_follow->getFollower($idorganisasi)){
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
            $this->set_response($response,REST_Controller::HTTP_OK);  
            
            }else{
                $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data");
                $this->set_response($result,REST_Controller::HTTP_OK);
               }
             
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Permission",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           
               
           
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                       }
                       
                   }else{
                    $response = array(
                        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                        "error"=>"Invalid Token Authorization",
                    );
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
           
               }else{
           
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"No Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
           return;	
        }


        public function user_get($username){
            $headers=$this->input->request_headers();
            if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                   $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($xp[4]>='1'){
       
           if($result=$this->M_follow->getFollowing($username)){
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
            $this->set_response($response,REST_Controller::HTTP_OK);  
            
            }else{
                $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data");
                $this->set_response($response,REST_Controller::HTTP_OK);
               }
             
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Permission",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           
               
           
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                       }
                       
                   }else{
                    $response = array(
                        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                        "error"=>"Invalid Token Authorization",
                    );
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
           
               }else{
           
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"No Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
           return;	
        }


        public function totalfollower_get($idorganisasi){
            $headers=$this->input->request_headers();
            if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                   $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($xp[4]>='1'){
       
           if($result=$this->M_follow->getTotalFollower($idorganisasi)){
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>array('total'=>$result));
            $this->set_response($response,REST_Controller::HTTP_OK);  
            
            }else{
                $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data");
                $this->set_response($response,REST_Controller::HTTP_OK);
               }
             
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Permission",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           
               
           
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                       }
                       
                   }else{
                    $response = array(
                        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                        "error"=>"Invalid Token Authorization",
                    );
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
           
               }else{
           
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"No Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
           return;	
        }




        public function followstatus_get($username,$idorganisasi){

            $headers=$this->input->request_headers();
            if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                   $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($xp[4]>='1'){
       
           $result=$this->M_follow->getStatusFollow($username,$idorganisasi);
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
            $this->set_response($response,REST_Controller::HTTP_OK);  
            
           
             
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Permission",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           
               
           
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                       }
                       
                   }else{
                    $response = array(
                        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                        "error"=>"Invalid Token Authorization",
                    );
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
           
               }else{
           
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"No Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
           return;

        }
        public function followthread_post(){
            $data = file_get_contents("php://input");
            $row = json_decode($data,true);
            if($this->input->post()){
                $rdata = array(
                    "idorganisasi"=>$this->input->post('idorganisasi'),
                    "username"=>$this->input->post('username'),
                    "idthread"=>$this->input->post('idthread')

                
                );
            }else{
                $rdata = array(
                    "idorganisasi"=>$row['idorganisasi'],
                    "username"=>$row['username'],
                    "idthread"=>$row['thread']
    
                );
            }
    
            $headers=$this->input->request_headers();
            if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                   $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($xp[4]>='1'){
       
           if($result=$this->M_follow->followThread($rdata)){
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
            $this->set_response($response,REST_Controller::HTTP_OK);  
            
            }else{
                $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data");
                $this->set_response($response,REST_Controller::HTTP_OK);
               }
             
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Permission",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           
               
           
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                       }
                       
                   }else{
                    $response = array(
                        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                        "error"=>"Invalid Token Authorization",
                    );
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
           
               }else{
           
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"No Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
           return;	
        }


        public function followthread_delete($idthread,$username){

            $headers=$this->input->request_headers();
            if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                   $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($xp[4]>='1'){
       
           if($result=$this->M_follow->unFollowThread($idthread,$username)){
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
            $this->set_response($response,REST_Controller::HTTP_OK);  
            
            }else{
                $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data");
                $this->set_response($response,REST_Controller::HTTP_OK);
               }
             
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Permission",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           
               
           
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                       }
                       
                   }else{
                    $response = array(
                        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                        "error"=>"Invalid Token Authorization",
                    );
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
           
               }else{
           
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"No Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
           return;	



        }

}