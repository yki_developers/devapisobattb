<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Newscreening extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_newscreening');
    }



    private function notifikasi($screening,$idkontak,$playerids){
        if($screening=="umum"){
        $idfaskes = $this->M_newscreening->getDataUmumDetail($idkontak);
        $hasil = $this->M_newscreening->getHasilNotifUmum($idkontak);
        $nama = $idfaskes->sdu_nama;
        $hasilscreening = $hasil->hasil_screening;
        $string = "screening mandiri";
        }elseif($screening=="pekerja"){

            $idfaskes = $this->M_newscreening->getDataPekerjaDetail($idkontak);
            $hasil = $this->M_newscreening->getHasilNotifPekerja($idkontak);
            $nama = $idfaskes->sdp_nama;
            $hasilscreening = $hasil->hasil_screening;
            $string = "screening program kerjasama";

        }elseif($screening=="sekolah"){
            $idfaskes = $this->M_newscreening->getDataSekolahDetail($idkontak);
            $hasil = $this->M_newscreening->getHasilNotifSekolah($idkontak);
            $nama = $idfaskes->dd_nama;
            $hasilscreening = $hasil->hasil_screening;
            $string = "screening program kerjasama";


        }elseif($screening=="anak_sekolah"){
            $idfaskes = $this->M_newscreening->getDataAnakSekolahDetail($idkontak);
            $hasil = $this->M_newscreening->getHasilNotifAnakSekolah($idkontak);
            $nama = $idfaskes->dd_nama;
            $hasilscreening = $hasil->hasil_screening;
            $string = "screening program kerjasama";



        }elseif($screening=="anak"){

            $idfaskes = $this->M_newscreening->getDataAnakDetail($idkontak);
            $hasil = $this->M_newscreening->getHasilNotifAnak($idkontak);
            $nama = $idfaskes->dd_nama;
            $hasilscreening = $hasil->hasil_screening;
            $string = "screening mandiri";

        }

        $content = array(
            "en"=>"Hasil  ".$string." atas nama ".strtoupper($nama)."  : ".$hasilscreening,
            "id"=>"Hasil ".$string." atas nama ".strtoupper($nama)."  : ".$hasilscreening);
        //$id = $this->input->post('idcontent');

        $data = array(
            "app_id"=>$this->config->item('OS_API_ID'),
         "include_player_ids"=>$playerids,
            "data"=>array("idcontent"=>"info"),
            "contents"=>$content
        );

        $jsondata = json_encode($data);
        //print_r($jsondata);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic NDgyODNjZTItOTk2Zi00ZjllLTg5NDAtNjljM2E2ZWY5MWU3'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsondata);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
        $response = curl_exec($ch);
        curl_close($ch);
        
     //print_r($response);

    }


    

    public function checktoken_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);

        if(!$this->input->post()){
            $rdata =$row;
        }else{

            $rdata = $this->input->post();

        }
        $response = $this->M_newscreening->getToken($rdata['token']);
        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function pekerja_post(){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);

        if(!$this->input->post()){
            $rdata =$row;
        }else{

            $rdata = $this->input->post();

        }


        
        foreach($rdata as $key=>$value){
            if(preg_match("/^sdp_/",$key)){
              $datadasar[$key] = $value;
            }elseif(preg_match("/^sp_/",$key)){
                $datascreening[$key] = $value;
            }elseif(preg_match("/^input_/",$key)){
                $datascreening[$key] = $value;
            }
        }



        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($id=$this->M_newscreening->inDataPekerja($datadasar)){

        $datascreening['sp_sdp_id'] = $id;

        if($result = $this->M_newscreening->inDataScreeningPekerja($datascreening)){

            $res = $this->M_newscreening->getHasilPekerja($result);

        $finalres = explode(" - ",$res->hasil_screening);
        switch($finalres[0]){
            case "Bukan Terduga TBC":
                $warna = "#66bb6a";
                $instruction = "Tetap Menjaga kesehatan dengan mengkonsumsi makanan gizi seimbang dan melakukan aktifitas fisik serta menerapkan etika batuk.";
                break;
                case "Terduga TBC":
                    $warna = "#ef5350";
                    $instruction = "Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                    break;
                    case "Terduga TBC RO":
                        $warna="#d32f2f";
                        $instruction = "Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                        break;
                        case "Menjalani Pengobatan":
                            $warna="#ffa726";
                        $instruction = "Pastikan anda tetap melakukan pengobatan rutin sampai sembuh dan menjaga kesehatan dengan mengkonsumsi makanan gizi seimbang dan melakukan aktifitas fisik serta menerapkan etika batuk. ";
                            break;
                            default:
                            $warna="#d32f2f";
                            $instruction="Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
        }


        if($res->kode_propinsi=='31'){

            // $finalres = explode(" - ",$res->hasil_screening);
             if(count($finalres)>1){
                 $f1 = $finalres[0];
                 $f2 = $finalres[1];
                 $resp = array("hasil"=>$finalres[0]." - ".$res->hasil_screening_hiv,"kriteria"=>$finalres[1],"warna"=>$warna,"instruksi"=>$instruction,"sp_id"=>$result,"nama"=>$datadasar['sdp_nama']);
                 }else{
                     $f1 = $finalres[0];
                 $f2 = null;
                     $resp = array("hasil"=>$finalres[0]." - ".$res->hasil_screening_hiv,"kriteria"=>null,"warna"=>$warna,"instruksi"=>$instruction,"sp_id"=>$result,"nama"=>$datadasar['sdp_nama']);
                 }
 
             }else{
                // $finalres = explode(" - ",$res->hasil_screening);
 
                 if(count($finalres)>1){
                     $f1 = $finalres[0];
                     $f2 = $finalres[1];
                     $resp = array("hasil"=>$finalres[0],"kriteria"=>$finalres[1],"warna"=>$warna,"instruksi"=>$instruction,"sp_id"=>$result,"nama"=>$datadasar['sdp_nama']);
                     }else{
                         $f1 = $finalres[0];
                     $f2 = null;
                         $resp = array("hasil"=>$finalres[0],"kriteria"=>null,"warna"=>$warna,"instruksi"=>$instruction,"sp_id"=>$result,"nama"=>$datadasar['sdp_nama']);
                     }
 
 
             }

        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK);  

   
    $idkontak = $this->M_newscreening->getDataPekerjaDetail($id);
    $playerids = $this->M_newscreening->getPlayerId($idkontak->program_puskesmas);
    $reslt = explode(" - ",$resp['hasil']);
    if($reslt[0]=='Terduga TBC'){
        if(isset($playerids)){
           foreach($playerids as $playid){
           $players[] = $playid->playerid;
           if(isset($playid->playerid)){

           $dataNotif = array(
            "notifikasi_playerid"=>$playid->playerid,
            "notifikasi_username"=>$playid->username,
            "notifikasi_text"=>"Hasil screening Program Kerjasama atas nama ".strtoupper($idkontak->sdp_nama)."  : ".$res->hasil_screening);

            $this->M_newscreening->insertNotifikasi($dataNotif);
            $this->notifikasi("pekerja",$id,$players);
        
        }


           }
       }
     
   }

  

        }else{
            $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
            $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);

        }
    }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }







    public function sekolahdewasa_post(){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);

        if(!$this->input->post()){
            $rdata =$row;
        }else{

            $rdata = $this->input->post();

        }


        
        foreach($rdata as $key=>$value){
            if(preg_match("/^dd_/",$key)){
              $datadasar[$key] = $value;
            }elseif(preg_match("/^sds_/",$key)){
                $datascreening[$key] = $value;
            }elseif(preg_match("/^input_/",$key)){
                $datascreening[$key] = $value;
            }
        }



        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($id=$this->M_newscreening->inDataSekolah($datadasar)){

        $datascreening['sds_datadasar_id'] = $id;

        if($result = $this->M_newscreening->inDataScreeningSekolahDewasa($datascreening)){

            $res = $this->M_newscreening->getHasilSekolahDewasa($result);


            $finalres = explode(" - ",$res->hasil_screening);
            switch($finalres[0]){
                case "Bukan Terduga TBC":
                    $warna = "#66bb6a";
                    $instruction = "Tetap Menjaga kesehatan dengan mengkonsumsi makanan gizi seimbang dan melakukan aktifitas fisik serta menerapkan etika batuk.";
                    break;
                    case "Terduga TBC":
                        $warna = "#ef5350";
                        $instruction = "Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                        break;
                        case "Terduga TBC RO":
                            $warna="#d32f2f";
                            $instruction = "Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                            break;
                            case "Menjalani Pengobatan":
                                $warna="#ffa726";
                            $instruction = "Pastikan anda tetap melakukan pengobatan rutin sampai sembuh dan menjaga kesehatan dengan mengkonsumsi makanan gizi seimbang dan melakukan aktifitas fisik serta menerapkan etika batuk. ";
                                break;
                                default:
                                $warna="#d32f2f";
                                $instruction="Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
            }


            

                if($res->kode_propinsi=='31'){

                    // $finalres = explode(" - ",$res->hasil_screening);
                     if(count($finalres)>1){
                         $f1 = $finalres[0];
                         $f2 = $finalres[1];
                         $resp = array("hasil"=>$finalres[0]." - ".$res->hasil_screening_hiv,"kriteria"=>$finalres[1],"warna"=>$warna,"instruksi"=>$instruction,"sds_id"=>$result,"nama"=>$datadasar['dd_nama']);
                         }else{
                             $f1 = $finalres[0];
                         $f2 = null;
                             $resp = array("hasil"=>$finalres[0]." - ".$res->hasil_screening_hiv,"kriteria"=>null,"warna"=>$warna,"instruksi"=>$instruction,"sds_id"=>$result,"nama"=>$datadasar['dd_nama']);
                         }
         
                     }else{
                        // $finalres = explode(" - ",$res->hasil_screening);
         
                         if(count($finalres)>1){
                             $f1 = $finalres[0];
                             $f2 = $finalres[1];
                             $resp = array("hasil"=>$finalres[0],"kriteria"=>$finalres[1],"warna"=>$warna,"instruksi"=>$instruction,"sds_id"=>$result,"nama"=>$datadasar['dd_nama']);
                             }else{
                                 $f1 = $finalres[0];
                             $f2 = null;
                                 $resp = array("hasil"=>$finalres[0],"kriteria"=>null,"warna"=>$warna,"instruksi"=>$instruction,"sds_id"=>$result,"nama"=>$datadasar['dd_nama']);
                             }
         
         
                     }



           // $resp = array("hasil"=>$finalres[0],"kriteria"=>$finalres[1]);


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK); 




    $idkontak = $this->M_newscreening->getDataSekolahDetail($id);
    $playerids = $this->M_newscreening->getPlayerId($idkontak->program_puskesmas);
    $reslt = explode(" - ",$resp['hasil']);
    if($reslt[0]=='Terduga TBC'){
        if(isset($playerids)){
           foreach($playerids as $playid){
           $players[] = $playid->playerid;
           if(isset($playid->playerid)){

           $dataNotif = array(
            "notifikasi_playerid"=>$playid->playerid,
            "notifikasi_username"=>$playid->username,
            "notifikasi_text"=>"Hasil screening Program Kerjasama atas nama ".strtoupper($idkontak->dd_nama)." : ".$res->hasil_screening);

            $this->M_newscreening->insertNotifikasi($dataNotif);
            $this->notifikasi("sekolah",$id,$players);
        
        }


           }
       }
     
   }

  

        }else{
            $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
            $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);

        }
    }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }






    public function sekolahanak_post(){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);

        if(!$this->input->post()){
            $rdata =$row;
        }else{

            $rdata = $this->input->post();

        }


        
        foreach($rdata as $key=>$value){
            if(preg_match("/^dd_/",$key)){
              $datadasar[$key] = $value;
            }elseif(preg_match("/^sas_/",$key)){
                $datascreening[$key] = $value;
            }elseif(preg_match("/^input_/",$key)){
                $datascreening[$key] = $value;
            }
        }



        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($id=$this->M_newscreening->inDataSekolah($datadasar)){

        $datascreening['sas_datadasar_id'] = $id;

        if($result = $this->M_newscreening->inDataScreeningSekolahAnak($datascreening)){

            $res = $this->M_newscreening->getHasilSekolahAnak($result);
            //echo $res;
            $finalres = explode(" - ",$res->hasil_screening);

            switch($finalres[0]){
                case "Bukan Terduga TBC":
                    $warna = "#66bb6a";
                    $instruction = "Tetap Menjaga kesehatan dengan mengkonsumsi makanan gizi seimbang dan melakukan aktifitas fisik serta menerapkan etika batuk.";
                    break;
                    case "Terduga TBC":
                        $warna = "#ef5350";
                        $instruction = "Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                        break;
                        case "Terduga TBC RO":
                            $warna="#d32f2f";
                            $instruction = "Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                            break;
                            case "Menjalani Pengobatan":
                                $warna="#ffa726";
                            $instruction = "Pastikan anda tetap melakukan pengobatan rutin sampai sembuh dan menjaga kesehatan dengan mengkonsumsi makanan gizi seimbang dan melakukan aktifitas fisik serta menerapkan etika batuk. ";
                                break;
                                default:
                                $warna="#d32f2f";
                                $instruction="Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
            }


            if(count($finalres)>1){
                $resp = array("hasil"=>$finalres[0],"kriteria"=>$finalres[1],"warna"=>$warna,"instruksi"=>$instruction,"sas_id"=>$result,"nama"=>$datadasar['dd_nama']);
                }else{
                    $resp = array("hasil"=>$finalres[0],"kriteria"=>null,"warna"=>$warna,"instruksi"=>$instruction,"sas_id"=>$result,"nama"=>$datadasar['dd_nama']);
                }


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK);  



    $idkontak = $this->M_newscreening->getDataAnakSekolahDetail($id);
    $playerids = $this->M_newscreening->getPlayerId($idkontak->program_puskesmas);
    $reslt = explode(" - ",$resp['hasil']);
    if($reslt[0]=='Terduga TBC'){
        if(isset($playerids)){
           foreach($playerids as $playid){
           $players[] = $playid->playerid;
           if(isset($playid->playerid)){

           $dataNotif = array(
            "notifikasi_playerid"=>$playid->playerid,
            "notifikasi_username"=>$playid->username,
            "notifikasi_text"=>"Hasil screening Program Kerjasama atas nama ".strtoupper($idkontak->dd_nama)."  : ".$res->hasil_screening);

            $this->M_newscreening->insertNotifikasi($dataNotif);
            $this->notifikasi("sekolah_anak",$id,$players);
        
        }


           }
       }
     
   }

        }else{
            $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
            $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);

        }
    }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }

   


    public function anak_post(){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);

        if(!$this->input->post()){
            $rdata =$row;
        }else{

            $rdata = $this->input->post();

        }


        
        foreach($rdata as $key=>$value){
            if(preg_match("/^dd_/",$key)){
              $datadasar[$key] = $value;
            }elseif(preg_match("/^sa_/",$key)){
                $datascreening[$key] = $value;
            }elseif(preg_match("/^input_/",$key)){
                $datascreening[$key] = $value;
            }
        }



        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($id=$this->M_newscreening->inDataSekolah($datadasar)){

        $datascreening['sa_datadasar_id'] = $id;

        if($result = $this->M_newscreening->inDataScreeningAnak($datascreening)){

            $res = $this->M_newscreening->getHasilAnak($result);
            //print_r($res);
            $finalres = explode(" - ",$res->hasil_screening);

            switch($finalres[0]){
                case "Bukan Terduga TBC":
                    $warna = "#66bb6a";
                    $instruction = "Tetap Menjaga kesehatan dengan mengkonsumsi makanan gizi seimbang dan melakukan aktifitas fisik serta menerapkan etika batuk.";
                    break;
                    case "Terduga TBC":
                        $warna = "#ef5350";
                        $instruction = "Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                        break;
                        case "Terduga TBC RO":
                            $warna="#d32f2f";
                            $instruction = "Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                            break;
                            case "Menjalani Pengobatan":
                                $warna="#ffa726";
                            $instruction = "Pastikan anda tetap melakukan pengobatan rutin sampai sembuh dan menjaga kesehatan dengan mengkonsumsi makanan gizi seimbang dan melakukan aktifitas fisik serta menerapkan etika batuk. ";
                                break;
                                default:
                                $warna="#d32f2f";
                                $instruction="Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
            }


            if(count($finalres)>1){
                $resp = array("hasil"=>$finalres[0],"kriteria"=>$finalres[1],"warna"=>$warna,"instruksi"=>$instruction,"sa_id"=>$result,"nama"=>$datadasar['dd_nama']);
                }else{
                    $resp = array("hasil"=>$finalres[0],"kriteria"=>null,"warna"=>$warna,"instruksi"=>$instruction,"sa_id"=>$result,"nama"=>$datadasar['dd_nama']);
                }



                

        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK);  
        }else{
            $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
            $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);

        }
    }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }

    public function umum_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);

        if(!$this->input->post()){
            $rdata =$row;
        }else{

            $rdata = $this->input->post();

        }


        
        foreach($rdata as $key=>$value){
            if(preg_match("/^sdu_/",$key)){
              $datadasar[$key] = $value;
            }elseif(preg_match("/^sc_/",$key)){
                $datascreening[$key] = $value;
            }elseif(preg_match("/^input_/",$key)){
                $datascreening[$key] = $value;
            }
        }



        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($id=$this->M_newscreening->inDataDasarUmum($datadasar)){

        $datascreening['sc_sdu_id'] = $id;

        if($result = $this->M_newscreening->inDataScreeningUmum($datascreening)){
            //echo $result;
            //die();

            $res = $this->M_newscreening->getHasilUmum($result);
           
            

            $finalres = explode(" - ",$res->hasil_screening);

            switch($finalres[0]){
                case "Bukan Terduga TBC":
                    $warna = "#66bb6a";
                    $instruction = "Tetap Menjaga kesehatan dengan mengkonsumsi makanan gizi seimbang dan melakukan aktifitas fisik serta menerapkan etika batuk.";
                    break;
                    case "Terduga TBC":
                        $warna = "#ef5350";
                        $instruction = "Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                        break;
                        case "Terduga TBC RO":
                            $warna="#d32f2f";
                            $instruction = "Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                            break;
                            case "Menjalani Pengobatan":
                                $warna="#ffa726";
                            $instruction = "Pastikan anda tetap melakukan pengobatan rutin sampai sembuh dan menjaga kesehatan dengan mengkonsumsi makanan gizi seimbang dan melakukan aktifitas fisik serta menerapkan etika batuk. ";
                                break;
                                default:
                                $warna="#d32f2f";
                                $instruction="Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
            }


          


                if($res->kode_propinsi=='31'){

                   // $finalres = explode(" - ",$res->hasil_screening);
                    if(count($finalres)>1){
                        $f1 = $finalres[0];
                        $f2 = $finalres[1];
                        $resp = array("hasil"=>$finalres[0]." - ".$res->hasil_screening_hiv,"kriteria"=>$finalres[1],"warna"=>$warna,"instruksi"=>$instruction,"sc_id"=>$result,"nama"=>$datadasar['sdu_nama']);
                        }else{
                            $f1 = $finalres[0];
                        $f2 = null;
                            $resp = array("hasil"=>$finalres[0]." - ".$res->hasil_screening_hiv,"kriteria"=>null,"warna"=>$warna,"instruksi"=>$instruction,"sc_id"=>$result,"nama"=>$datadasar['sdu_nama']);
                        }
        
                    }else{
                       // $finalres = explode(" - ",$res->hasil_screening);
        
                        if(count($finalres)>1){
                            $f1 = $finalres[0];
                            $f2 = $finalres[1];
                            $resp = array("hasil"=>$finalres[0],"kriteria"=>$finalres[1],"warna"=>$warna,"instruksi"=>$instruction,"sc_id"=>$result,"nama"=>$datadasar['sdu_nama']);
                            }else{
                                $f1 = $finalres[0];
                            $f2 = null;
                                $resp = array("hasil"=>$finalres[0],"kriteria"=>null,"warna"=>$warna,"instruksi"=>$instruction,"sc_id"=>$result,"nama"=>$datadasar['sdu_nama']);
                            }
        
        
                    }
 

            $arrData = array(
                "sc_id"=>$res->sc_id,
                "nama_pengguna"=>$res->sdu_nama,
                "nohp"=>$res->sdu_phone,
                "alamat_pengguna"=>$res->sdu_alamat,
                "sc_date"=>$res->tgl_screening,
                "status_rtl"=>$res->status_rtl,
                "status_keterangan"=>$res->status_keterangan,
                "nama_propinsi"=>$res->nama_propinsi,
                "nama_kabupaten"=>$res->nama_kabupaten,
                "nama_fasyankes"=>$res->nama_fasyankes,
                "hf_code"=>$res->hf_code,
                "input_user"=>$res->input_user,
                "province_code"=>$res->province_code,
                "district_code"=>$res->district_code,
                "screening_date"=>$res->sc_date,
                "text_hasil"=>$f1,
                "kriteria"=>$f2,
                "jenis"=>"dewasa_umum",
                "diperiksa_tbc"=>$res->diperiksa_tbc,
                "sakit_tbc"=>$res->sakit_tbc,
                "mendapat_tpt"=>$res->mendapat_tpt
            );

            if($this->M_newscreening->inHasilPerfaskes($arrData)){

                $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$resp);
                $this->set_response($response,REST_Controller::HTTP_OK);  


                


            }else{
                $this->M_newscreening->delDataDasarUmum($id);
                $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error","message"=>"Gagal Input");
                $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }
           


            //notifikasi umum 
            $playerids = $this->M_newscreening->getPlayerId($datadasar['sdu_puskesmas']);
            $idkontak = $this->M_newscreening->getDataUmumDetail($id);
            $reslt = explode(" - ",$resp['hasil']);
            if($reslt[0]=='Terduga TBC'){
               if(isset($playerids)){
                   foreach($playerids as $playid){
                   $players[] = $playid->playerid;
                   if(isset($playid->playerid)){
                // print_r($resp);

                   $dataNotif = array(
                    "notifikasi_playerid"=>$playid->playerid,
                    "notifikasi_username"=>$playid->username,
                    "notifikasi_text"=>"Hasil screening mandiri atas nama ".strtoupper($idkontak->sdu_nama)."  : ".$res->hasil_screening);
    
                    $this->M_newscreening->insertNotifikasi($dataNotif);
                    $this->notifikasi("umum",$id,$players);
                
                }


                   }
               }
             
           }
           
          

       
        }else{
            $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
            $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);

        }
    }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  

    }


    public function getdatadasar_get($metode){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){

    if($metode=='umum'){
        $result = $this->M_newscreening->getDataDasarUmum();

    }elseif($metode=='pekerja'){
        $result = $this->M_newscreening->getDataDasarKerjasama();

    }elseif($metode=='anak'){
        $result = $this->M_newscreening->getDataDasarAnak();

    }
   
 
        if($result){

        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
        }else{
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"response"=>null,"message"=>"no data");
            $this->set_response($response,REST_Controller::HTTP_OK);

        }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;
    }





    public function gethistory_get($username){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){

$result = $this->M_newscreening->getHistory($username);
 
        if($result){

        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
        }else{
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"response"=>null,"message"=>"no data");
            $this->set_response($response,REST_Controller::HTTP_OK);

        }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;
    }







    public function umum_put($id){
        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);
    
        $rdata = $this->put();
    


        
        foreach($rdata as $key=>$value){
            if(preg_match("/^sdu_/",$key)){
              $datadasar[$key] = $value;
            }elseif(preg_match("/^sc_/",$key)){
                $datascreening[$key] = $value;
            }elseif(preg_match("/^input_/",$key)){
                $datascreening[$key] = $value;
            }
        }



        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($resp=$this->M_newscreening->updateScreeningUmum($rdata,$id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$resp);
        $this->set_response($response,REST_Controller::HTTP_OK);  


    }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  

    }



    public function anak_put($id){
        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);
    
        $rdata = $this->put();
    


        
        foreach($rdata as $key=>$value){
            if(preg_match("/^dd_/",$key)){
                $datadasar[$key] = $value;
              }elseif(preg_match("/^sa_/",$key)){
                  $datascreening[$key] = $value;
              }elseif(preg_match("/^input_/",$key)){
                  $datascreening[$key] = $value;
              }
        }



        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($resp=$this->M_newscreening->updateScreeningAnak($rdata,$id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$resp);
        $this->set_response($response,REST_Controller::HTTP_OK);  


    }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  

    }





    public function pekerja_put($id){
        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);
    
        $rdata = $this->put();
    


        
        foreach($rdata as $key=>$value){
            if(preg_match("/^sdp_/",$key)){
                $datadasar[$key] = $value;
              }elseif(preg_match("/^sp_/",$key)){
                  $datascreening[$key] = $value;
              }elseif(preg_match("/^input_/",$key)){
                  $datascreening[$key] = $value;
              }
        }



        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($resp=$this->M_newscreening->updateScreeningPekerja($rdata,$id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$resp);
        $this->set_response($response,REST_Controller::HTTP_OK);  


    }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  

    }
    


    public function sekolahdewasa_put($id){
        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);
    
        $rdata = $this->put();
    


        
        foreach($rdata as $key=>$value){
            if(preg_match("/^dd_/",$key)){
                $datadasar[$key] = $value;
              }elseif(preg_match("/^sds_/",$key)){
                  $datascreening[$key] = $value;
              }elseif(preg_match("/^input_/",$key)){
                  $datascreening[$key] = $value;
              }
        }



        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($resp=$this->M_newscreening->updateScreeningSekolahDewasa($rdata,$id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$resp);
        $this->set_response($response,REST_Controller::HTTP_OK);  


    }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  

    }



    public function sekolahanak_put($id){
        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);
    
        $rdata = $this->put();
    


        
        foreach($rdata as $key=>$value){
            if(preg_match("/^dd_/",$key)){
              $datadasar[$key] = $value;
            }elseif(preg_match("/^sas_/",$key)){
                $datascreening[$key] = $value;
            }elseif(preg_match("/^input_/",$key)){
                $datascreening[$key] = $value;
            }
        }



        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($resp=$this->M_newscreening->updateScreeningSekolahAnak($rdata,$id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$resp);
        $this->set_response($response,REST_Controller::HTTP_OK);  


    }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  

    }

    public function fback_put($mode,$id,$kode){
        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);
        $rdata = $this->put();




        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
   /*    if($resp=$this->M_newscreening->updateScreeningSekolahAnak($rdata,$id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$resp);
        $this->set_response($response,REST_Controller::HTTP_OK);  


    }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
     
*/
   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    


    }

}