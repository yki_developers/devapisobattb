<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Dashboardnasional extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('dashboard/M_dashboard');
    }

    public function index_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);

        if(!$this->input->post()){
            $rdata =$row;
        }else{

            $rdata = $this->input->post();

        }

        $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_dashboard->CapaianNasional($rdata)){
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "message"=>"Accepted",
           "response"=>$id              
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>"Data not found" ,
           "response"=>null              
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }


    public function propinsi_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);

        if(!$this->input->post()){
            $rdata =$row;
        }else{

            $rdata = $this->input->post();

        }

        $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_dashboard->CapaianPropinsi($rdata)){
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "message"=>"Accepted",
           "response"=>$id              
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>"Data not found" ,
           "response"=>null              
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }


    public function kabupaten_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);

        if(!$this->input->post()){
            $rdata =$row;
        }else{

            $rdata = $this->input->post();

        }

        $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_dashboard->CapaianKabupaten($rdata)){
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "message"=>"Accepted",
           "response"=>$id              
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>"Data not found" ,
           "response"=>null              
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }


    public function piedata_post($tipedata){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);

        if(!$this->input->post()){
            $rdata =$row;
        }else{

            $rdata = $this->input->post();

        }

        $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_dashboard->pieNasional($tipedata,$rdata)){
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "message"=>"Accepted",
           "response"=>$id              
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>"Data not found" ,
           "response"=>null              
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }



    public function cascade_post($skrining){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);

        if(!$this->input->post()){
            $rdata =$row;
        }else{

            $rdata = $this->input->post();

        }

        $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
            if($skrining=='pusat'){
                $id=$this->M_dashboard->cascadeSkriningPusat($rdata);
            }elseif($skrining=='propinsi'){
                $propinsi=$rdata['propinsi'];
                unset($rdata['propinsi']);

                $id=$this->M_dashboard->cascadeSkriningPropinsi($rdata,$propinsi);
            }elseif($skrining=='kabupaten'){
                $kabupaten=$rdata['kabupaten'];
                unset($rdata['kabupaten']);
                $id=$this->M_dashboard->cascadeSkriningKabupaten($rdata,$kabupaten);
            }elseif($skrining=='fasyankes'){
                $fasyankes=$rdata['fasyankes'];
                unset($rdata['fasyankes']);
                $id=$this->M_dashboard->cascadeSkriningFasyankes($rdata,$fasyankes);
            }            
            
            if($id){
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "message"=>"Accepted",
           "response"=>$id              
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>"Data not found" ,
           "response"=>null              
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }


}