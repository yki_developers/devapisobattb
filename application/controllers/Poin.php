<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Poin extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_poin');
    }

    public function index_get(){
        

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){

//$result = $this->M_newscreening->getHistory($username);
 $result = $this->M_poin->getTopTen();
        if($result){

        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
        }else{
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"response"=>null,"message"=>"no data");
            $this->set_response($response,REST_Controller::HTTP_OK);

        }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;


    }




    public function arsip_get($bln,$thn){
       

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){

//$result = $this->M_newscreening->getHistory($username);
 $result = $this->M_poin->getTopTenArsip($bln,$thn);
        if($result){

        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
        }else{
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"response"=>null,"message"=>"no data");
            $this->set_response($response,REST_Controller::HTTP_OK);

        }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;


    }


    public function mypoin_get($username){
        

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){

//$result = $this->M_newscreening->getHistory($username);
if($list = $this->M_poin->listPoin()){
    $i=1;
    foreach($list as $rank){
        $id = $rank->poin_user;
        $net[$id] = $i;

    $i++;
}
}
 $result = $this->M_poin->getMyPoin(urldecode($username));
        if($result){
          
          //  print_r($result);

           foreach($result as $nres){
               $total = $nres->total_poin;
               $rank = $net[$username];


           }

           unset($result[0]->total_poin);
           unset($result[0]->poin_total);

           $result[0]->total_poin = $total;
           $result[0]->poin_total = $total;
           $result[0]->ranking = $rank;


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
        }else{
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"response"=>null,"message"=>"no data");
            $this->set_response($response,REST_Controller::HTTP_OK);

        }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;


    }


    public function checkin_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        } 


        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){



$lastDay = $this->M_poin->checkDay($rdata['username']);

if($lastDay){
    $last = $lastDay->dp_checkin_day;
    if($last<'3'){
        $poin = '1';
        $day = $last+'1';
        $p = "1";
    }elseif($last>='3' && $last<'5'){
        $poin = '2';
        $day = $last+'1';
        $p ="2";
    }elseif($last>='5' && $last<'7'){
        $poin = '5';
        $day = $last+'1';
        $p='3';
    }else{
        $p = "4";
        $poin = '1';
        $day = '1';
    }
}else{
    $poin = '1';
    $day = '1';
    $p='0';
}

$Qdata = array(
    "dp_username"=>$rdata['username'],
    "dp_checkin_day"=>$day,
    "dp_poin_value"=>$poin,
    "dp_datetime"=>date("Y-m-d H:i:s")
);
//$result = $this->M_newscreening->getHistory($username);
 //$result = $this->M_poin->getMyPoin($username);
 $check = $this->M_poin->lastCheck($rdata['username']);       
 if($check==0){
 if($this->M_poin->dailyCheckin($Qdata)){
            $ret = array("point"=>$poin,"checkin_status"=>'true');

        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$ret);
    $this->set_response($response,REST_Controller::HTTP_OK);  
        }else{
            $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"500","response"=>null,"message"=>"CHECKIN FAILED");
            $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);

        }

    }else{

        $response = array(
            "status"=>REST_Controller::HTTP_BAD_REQUEST,
            "error"=>"Sudah Pernah Checkin"
        );
        $this->set_response($response,REST_controller::HTTP_BAD_REQUEST);



    }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;




    }



    public function reward_get(){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){

//$result = $this->M_newscreening->getHistory($username);
 $result = $this->M_poin->getListReward();
        if($result){

        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
        }else{
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"response"=>null,"message"=>"no data");
            $this->set_response($response,REST_Controller::HTTP_OK);

        }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;



    }

    public function sharepoin_get($username,$artikel_id){







        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){



    $check = $this->M_poin->checkUserShare($username,$artikel_id);
           if($check==0){
               $checkTime = $this->M_poin->checkSharePoin($username);
               if(isset($checkTime)){
               if( $checkTime->tstatus=='1'){
                $rdata = array("psa_username"=>$username,"psa_artikel_id"=>$artikel_id,"psa_poin_value"=>"1");
                $result = $this->M_poin->addPoinShare($rdata);
            }
        }else{
            $rdata = array("psa_username"=>$username,"psa_artikel_id"=>$artikel_id,"psa_poin_value"=>"1");
            $result = $this->M_poin->addPoinShare($rdata);
        }

        if($result){

            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
            }else{
                $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"response"=>null,"message"=>"no data");
                $this->set_response($response,REST_Controller::HTTP_OK);
    
            }
         
    
           }else{
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"response"=>null,"message"=>"no data");
            $this->set_response($response,REST_Controller::HTTP_OK);
           }



//$result = $this->M_newscreening->getHistory($username);


      
   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;


    }

}
?>