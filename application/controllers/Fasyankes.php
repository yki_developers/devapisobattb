<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
Class Fasyankes extends  REST_controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('M_faskes');
        $this->load->library("images");
    }



    public function search_post()
    {
    
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $search = $this->input->post('search');
            $page = $this->input->post('page');
            $size = $this->input->post('size');
        }else{
            $search = $row['search'];
            $page = $row['page'];
            $size = $row['size'];
        }
        
        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

            if ($decodedToken != false) {
                
                if($idfasyankes = $this->M_faskes->SearchFasyankes($search,$page,$size)){
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null, 
                    "response"=>$idfasyankes
                );
            }else{
                    $response = array(
                        "status"=>REST_Controller::HTTP_OK,
                        "error"=>null, 
                        "message"=>"no data",
                        "response"=>false
                    );
            }
				//echo $idlab;
                $this->set_response($response, REST_Controller::HTTP_OK);
                
            }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
				
			}


     }else{
         $response = array(
             "status"=>REST_Controller::HTTP_UNAUTHORIZED,
             "error"=>"No Token Authorization",
         );
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
     }

    }

  

public function nearme_post()
    {
    
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $lat = $this->input->post('lat');
            $lon = $this->input->post('lon');
            $page = $this->input->post('page');
            $size = $this->input->post('size');
        }else{
            $lat = $row['lat'];
            $lon = $row['lon'];
            $page = $row['page'];
            $size = $row['size'];
        }
        
        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

            if ($decodedToken != false) {
                
                $idfasyankes = $this->M_faskes->ListFasyankesNearme($lat,$lon,$page,$size);
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null, 
                    "response"=>$idfasyankes
                    
                );
				//echo $idlab;
                $this->set_response($response, REST_Controller::HTTP_OK);
                
            }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
				
			}


     }else{
         $response = array(
             "status"=>REST_Controller::HTTP_UNAUTHORIZED,
             "error"=>"No Token Authorization",
         );
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
     }

    }


public function fasyankes_get($kabupaten,$page="1",$size="10",$order="DESC")
    {
    
       
        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

            if ($decodedToken != false) {
                
                $idfasyankes = $this->M_faskes->ListFasyankes($kabupaten,$page,$size,$order);
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null, 
                    "response"=>$idfasyankes
                    
                );
				//echo $idlab;
                $this->set_response($response, REST_Controller::HTTP_OK);
                
            }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
				
			}


     }else{
         $response = array(
             "status"=>REST_Controller::HTTP_UNAUTHORIZED,
             "error"=>"No Token Authorization",
         );
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
     }

    }
   
    public function detailFasyankes_get($idfaskes){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               if ($decodedToken != false) {
                   $idfasyankes = $this->M_faskes->detailFasyankes($idfaskes);
                   //echo $idlab;
                   $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null, 
                    "response"=>$idfasyankes
                    
                );
				//echo $idlab;
                $this->set_response($response, REST_Controller::HTTP_OK);
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   
               }

    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
}
    public function fasyankes_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $insertArray = $this->input->post();
        }else{
            $insertArray = $row;
        }
        


      /*  $path = "./assets/uploaded/fasyankes/";
        $thumbnail = "./assets/uploaded/fasyankes/thumbnail/";
        $foto = date("Ymd")."_".$insertArray['kdfasyankes'].".jpg";
        $nsrc = $path.$foto;
        $tsrc = $thumbnail.$foto;
        $this->images->base64tojpg($nsrc,$String64,$tsrc,512,512);
        $this->images->base64tojpg($nsrc,$String64,$nsrc,1125,450);
        $imgSrc = base_url()."assets/uploaded/fasyankes/".$foto;
        $tmbSrc = base_url()."assets/uploaded/fasyankes/thumbnail/".$foto;

        $insertArray['foto'] = $imgSrc;
        $insertArray['thumbnails']=$tmbSrc;
        */


        //$newArray = array_push($insertArray,"foto"=>$imgSrc,"thumbnail"=>$tmbSrc);

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               if ($decodedToken != false) {
                   if($id = $this->M_faskes->insertNew($insertArray)){
                       $response = array(
                           "status"=>REST_Controller::HTTP_OK,
                           "error"=>null, 
                           "message"=>"Accepted");
                           $this->set_response($response, REST_Controller::HTTP_OK);

                   }else{
                    $response = array(
                        "status"=>REST_Controller::HTTP_OK,
                        "error"=>null,
                        "message"=>"Insert Error",
                        "response"=>false
                    );
                        $this->set_response($response, REST_Controller::HTTP_OK);
                   };
                   //echo $idlab;
                 
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   
               }


    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
}
    

public function profile_put($idfaskes){

    $data = file_get_contents("php://input","PUT");
    $row = json_decode($data,true);

    $insertArray = $this->put();
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           if ($decodedToken != false) {
               if($id = $this->M_faskes->updateFaskes($insertArray,$idfaskes)){
                  $response = array(
                      "status"=>REST_Controller::HTTP_OK,
                      "error"=>null, 
                      "message"=>"Accepted",
                      "response"=>$id 
                  );
                  $this->set_response($response, REST_Controller::HTTP_OK);
               }else{
                   $response = array(
                       "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 
                       "error"=>"Internal Server Error"
                   );
                   $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
               };
               //echo $idlab;
               
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               
           }


}else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"No Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
}


}



    public function fasyankes_put($idfaskes){

        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);

        
/*
        $insertArray = array(
            "kdfasyankes"=>$this->put('kdfasyankes'),
            "idpropinsi"=>$this->put('idpropinsi'),
            "idkabupaten"=>$this->put('idkabupaten'),
            "idkecamatan"=>$this->put('idkecamatan'),
            "idkelurahan"=>$this->put('idkelurahan'),
            "nama_fasyankes"=>$this->put('nama_fasyankes'),
            "alamat_fasyankes"=>$this->put('alamat_fasyankes'),
            "kode_pos"=>$this->put('kode_pos'),
            "fax"=>$this->put('fax'),
            "telp_fasyankes"=>$this->put('telp_fasyankes'),
            "email"=>$this->put('email'),
            "website"=>$this->put('website'),
            "jenis_fasyankes"=>$this->put('jenis_fasyankes'),
            "status_kepemilikan"=>$this->put('status_kepemilikan'),
            "longitude"=>$this->put('longitude'),
            "latitude"=>$this->put('latitude'),
            "status"=>$this->put('status'),
            "jam_buka"=>$this->put('jam_buka')
        );
  */
  $insertArray = $this->put();
  unset($insertArray['foto']);
$String64 = $this->put('foto');
     


        $path = "./assets/uploaded/fasyankes/";
        $thumbnail = "./assets/uploaded/fasyankes/thumbnail/";
        $foto = date("Ymd")."_".$insertArray['kdfasyankes'].".jpg";
        $nsrc = $path.$foto;
        $tsrc = $thumbnail.$foto;
        $this->images->base64tojpg($nsrc,$String64,$tsrc,512,512);
        $this->images->base64tojpg($nsrc,$String64,$nsrc,1125,450);
        $imgSrc = base_url()."assets/uploaded/fasyankes/".$foto;
        $tmbSrc = base_url()."assets/uploaded/fasyankes/thumbnail/".$foto;

        $insertArray['foto'] = $imgSrc;
        $insertArray['thumbnails']=$tmbSrc;

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               if ($decodedToken != false) {
                   if($id = $this->M_faskes->updateFaskes($insertArray,$idfaskes)){
                      $response = array(
                          "status"=>REST_Controller::HTTP_OK,
                          "error"=>null, 
                          "message"=>"Accepted",
                          "response"=>$id 
                      );
                      $this->set_response($response, REST_Controller::HTTP_OK);
                   }else{
                       $response = array(
                           "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 
                           "error"=>"Internal Server Error"
                       );
                       $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                   };
                   //echo $idlab;
                   
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   
               }


    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }


    }
    public function fasyankes_delete($idfaskes){
       
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               if ($decodedToken != false) {
                   if($id = $this->M_faskes->hapusFaskes($idfaskes)){
                     $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$id);  
                    $respon = array("success"=>$id);
                    $this->set_response($response, REST_Controller::HTTP_OK);
                   
                    }else{
                       $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
                       $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                   };
                   //echo $idlab;
            
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   
               }


    }else{

        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }


    }


    public function listall_get($page="1",$size="50")
    {
    
       
        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

			//return $decodedToken;
    if ($decodedToken != false) {
    $xp = explode("_",$decodedToken);
if($xp[3]==$headers['Xkey']){

if($xp[4]>='1'){

    if($result=$this->M_faskes->listAllFasyankes($page,$size)){
        $total = $this->M_faskes->countAll();
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result,"total"=>$total);
        $this->set_response($response,REST_Controller::HTTP_OK);  
        
        }else{
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"No Data");
        $this->set_response($response,REST_Controller::HTTP_OK);
           }
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Permission",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       
           
       
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
    return;	
        }
   



}