<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
Class Screening extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_screening');
    }

    private function notifikasi($rdata,$playerid){
        $content = array("en"=>$rdata,"id"=>$rdata);
        $data = array(
            "app_id"=>$this->config->item("OS_API_ID"), 
   
               "include_player_ids"=>array($playerid),
   
               /* "included_segments" => array(
                   'All'
               ),*/
               "data"=>array(),
               "contents"=>$content
           );
           $jsondata = json_encode($data);
           $ch = curl_init();
           curl_setopt($ch, CURLOPT_URL, $this->config->item('OS_API_URL'));
           curl_setopt($ch, CURLOPT_HTTPHEADER, array(
               'Content-Type: application/json; charset=utf-8', $this->config->item('OS_API_KEY')
             
           ));
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
           curl_setopt($ch, CURLOPT_HEADER, FALSE);
           curl_setopt($ch, CURLOPT_POST, TRUE);
           curl_setopt($ch, CURLOPT_POSTFIELDS, $jsondata);
           curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
           
           $response = curl_exec($ch);
           curl_close($ch);
           return $response;
    }

    public function index_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        }

    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_screening->insert($rdata)){
                   if($id>0){
                       $hasil = $this->M_screening->getHasil($id);
                   
                    }
                    /*
                   $text = ucwords($hasil->nama_pengguna)."  telah melakukan skrining TBC dengan hasil ".$hasil->text_hasil;
                   if($getPlayerId = $this->M_screening->getPlayerId($hasil->idfaskes)){

                   foreach($getPlayerId as $ply){
                    $rdata = array(
                        "notifikasi_username"=>$ply->username,
                        "notifikasi_playerid"=>$ply->playerid,
                        "notifikasi_text"=>$text
                    );   
                    if($this->M_screening->insertNotifikasi($rdata)){
                        $this->notifikasi($text,$ply->playerid);
                    }
                   }

                }*/

       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "message"=>"Accepted",
           "response"=>array('hasil'=>$hasil->kode_hasil,'screening_id'=>$id)               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "message"=>"Not Accepted",
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }


    }


    public function index_put($username){
        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);
    
        $rdata = $this->put();
    

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_screening->update($rdata,$username)){
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "message"=>"Accepted",
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "message"=>"Not Accepted",
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }


    }
    

    public function getlast_get($username){

        $hasil = $this->M_screening->getLastScreening($username);
        if($hasil){
        $resp = $this->M_screening->getHasilLast($username,$hasil->sc_date);
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null,
            "message"=>null,
            "response"=>$resp           
        );
    }else{

        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null,
            "message"=>null,
            "response"=>null            
        );

    }
        $this->set_response($response, REST_Controller::HTTP_OK);

    }



    public function hasilfaskes_get($kodefaskes){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_screening->getHasilPerfaskes($kodefaskes)){
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }


    }



    public function hasilkab_get($kab){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_screening->getHasilPerKab($kab)){
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }


    }




    public function hasilprop_get($prop){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_screening->getHasilPerProp($prop)){
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }


    }








    public function index_get($username){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_screening->getListScreening($username)){
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }


    }





    public function filter_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        }
  
        


        
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($result=$this->M_screening->getFilterScreening($rdata)){

       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "message"=>"Accepted",
           "response"=>$result               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "message"=>"no data",
           "response"=>$result               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }


    }






    public function reportfaskes_get($kodefaskes){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_screening->reportHasilPerfaskes($kodefaskes)){
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }


    }




    public function reportkabupaten_get($kab){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_screening->reportHasilPerKab($kab)){
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }


    }



    public function reportpropinsi_get($prop){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_screening->reportHasilPerProp($prop)){
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }


    }




    public function hasilfaskes_post($kodefaskes){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        }


        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {

                if(isset($rdata['search'])){
                    $id=$this->M_screening->searchHasilPerfaskes($kodefaskes,$rdata['search']);

                }else{

                    $id=$this->M_screening->newHasilPerfaskes($kodefaskes,$rdata['page'],$rdata['size']);
                }
                   if($id){
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }


    }



    public function indexlist_post($username){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        }


        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if(isset($rdata['search'])){
                    $id=$this->M_screening->searchListScreening($username,$rdata['search'],$rdata['page'],$rdata['size']);

                   }else{
                    $id=$this->M_screening->newListScreening($username,$rdata['page'],$rdata['size']);

                   }
                   if($id){
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }


    }





}