<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Ikonscreening extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("M_ikonscreening");
    }

    private function notifikasi($idkontak,$playerids){
        $idfaskes = $this->M_ikonscreening->getKontakDetail($idkontak);
        $hasil = $this->M_ikonscreening->getHasilScreening($idkontak);
        $content = array(
            "en"=>"Hasil IK atas nama ".strtoupper($idfaskes->ikon_dd_nama)."  : ".$hasil->hasil_screening,
            "id"=>"Hasil IK atas nama ".strtoupper($idfaskes->ikon_dd_nama)."  : ".$hasil->hasil_screening);
        //$id = $this->input->post('idcontent');

        $data = array(
            "app_id"=>$this->config->item('OS_API_ID'),
         "include_player_ids"=>$playerids,
            "data"=>array("idcontent"=>"info"),
            "contents"=>$content
        );

        $jsondata = json_encode($data);
        //print_r($jsondata);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic NDgyODNjZTItOTk2Zi00ZjllLTg5NDAtNjljM2E2ZWY5MWU3'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsondata);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
        curl_exec($ch);
        curl_close($ch);
        
   // print_r($response);

    }


public function indekscase_post(){

    $data = file_get_contents("php://input");
    $row = json_decode($data,true);

    if(!$this->input->post()){
        $rdata =$row;
    }else{

        $rdata = $this->input->post();

    }

   
    foreach($rdata as $key=>$value){
        //echo $key." = ".$value."<br>";
    if(preg_match("/^indeks_/",$key)){
        $dataindeks[$key] = $value;
    }elseif(preg_match("/^bridge_/",$key)){
        $databridging[$key] = $value;
    }
}
//print_r($databridging);
if(isset($dataindeks['indeks_person_id'])){
    $person_id = $dataindeks['indeks_person_id'];
    if($person_id!='0'){
    $Qdata = array(
        "indeks_tb03register"=>$dataindeks['indeks_tb03register'],
        "indeks_person_id"=>$person_id
    );
}else{
    unset($dataindeks['indeks_person_id']);
    $Qdata = array(
        "indeks_tb03register"=>$dataindeks['indeks_tb03register']
); 
}
}else{
    $Qdata = array(
        "indeks_tb03register"=>$dataindeks['indeks_tb03register']
);
}


    $idindeks = $this->M_ikonscreening->checkindeks($Qdata);



    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

           //return $decodedToken;
   if ($decodedToken != false) {
   $xp = explode("_",$decodedToken);
if($xp[3]==$headers['Xkey']){


if($idindeks){
$indeks_id = $idindeks->indeks_id;   
}else{
$indeks_id = $this->M_ikonscreening->insertIndeks($dataindeks);

}


  
if($indeks_id!=0){
    
    $databridging['bridge_indeks_id'] = $indeks_id;
    
    $id = array("indeks_id"=>$indeks_id);

    $bcheck = $this->M_ikonscreening->checkBridge($databridging);
    if($bcheck!=0){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$id);
    $this->set_response($response,REST_Controller::HTTP_OK);
    }else{

        if($this->M_ikonscreening->insertBrigde($databridging)){
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$id);
            $this->set_response($response,REST_Controller::HTTP_OK);
            }else{
                $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error","message"=>"USername tidak ditemukan");
                $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }
            


    }
   
  


    
    }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error","message"=>"Data dasar gagal disimpan");
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);

    }

 


   

}else{
$response = array(
    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
    "error"=>"Invalid Token Authorization",
);
$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }

   }else{

    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"No Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
   return;	  

}



    public function index_post(){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);

        if(!$this->input->post()){
            $rdata =$row;
        }else{

            $rdata = $this->input->post();

        }


        
        foreach($rdata as $key=>$value){
            if(preg_match("/^ikon_dd_/",$key)){
                $datadasar[$key] = $value;
            }elseif(preg_match("/^input_/",$key)){
                $datadasar[$key] = $value;
            }else{
                $datascreening[$key] = $value;
            }
        }

        if(!isset($datadasar['ikon_dd_puskesmas'])){
            $row = $this->M_ikonscreening->getPuskesmasIndeks($datadasar['ikon_dd_indeks_id']);

            $datadasar['ikon_dd_puskesmas'] = $row->indeks_puskesmas;

        }

       


        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 

        if($datadasar['ikon_dd_umur']<5){
            $datadasar['ikon_dd_screening'] = "balita";
        }elseif($datadasar['ikon_dd_umur']>=5 && $datadasar['ikon_dd_umur']<15){
            $datadasar['ikon_dd_screening'] = "anak";
        }else{
            $datadasar['ikon_dd_screening'] = "dewasa";
        }


        if($result = $this->M_ikonscreening->insertDatadasar($datadasar)){


          
    
            if($datadasar['ikon_dd_umur']<5){
                $datascreening['ikb_dd_id']=$result;
                if($this->M_ikonscreening->insertScreeningBalita($datascreening)){
                $resp = $this->M_ikonscreening->getHasilScreening($result);

            $rsplit = explode(" - ",$resp->hasil_screening);
             
            $hasil =  $resp->hasil_screening;
                 
 
                 switch($rsplit[0]){
                     case "Bukan Terduga TBC":
                         $warna = "#66bb6a";
                         $instruction = "Tetap Menjaga kesehatan dengan mengkonsumsi makanan gizi seimbang dan melakukan aktifitas fisik serta menerapkan etika batuk.";
                         break;
                         case "Terduga TBC":
                             $warna = "#ef5350";
                             $instruction = "Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                             break;
                             case "Terduga TBC RO":
                                 $warna="#d32f2f";
                                 $instruction = "Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                                 break;
                                 case "Menjalani Pengobatan":
                                     $warna="#ffa726";
                                 $instruction = "Pastikan anda tetap melakukan pengobatan rutin sampai sembuh dan menjaga kesehatan dengan mengkonsumsi makanan gizi seimbang dan melakukan aktifitas fisik serta menerapkan etika batuk. ";
                                     break;
                                     default:
                                     $warna="#d32f2f";
                                     $instruction="Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                 }
 
                 
 
                 $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted",
                 "response"=>array("hasil"=>$hasil,"warna"=>$warna,"instruksi"=>$instruction,"id"=>$result));
                $this->set_response($response,REST_Controller::HTTP_OK);
                
                $playerids = $this->M_ikonscreening->getPlayerId($datadasar['ikon_dd_puskesmas']);
                $idkontak = $this->M_ikonscreening->getKontakDetail($result);
                if($resp->hasil_screening!='Terpapar TBC'){
                   if(isset($playerids)){
                       foreach($playerids as $playid){
                       $players[] = $playid->playerid;

                    // print_r($resp);

                       $dataNotif = array(
                        "notifikasi_playerid"=>$playid->playerid,
                        "notifikasi_username"=>$playid->username,
                        "notifikasi_text"=>"Hasil IK atas nama ".strtoupper($idkontak->ikon_dd_nama)."  : ".$resp->hasil_screening);
        
                        $this->M_ikonscreening->insertNotifikasi($dataNotif);


                       }
                   }
                   if(isset($players)){
                    $this->notifikasi($result,$players);
                    }
               }


                }else{
                    $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error","message"=>"Data Screening Gagal di simpan");
                    $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                }
            }elseif($datadasar['ikon_dd_umur']>=5 && $datadasar['ikon_dd_umur']<15){
                $datascreening['ika_dd_id']=$result;
                 if($this->M_ikonscreening->insertScreeningAnak($datascreening)){
                $resp = $this->M_ikonscreening->getHasilScreening($result);
                $rsplit = explode(" - ",$resp->hasil_screening);
            
                $hasil =  $resp->hasil_screening;
  
                  switch($rsplit[0]){
                      case "Bukan Terduga TBC":
                          $warna = "#66bb6a";
                          $instruction = "Tetap Menjaga kesehatan dengan mengkonsumsi makanan gizi seimbang dan melakukan aktifitas fisik serta menerapkan etika batuk.";
                          break;
                          case "Terduga TBC":
                              $warna = "#ef5350";
                              $instruction = "Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                              break;
                              case "Terduga TBC RO":
                                  $warna="#d32f2f";
                                  $instruction = "Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                                  break;
                                  case "Menjalani Pengobatan":
                                      $warna="#ffa726";
                                  $instruction = "Pastikan anda tetap melakukan pengobatan rutin sampai sembuh dan menjaga kesehatan dengan mengkonsumsi makanan gizi seimbang dan melakukan aktifitas fisik serta menerapkan etika batuk. ";
                                      break;
                                      default:
                                      $warna="#d32f2f";
                                      $instruction="Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                  }
  
                  
  
                  $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted",
                  "response"=>array("hasil"=>$hasil,"warna"=>$warna,"instruksi"=>$instruction,"id"=>$result));
                 $this->set_response($response,REST_Controller::HTTP_OK); 

                 $playerids = $this->M_ikonscreening->getPlayerId($datadasar['ikon_dd_puskesmas']);
                 $idkontak = $this->M_ikonscreening->getKontakDetail($result);
                 if($resp->hasil_screening!='Terpapar TBC'){
                    if(isset($playerids)){
                        foreach($playerids as $playid){
                        $players[] = $playid->playerid;
 
                     // print_r($resp);
 
                        $dataNotif = array(
                         "notifikasi_playerid"=>$playid->playerid,
                         "notifikasi_username"=>$playid->username,
                         "notifikasi_text"=>"Hasil IK atas nama ".strtoupper($idkontak->ikon_dd_nama)."  : ".$resp->hasil_screening);
         
                         $this->M_ikonscreening->insertNotifikasi($dataNotif);
 
 
                        }
                    }
                    if(isset($players)){
                        $this->notifikasi($result,$players);
                        }
                }


                 }else{
                    $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error","message"=>"Data Screening Gagal di simpan");
                    $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                }


            }else{
                $datascreening['ikd_dd_id']=$result;
                if($this->M_ikonscreening->insertScreeningDewasa($datascreening)){
                $resp = $this->M_ikonscreening->getHasilScreening($result);
                $hiv = $this->M_ikonscreening->getHasilScreeningHiv($result);
              
                $rsplit = explode(" - ",$resp->hasil_screening);
                if($resp->ikon_dd_propinsi=='31'){
                  
 
                  if(count($rsplit)>1){
                     $hasil =  $rsplit[0]." - ".$rsplit[1];
                     //$kriteria = $rsplit[1];
                     }else{
                         $hasil =  $rsplit[0]." - ".$hiv;
                        // $kriteria = null;
                     }
 
                  }else{
                        $hasil =  $resp->hasil_screening;
                        
                  }
  
                  
  
                  switch($rsplit[0]){
                      case "Bukan Terduga TBC":
                          $warna = "#66bb6a";
                          $instruction = "Tetap Menjaga kesehatan dengan mengkonsumsi makanan gizi seimbang dan melakukan aktifitas fisik serta menerapkan etika batuk.";
                          break;
                          case "Terduga TBC":
                              $warna = "#ef5350";
                              $instruction = "Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                              break;
                              case "Terduga TBC RO":
                                  $warna="#d32f2f";
                                  $instruction = "Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                                  break;
                                  case "Menjalani Pengobatan":
                                      $warna="#ffa726";
                                  $instruction = "Pastikan anda tetap melakukan pengobatan rutin sampai sembuh dan menjaga kesehatan dengan mengkonsumsi makanan gizi seimbang dan melakukan aktifitas fisik serta menerapkan etika batuk. ";
                                      break;
                                      default:
                                      $warna="#d32f2f";
                                      $instruction="Anda perlu melakukan pemeriksaan lebih lanjut ke fasyankes untuk mengetahui apakah anda sakit TBC atau tidak. Hubungi :";
                  }
  
                  
  //print_r($players);
  
                  $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted",
                  "response"=>array("hasil"=>$hasil,"warna"=>$warna,"instruksi"=>$instruction,"id"=>$result));
                 $this->set_response($response,REST_Controller::HTTP_OK);



                 $playerids = $this->M_ikonscreening->getPlayerId($datadasar['ikon_dd_puskesmas']);
                 $idkontak = $this->M_ikonscreening->getKontakDetail($result);
                // echo $datadasar['ikon_dd_puskesmas'];
                 //print_r($playerids);
                 if($resp->hasil_screening!='Terpapar TBC'){
                    if(isset($playerids)){
                        foreach($playerids as $playid){
                        $players[] = $playid->playerid;
 
                     // print_r($resp);
 
                        $dataNotif = array(
                         "notifikasi_playerid"=>$playid->playerid,
                         "notifikasi_username"=>$playid->username,
                         "notifikasi_text"=>"Hasil IK atas nama ".strtoupper($idkontak->ikon_dd_nama)."  : ".$resp->hasil_screening);
         
                         $this->M_ikonscreening->insertNotifikasi($dataNotif);
 
 
                        }
                    }
                    if(isset($players)){
                    $this->notifikasi($result,$players);
                    }
                }


                

                
                }else{
                    $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error","message"=>"Data Screening Gagal di simpan");
                    $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                }
            }

        
          
        }else{
            $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error","message"=>"Data dasar gagal disimpan");
            $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);

        }
    
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }






    public function indekslist_get($username){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   $resp = $this->M_ikonscreening->getListIndexByUser($username);
   if($resp){
   $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data found","response"=>$resp);
   $this->set_response($response,REST_Controller::HTTP_OK);  
   }else{
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data not found","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK);  
   }

       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }




    public function indeksbykader_get($kader){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   $resp = $this->M_ikonscreening->getListIndexByKader($kader);
   if($resp){
   $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data found","response"=>$resp);
   $this->set_response($response,REST_Controller::HTTP_OK);  
   }else{
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data not found","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK);  
   }

       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }



    public function indeksbyfaskes_get($faskes){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   $resp = $this->M_ikonscreening->getListIndexByFaskes($faskes);
   if($resp){
   $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data found","response"=>$resp);
   $this->set_response($response,REST_Controller::HTTP_OK);  
   }else{
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data not found","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK);  
   }

       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }







    public function indekslist_post(){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        }


        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   $resp = $this->M_ikonscreening->getListIndex($rdata);
   if($resp){
   $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data found","response"=>$resp);
   $this->set_response($response,REST_Controller::HTTP_OK);  
   }else{
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data not found","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK);  
   }

       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }




    public function ikonlist_get($indeks){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   $resp = $this->M_ikonscreening->getListIkonByIndeks($indeks);
   if($resp){
   $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data found","response"=>$resp);
   $this->set_response($response,REST_Controller::HTTP_OK);  
   }else{
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data not found","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK);  
   }

       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }



    public function ikonlistuser_get($indeks,$user){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   $resp = $this->M_ikonscreening->getListIkonByIndeksByUser($indeks,$user);
   if($resp){
   $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data found","response"=>$resp);
   $this->set_response($response,REST_Controller::HTTP_OK);  
   }else{
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data not found","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK);  
   }

       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }


    public function index_put($idscreening){
        $data = file_get_contents("php://input","PUT");
        $rdata = $this->put();
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_ikonscreening->update($rdata,$idscreening)){
                   
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null,
                    "message"=>"success",
                    "response"=>$id);    
        $this->set_response($response, REST_Controller::HTTP_OK);  

      
  
  
  
        }else{
       $response = array(
           "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
           "error"=>null,
           "message"=>"failed",
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }



    public function feedback_put($idscreening,$kodefaskes){
        //$data = file_get_contents("php://input","PUT");
        $rdata['status_rtl'] = "2";
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               $check = $this->M_ikonscreening->checkFaskes($idscreening,$kodefaskes);
               if($check>0){

               if($id=$this->M_ikonscreening->update($rdata,$idscreening)){
                   
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null,
                    "message"=>"success",
                    "response"=>$id);    
        $this->set_response($response, REST_Controller::HTTP_OK);  

      
  
  
  
        }else{
       $response = array(
           "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
           "error"=>null,
           "message"=>"failed",
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }

}else{
    $response = array(
        "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
        "error"=>"Kode Faskes Tidak sesuai",
        "response"=>null);    
$this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);  

}



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }




    public function indeks_put($indeks_id){
        $data = file_get_contents("php://input","PUT");
        $rdata = $this->put();
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_ikonscreening->updateIndeks($rdata,$indeks_id)){
                   
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null,
                    "message"=>"success",
                    "response"=>$id);    
        $this->set_response($response, REST_Controller::HTTP_OK);  

      
  
  
  
        }else{
       $response = array(
           "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
           "error"=>null,
           "message"=>"failed",
           "response"=>null              
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }





    public function indeks_get($indeks_id){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   $resp = $this->M_ikonscreening->getDetailIndeks($indeks_id);
   if($resp){
   $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data found","response"=>$resp);
   $this->set_response($response,REST_Controller::HTTP_OK);  
   }else{
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data not found","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK);  
   }

       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }



    public function indeks_delete($indeks_id){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   $resp = $this->M_ikonscreening->deleteIndeks($indeks_id);
   if($resp){
   $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data berhasil dihapus","response"=>$resp);
   $this->set_response($response,REST_Controller::HTTP_OK);  
   }else{
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data not found","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK);  
   }

       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }








    public function datadasar_put($ikon_dd_id){
        $data = file_get_contents("php://input","PUT");
        $rdata = $this->put();
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_ikonscreening->updateDataDasar($rdata,$ikon_dd_id)){
                   
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null,
                    "message"=>"success",
                    "response"=>$id);    
        $this->set_response($response, REST_Controller::HTTP_OK);  

      
  
  
  
        }else{
       $response = array(
           "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
           "error"=>null,
           "message"=>"failed",
           "response"=>null              
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }





    public function datadasar_get($ikon_dd_id){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   $resp = $this->M_ikonscreening->getDetailDatadasar($ikon_dd_id);
   if($resp){
   $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data found","response"=>$resp);
   $this->set_response($response,REST_Controller::HTTP_OK);  
   }else{
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data not found","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK);  
   }

       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }


    public function datadasar_delete($ikon_dd_id){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   $resp = $this->M_ikonscreening->deleteDatadasar($ikon_dd_id);
   if($resp){
   $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data berhasil dihapus","response"=>$resp);
   $this->set_response($response,REST_Controller::HTTP_OK);  
   }else{
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data not found","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK);  
   }

       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }

    public function searchindeks_post(){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
    
        if(!$this->input->post()){
            $rdata =$row['search'];
        }else{
    
            $rdata = $this->input->post('search');
    
        }



        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   $resp = $this->M_ikonscreening->searchIndeks($rdata);
   if($resp){
   $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data berhasil dihapus","response"=>$resp);
   $this->set_response($response,REST_Controller::HTTP_OK);  
   }else{
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data not found","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK);  
   }

       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  



    }









    public function indekslistbyuser_post($username){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
    
        if($this->input->post()){
            $rdata =$this->input->post();
        }else{
    
            $rdata = $row;
    
        }

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){

    if(isset($rdata['search'])){
        $resp = $this->M_ikonscreening->searchListIndexByUser($username,$rdata);
            }else{
                $resp = $this->M_ikonscreening->newListIndexByUser($username,$rdata['page'],$rdata['size']);
            }

   //$resp = $this->M_ikonscreening->searchListIndexByUser($username,$rdata);
   if($resp){
   $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data found","response"=>$resp);
   $this->set_response($response,REST_Controller::HTTP_OK);  
   }else{
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data not found","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK);  
   }

       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }




    public function indeksbykader_post($kader){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
    
        if($this->input->post()){
            $rdata =$this->input->post();
        }else{
    
            $rdata = $row;
    
        }

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){

    if(isset($rdata['search'])){
        $resp = $this->M_ikonscreening->searchListIndexByKader($kader,$rdata);
            }else{
                $resp = $this->M_ikonscreening->newListIndexByKader($kader,$rdata['page'],$rdata['size']);
            }
  
   if($resp){
   $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data found","response"=>$resp);
   $this->set_response($response,REST_Controller::HTTP_OK);  
   }else{
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data not found","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK);  
   }

       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }



    public function indeksbyfaskes_post($faskes){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
    
        if($this->input->post()){
            $rdata =$this->input->post();
        }else{
    
            $rdata = $row;
    
        }
        //print_r($rdata);

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
       if(isset($rdata['search'])){
   $resp = $this->M_ikonscreening->searchListIndexByFaskes($faskes,$rdata);
       }else{
           $resp = $this->M_ikonscreening->newListIndexByFaskes($faskes,$rdata['page'],$rdata['size']);
       }
   if($resp){
   $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data found","response"=>$resp);
   $this->set_response($response,REST_Controller::HTTP_OK);  
   }else{
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data not found","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK);  
   }

       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }


    public function ikonlist_post($indeks){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
    
        if($this->input->post()){
            $rdata =$this->input->post();
        }else{
    
            $rdata = $row;
    
        }


        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
    if(isset($rdata['search'])){
   $resp = $this->M_ikonscreening->searchListIkonByIndeks($indeks,$rdata);
    }else{
        $resp = $this->M_ikonscreening->newListIkonByIndeks($indeks,$rdata['page'],$rdata['size']);

    }
   if($resp){
   $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data found","response"=>$resp);
   $this->set_response($response,REST_Controller::HTTP_OK);  
   }else{
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"data not found","response"=>$resp);
    $this->set_response($response,REST_Controller::HTTP_OK);  
   }

       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    
    }


}