<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Login extends REST_controller{ 
    private $urlReset;
public function __construct(){
    parent::__construct();
    $this->load->model('M_login');
    $this->load->model('M_faskes');
    $this->load->model("sys/M_greeting");
    $this->load->library("images");
    $this->urlReset = "https://akun.sobattb.id/sys/reset";
    $this->urlActivated = "https://akun.sobattb.id/sys/activated";


   

}


public function auth_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
    if(!$this->input->post()){
        $username = $row['username'];
        $password = md5($row['userpassword']);
        //$playerid = $row['playerid'];
    }else{
        $username = $this->input->post("username");
        $password = md5($this->input->post("userpassword"));
        //$playerid = $this->input->post('playerid');
    }

    $authUser = array(
        "username"=>$username,
        "userpassword"=>$password
    );

    $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

			//return $decodedToken;
            if ($decodedToken != false) {
    $xp = explode("_",$decodedToken);
if($xp[3]==$headers['Xkey']){
    if($id=$this->M_login->getAuthUser($authUser)){
        if($id->usergroup=='3'){
            $id->kdfasyankes = $this->M_login->getFaskes($username);
        }elseif($id->usergroup=='10'){
            $id->kader_id = $this->M_login->getKaderId($username);
        }
        $ret = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null,
            "message"=>"Accepted",
            "response"=>$id
            
        );   
    }else{
$ret= array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
"error"=>"Internal Server Error");
    }
    $this->set_response($ret, REST_Controller::HTTP_OK);
    

			}else{

		$response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "Invalid Key";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            }
            
        }else{
        $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "Invalid Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
        }

	}else{
        $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
	}
	return;	

}


public function token_get(){
    $headers=$this->input->request_headers();

   
    if(array_key_exists('Xkey',$headers) && !empty($headers['Xkey'])){
    
    if($dataq = $this->M_login->tokenGet($headers['Xkey'])){
       $src_token = $dataq->token;
               $token['status'] = REST_Controller::HTTP_OK;
               $token['error'] = null;
               $token['message']= "Accepted";
               $token['response']['token'] = AUTHORIZATION::generateToken($src_token);
               $this->set_response($token,REST_Controller::HTTP_OK);
    }else{
           
           $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $token['error'] = "Invalid Key";
           //$st['token']="";
           $this->set_response($token, REST_Controller::HTTP_UNAUTHORIZED);
    }
    }
}


public function coba_get(){
    $headers=$this->input->request_headers();
    if(array_key_exists('Authorization',$headers) && !empty($headers['Authorization'])){
    $this->set_response($headers['Authorization'],REST_Controller::HTTP_OK);
    }else{
        $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $token['error'] = "Invalid Key";
        //$st['token']="";
        $this->set_response($token, REST_Controller::HTTP_UNAUTHORIZED);
    }
}



public function sentMail($rdata){
    //print_r($rdata);
    $this->mail = $this->phpmailer_load->load(); 
    $this->mail->isSMTP(); 
    $this->mail->Host = "smtp.office365.com"; 
    //$this->mail->SMTPDebug = 2;
    $this->mail->SMTPAuth = true;
    $this->mail->Username = "admin.apps@yki4tbc.org";
    $this->mail->Password = 'YKItbM7j/hZx}\8/k&';
    $this->mail->SMTPSecure = 'tls';
    $this->mail->Port = 587;
    $this->mail->setFrom('admin.apps@yki4tbc.org', 'SOBAT TB'); 
    $this->mail->Subject = "reset password SOBAT TB ";
    $this->mail->addAddress($rdata->email,$rdata->nama_pengguna); 
  
   

$urlReset = $this->urlReset."/".$rdata->uuid;
$body = "Yth, ".$rdata->nama_pengguna."\r\n";

$body .= "<br>\r\n";
$body .= "Anda Telah melakukan permintaan reset password melalui aplikasi SOBAT TB pada tanggal ".date("d/m/Y");
$body .= "<br>\r\n";
$body .= "<br>\r\n";
$body .= "Untuk dapat mereset password anda, silahkan klik link berikut ini\r\n";
$body .= "<br><a href=\"".$urlReset."\">".$urlReset."</a>\r\n";
$body .= "<br>";
$body .= "<br>\r\n";
$body .= "<br>\r\n";
$body .= "<br>Link tersebut hanya berlaku selama 1 jam";
$body .= "<br>\r\n";
$body .= "<br>\r\n";
$body .= "<br><br>Terima kasih";

$this->mail->msgHtml($body);
if($this->mail->send()){
    return true;
}else{
    return false;
    //echo $this->mail->ErrorInfo.'Gh!E&Fy}Y54F]8ppYC$F!8';
}

}








public function mailActivation($username){
    $akun = $this->M_login->getUserData($username);
    //print_r($rdata);
    $this->mail = $this->phpmailer_load->load(); 
    $this->mail->isSMTP(); 
    $this->mail->Host = "smtp.office365.com"; 
    //$this->mail->SMTPDebug = 2;
    $this->mail->SMTPAuth = true;
    $this->mail->Username = "admin.apps@yki4tbc.org";
    $this->mail->Password = 'YKItbM7j/hZx}\8/k&';
    $this->mail->SMTPSecure = 'tls';
    $this->mail->Port = 587;
    $this->mail->setFrom('admin.apps@yki4tbc.org', 'SOBAT TB'); 
    $this->mail->Subject = "Aktifasi Akun SOBATTB ";
    $this->mail->addAddress($akun->email,$akun->nama_pengguna); 
  
   

$url = $this->urlActivated."/".$akun->kode_aktifasi;
$body = "Yth, ".$akun->nama_pengguna."\r\n";

$body .= "<br>\r\n";
$body .= "Terima kasih, Anda Telah melakukan registrasi akun SOBAT TB pada tanggal ".date("d/m/Y");
$body .= "<br>\r\n";
$body .= "<br>\r\n";
$body .= "Untuk dapat mengaktifkan akun ini silahakn klik link dibawah ini \r\n";
$body .= "<br><a href=\"".$url."\">".$url."</a>\r\n";
$body .= "<br>";
$body .= "<br>\r\n";
$body .= "<br>\r\n";
$body .= "<br>\r\n";
$body .= "<br>\r\n";
$body .= "<br><br>Terima kasih";

$this->mail->msgHtml($body);
if($this->mail->send()){
    return true;
}else{
    return false;
    //echo $this->mail->ErrorInfo.'Gh!E&Fy}Y54F]8ppYC$F!8';
}

}




public function greetingMail($username){
    $akun = $this->M_login->getUserData($username);
    if($akun->nama_pengguna!=null){
    $nama = $akun->nama_pengguna;
    }else{
        $nama = $akun->email;
    }
    $html = $this->M_greeting->getTemplate("1");

    //print_r($rdata);
    $this->mail = $this->phpmailer_load->load(); 
    $this->mail->isSMTP(); 
    $this->mail->Host = "smtp.office365.com"; 
    //$this->mail->SMTPDebug = 2;
    $this->mail->SMTPAuth = true;
    $this->mail->Username = $html->username;
    $this->mail->Password = $html->pwd;
    $this->mail->SMTPSecure = 'tls';
    $this->mail->Port = 587;
    $this->mail->setFrom($html->username, $html->template_apps); 
    $this->mail->Subject = $html->template_subject;
    $this->mail->addAddress($akun->email,$akun->nama_pengguna); 
  

$body= str_replace("<<First Name>>","<b>".strtoupper($nama)."</b>",$html->template_text);
  

$this->mail->msgHtml($body);
if($this->mail->send()){
    return true;
}else{
   return $this->mail->ErrorInfo;//false;
    //echo $this->mail->ErrorInfo.'Gh!E&Fy}Y54F]8ppYC$F!8';
}

}


public function getpassword_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
    if(!$this->input->post()){
        $email = $row['email'];
    }else{
        $email = $this->input->post('email');
    }

$mail = array("email"=>$email);
    $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
                    
                $ret = $this->M_login->getPassword($email);
               // print_r($ret);

                if($ret!=false){
                    $sentMail = $this->sentMail($ret);

                if($sentMail==true){
                    $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null, 
                    "message"=>"Accepted",
                    "response"=>true 
                    );
                }else{

                    $response = array(
                        "status"=>REST_Controller::HTTP_OK,
                        "error"=>null, 
                        "message"=>"Email Tidak terkirim",
                        "response"=>false 
                        );

                }              
                
                	$this->set_response($response, REST_Controller::HTTP_OK);
            }else{
             

                $response = array(
                    "status"=>REST_Controller::HTTP_NOT_FOUND,
                     "error"=>"E-mail Doesn't Exists", 
                       );
                                 $this->set_response($response, REST_Controller::HTTP_NOT_FOUND);

            }

			}else{
		$token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "Invalid Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
	}
	return;	

}


private function checkEmail($email){
    return $this->M_login->email_checking($email);
}

private function checkUsername($username){
    return $this->M_login->username_checking($username);
}







public function register_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
    if(!$this->input->post()){
        $username = $row['username'];
        $email = $row['email'];
        $nohp = $row['nohp'];

        $data = array(
            "username" => $row['username'],
            "userpassword" => md5($row['userpassword']),
            "usergroup" => $row['usergroup'],
            "nama_pengguna" => $row['nama_pengguna'],
            "email" => $row['email'],
            "nohp" => $row['nohp'],
            "jenis_kelamin"=> $row['jenis_kelamin'],
            "tgl_lahir" => $row['tgl_lahir'],
            "idpropinsi"=>$row['idpropinsi'],
            "idkabupaten"=>$row['idkabupaten'],
            "idkecamatan"=>$row['idkecamatan'],
            "idkelurahan"=>$row['idkelurahan'],
            "idfaskes"=>$row['idfaskes'],
            "alamat_pengguna"=>$row['alamat_pengguna']
        );



    }else{
        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $nohp = $this->input->post('nohp');

        $data = array(
            "username" => $this->input->post('username'),
            "userpassword" => md5($this->input->post('userpassword')),
            "usergroup" => $this->input->post('usergroup'),
            "nama_pengguna" => $this->input->post('nama_pengguna'),
            "email" => $this->input->post('email'),
            "nohp" => $this->input->post('nohp'),
            "jenis_kelamin"=> $this->input->post('jenis_kelamin'),
            "tgl_lahir" => $this->input->post('tgl_lahir'),
            "idpropinsi"=>$this->input->post('idpropinsi'),
            "idkabupaten"=>$this->input->post('idkabupaten'),
            "idkecamatan"=>$this->input->post('idkecamatan'),
            "idkelurahan"=>$this->input->post('idkelurahan'),
            "idfaskes"=>$this->input->post('idfaskes'),
            "alamat_pengguna"=>$this->input->post('alamat_pengguna')
        );

    }

    $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
    
    
    

    if($id=$this->M_login->registerUser($data)){
        $response = array(
            "status"=>REST_Controller::HTTP_CREATED,
            "error"=>null, 
            "message"=>"Accepted"               
        );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
            "error"=>"Internal Server Error"               
        );
            $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
    }



	
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
	}
	return;	


}



public function emailValidation_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
    if(!$this->input->post()){
        $email = $row['email'];
    }else{
        $email=$this->input->post('email');
    }


   $ret=$this->M_login->email_validation($email);
   $this->set_response($ret, REST_Controller::HTTP_OK);
           
   return;
}

public function usernameValidation_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
    if(!$this->input->post()){
        $username = $row['username'];
    }else{
        $username=$this->input->post('username');
    }
   
   $ret=$this->M_login->username_validation($username);
   $response = array(
    "status"=>REST_Controller::HTTP_OK,
    "error"=>null, 
    "message"=>"Accepted",
    "response"=>$ret
);
   $this->set_response($response, REST_Controller::HTTP_OK);
   return;
}

public function nohpValidation_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
    if(!$this->input->post()){
        $nohp = $row['nohp'];
    }else{
        $nohp=$this->input->post('nohp');
    }

   $ret=$this->M_login->nohp_validation($nohp);
   $this->set_response($ret, REST_Controller::HTTP_OK);
   return;
}


public function faskesdomisili_get($kecamatan){

    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_faskes->ListByKecamatan($kecamatan)){
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "message"=>"Accepted",
           "response"=>$id              
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>"Internal Server Error" ,
           "response"=>false              
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
}










public function resend_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
    if($this->input->post()){
        $rdata = $this->input->post();
    }else{
        $rdata = $row;
    }

    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;

           if ($decodedToken != false) {
               $m = $this->M_login->updateOTP($rdata);
               if($id=$this->M_login->getOTP($rdata)){
                $text = rawurlencode("Kode verifikasi SOBAT-TB kamu adalah $id->otp
                jangan berikan kode OTP kepada siapapun. Salam TOSS TBC (Temukan Obati Sampai Sembuh) #SOBAT-TB");
                $url = "https://api.tcastsms.net/api/v2/SendSMS?ApiKey=fPr3INujSYekG58JWAVSAlTigJcrneBuWUxYBAw9St4=&ClientId=ff7466e6-fae4-429e-a4bf-f7a20d1b6f49&SenderId=SobatTB&Message=".$text."&MobileNumbers=".$nohp."&Is_Unicode=false&Is_Flash=false";
                // $url = "https://numberic1.tcastsms.net:20005/sendsms?account=mask_sobattb&password=H9e6abY7&sender=SobatTB&numbers=".$id->nohp."&content=".$text;
 $ch = curl_init();
curl_setopt( $ch, CURLOPT_URL, $url);
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
'Content-Type: application/json',
'Accept:application/json'
)
);

$response = curl_exec( $ch );

$httpCode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
$responseBody = json_decode( $response, true );
//return $responseBody;
curl_close($ch);




       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "message"=>"Valid",
           "response"=>$responseBody              
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "response"=>$id              
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }




}

public function otp_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
    if($this->input->post()){
        $rdata = $this->input->post();
    }else{
        $rdata = $row;
    }


    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_login->checkOTP($rdata)){
                   $this->M_login->aktifasi($rdata);
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "message"=>"Valid",
           "response"=>$id              
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_UNAUTHORIZED,
           "error"=>"Invalid OTP"               
       );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }



}

//self register for mobile user
public function selfregistration_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
    if(!$this->input->post()){
        $email = $row['email'];
        $username = $row['email'];
        $password = @$row['userpassword'];
        $nohp = $row['nohp'];
        $nama = $row['nama_pengguna'];
       // $otp_hash = $row['otp_hash'];
    }else{
        $email = $this->input->post('email');
        $username = $this->input->post('email');
        $password = @$this->input->post('userpassword');
        $nohp = $this->input->post('nohp');
        $nama = $this->input->post('nama_pengguna');
       // $otp_hash = $this->input->post('otp_hash');
    }

    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {

        $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
        $otp = mt_rand(100000,999999);
        //return $decodedToken;
        if ($decodedToken != false) {
            //if($password!=null){
            $data = array("username"=>$username,"email"=>$email,"userpassword"=>md5($password),"nohp"=>$nohp,"nama_pengguna"=>$nama,"active"=>"1");
            //}else{
             //   $data = array("username"=>$username,"email"=>$email) ;
            //}
           
            
            $d = $this->checkEmail($email);
            if($d=="0"){

            if($id=$this->M_login->selfRegisterUser($data)){
              


             //  $text = rawurlencode("<#> Kode OTP SobatTB Anda: $otp #SobatTB $otp_hash");
/*
               $url = "https://api.tcastsms.net/api/v2/SendSMS?ApiKey=fPr3INujSYekG58JWAVSAlTigJcrneBuWUxYBAw9St4=&ClientId=ff7466e6-fae4-429e-a4bf-f7a20d1b6f49&SenderId=SobatTB&Message=".$text."&MobileNumbers=".$nohp."&Is_Unicode=false&Is_Flash=false";
$ch = curl_init();
curl_setopt( $ch, CURLOPT_URL, $url);
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
'Content-Type: application/json',
'Accept:application/json'
)
);

$response = curl_exec( $ch );

$httpCode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
$responseBody = json_decode( $response, true );
return $responseBody;
curl_close($ch);



*/






               
                
                
                
               $statusmail = $this->greetingMail($email);
                
                $response = array(
                    "status"=>REST_Controller::HTTP_CREATED,
                    "error"=>null, 
                    "message"=>"Accepted",
                    "response"=>$id ,
                    "email"=>$statusmail             
                );
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
            }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
                    "error"=>"Internal Server Error"               
                );
                    $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }
       }else{
            $response = array(
                "status"=>REST_Controller::HTTP_BAD_REQUEST,
                "error"=>"E-mail Duplicate"
            );
            $this->set_response($response,REST_controller::HTTP_BAD_REQUEST);
        }
        

            


        }else{

            $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
            $response['error'] = "Invalid Token Authorization";
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);

        }
        


    }else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);

    }



}




public function webregistration_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
    if(!$this->input->post()){
        $rdata = $row;
      /*  $email = $row['email'];
        $username = $row['email'];
        $password = @$row['userpassword'];
        $nohp = $row['nohp'];
        */
       // $otp_hash = $row['otp_hash'];
    }else{
        $rdata = $this->input->post();
        /*
        $email = $this->input->post('email');
        $username = $this->input->post('email');
        $password = @$this->input->post('userpassword');
        $nohp = $this->input->post('nohp');
       */
        // $otp_hash = $this->input->post('otp_hash');
    }

    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {

        $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
        $otp = mt_rand(100000,999999);
        //return $decodedToken;
        if ($decodedToken != false) {
            //if($password!=null){
           // $data = array("username"=>$username,"email"=>$email,"userpassword"=>md5($password),"nohp"=>$nohp);
            //}else{
             //   $data = array("username"=>$username,"email"=>$email) ;
            //}
            
            $password = md5($rdata['userpassword']);
            unset($rdata['userpassword']);
            $rdata['userpassword'] = $password;
            $rdata['otp']= $otp;
            $rdata['kode_aktifasi'] = $this->M_login->getUUID()->uid;
            
            $d = $this->checkEmail($rdata['email']);
            if($d=="0"){

                if($this->checkUsername($rdata['username'])=='0'){ 

            if($id=$this->M_login->selfRegisterUser($rdata)){
                    $this->mailActivation($rdata['username']);
                    $this->greetingMail($rdata['username']);
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null, 
                    "message"=>"Accepted",
                    "response"=>$id               
                );
                    $this->set_response($response, REST_Controller::HTTP_OK);
            }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
                    "error"=>"Internal Server Error"               
                );
                    $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }

        }else{

            $response = array(
                "status"=>REST_Controller::HTTP_BAD_REQUEST,
                "error"=>"Username Sudah digunakan"
            );
            $this->set_response($response,REST_controller::HTTP_BAD_REQUEST);
        }





       }else{
            $response = array(
                "status"=>REST_Controller::HTTP_BAD_REQUEST,
                "error"=>"E-mail Duplicate"
            );
            $this->set_response($response,REST_controller::HTTP_BAD_REQUEST);
        }
        

            


        }else{

            $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
            $response['error'] = "Invalid Token Authorization";
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);

        }
        


    }else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);

    }



}



public function pid_put(){
    $data = file_get_contents("php://input","PUT");
    $row = json_decode($data,true);
    $email = $this->put('email');
    $playerid = $this->put('playerid');
    $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
                if($id=$this->M_login->updatePlayerid(array('playerid'=>$playerid),$email)){
        $response = array(
            "status"=>REST_Controller::HTTP_CREATED,
            "error"=>null, 
            "message"=>"Accepted",
            "response"=>$id              
        );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null,
            "message"=>"no data"              
        );
            $this->set_response($response, REST_Controller::HTTP_OK);
    }



	
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }

}

public function remove_put(){
    $data = file_get_contents("php://input","PUT");
    $row = json_decode($data,true);
    $email = $this->put('email');
    $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
                if($id=$this->M_login->updatePlayerid(array('playerid'=>null),$email)){
        $response = array(
            "status"=>REST_Controller::HTTP_CREATED,
            "error"=>null, 
            "message"=>"Accepted",
            "response"=>$id               
        );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null,
            "message"=>"no data"              
        );
            $this->set_response($response, REST_Controller::HTTP_OK);
    }



	
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }

}

public function fotoprofile_put(){
    $data = file_get_contents("php://input","PUT");
    $row = json_decode($data,true);
    $uid = $this->M_login->getUUID();

    $cf = "./assets/uploaded/profiles/";
    $cf .= $uid->uid."_".date("Ymd")."_".str_replace(" ","_",$this->put("email")).".jpg";
    $fpath = "./assets/uploaded/profiles/";
    $filename = $uid->uid."_".date("Ymd")."_".str_replace("@","_at_",$this->put("email")).".jpg";

    $this->images->base64tojpg($cf,$this->put("foto"),$fpath.$filename,'512','512');
 
    $email = $this->put('email');
    $nohp = $this->put('nohp');
    $foto = base_url()."assets/uploaded/profiles/".$filename;
    $data = array("foto"=>$foto);
    unlink($cf);
    $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
                if($id=$this->M_login->updateProfile($data,$email)){
        $response = array(
            "status"=>REST_Controller::HTTP_CREATED,
            "error"=>null, 
            "message"=>"Accepted",
            "response"=>$this->M_login->detailProfile($email)               
        );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
            "error"=>"Internal Server Error"               
        );
            $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
    }



	
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
    


}



public function updateprofile_put(){
    $data = file_get_contents("php://input","PUT");
    $row = json_decode($data,true);
    
    $email = $this->put('email');
   // $nohp = $this->put('nohp');
    
    $data = $this->put();
/*if($this->put('username')!=''){
        $data = array(
            "username" => $this->put('username'),
            "nama_pengguna" => $this->put('nama_pengguna'),
            //"nohp" => $this->put('nohp'),
            "jenis_kelamin"=> $this->put('jenis_kelamin'),
            "tgl_lahir" => $this->put('tgl_lahir'),
            "idpropinsi"=>$this->put('idpropinsi'),
            "idkabupaten"=>$this->put('idkabupaten'),
            "idkecamatan"=>$this->put('idkecamatan'),
            "idkelurahan"=>$this->put('idkelurahan'),
            "idfaskes"=>$this->put('idfaskes'),
            "alamat_pengguna"=>$this->put('alamat_pengguna'),
            "twitter"=>$this->put('twitter'),
            "instagram"=>$this->put('instagram'),
            "playerid"=>$this->put('playerid')
        );
    }else{
        $data = array(
            "nama_pengguna" => $this->put('nama_pengguna'),
           // "nohp" => $this->put('nohp'),
            "jenis_kelamin"=> $this->put('jenis_kelamin'),
            "tgl_lahir" => $this->put('tgl_lahir'),
            "idpropinsi"=>$this->put('idpropinsi'),
            "idkabupaten"=>$this->put('idkabupaten'),
            "idkecamatan"=>$this->put('idkecamatan'),
            "idkelurahan"=>$this->put('idkelurahan'),
            "idfaskes"=>$this->put('idfaskes'),
            "alamat_pengguna"=>$this->put('alamat_pengguna'),
            "twitter"=>$this->put('twitter'),
            "instagram"=>$this->put('instagram'),
            "playerid"=>$this->put('playerid')
        );
    }*/
unset($data['linkfoto']);
        if($this->put('foto')){
            $uid = $this->M_login->getUUID();


            $cf = "./assets/uploaded/profiles/";
            $cf .= $uid->uid."_".date("Ymd")."_".str_replace(" ","_",$this->put("nama_pengguna")).".jpg";
            $fpath = "./assets/uploaded/profiles/";
            $filename = $uid->uid."_".date("Ymd")."_".str_replace("@","_at_",$this->put("email")).".jpg";
        
            $this->images->base64tojpg($cf,$this->put("foto"),$fpath.$filename,'512','512');
            $foto = base_url()."assets/uploaded/profiles/".$filename;
            $data['foto'] = $foto;

        }elseif($this->put('linkfoto')){
            $data['foto'] = $this->put('linkfoto');
        }

    $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
                if($id=$this->M_login->updateProfile($data,$email)){
        $response = array(
            "status"=>REST_Controller::HTTP_CREATED,
            "error"=>null, 
            "message"=>"Accepted",
            "response"=>$this->M_login->detailProfile($email)               
        );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
            "error"=>"Internal Server Error"               
        );
            $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
    }
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{
        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
}

public function  profile_put($username){
    $data = file_get_contents("php://input","PUT");
    $row = json_decode($data,true);
    
        $data = array(
            "bio" => $this->put('bio')
        );
    $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
                if($id=$this->M_login->updateProfile($data,$username)){
        $response = array(
            "status"=>REST_Controller::HTTP_CREATED,
            "error"=>null, 
            "message"=>"Accepted",
            "response"=>$id               
        );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
            "error"=>"Internal Server Error"               
        );
            $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
    }
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
}
public function changepassword_put(){
    $data = array(
        "userpassword"=>md5($this->put('userpassword')),
        "req_reset"=>null,
        "expired_req"=>null,
        "active"=>"1"
    
    );
    $email = $this->put('email');

    $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
                if($id=$this->M_login->updateProfile($data,$email)){
        $response = array(
            "status"=>REST_Controller::HTTP_CREATED,
            "error"=>null, 
            "message"=>"Accepted",
            "response"=>$id             
        );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
            "error"=>"Internal Server Error"               
        );
            $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
    }



	
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
}



public function changeemail_put(){
    $data = array("email"=>$this->put('email'),"username"=>$this->put('email'));
    $email = $this->put("oldemail");
    

    $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {

                $d = $this->checkEmail($this->put('email'));
                if($d==true){
                if($id=$this->M_login->updateProfile($data,$email)){
        $response = array(
            "status"=>REST_Controller::HTTP_CREATED,
            "error"=>null, 
            "message"=>"Accepted"               
        );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
            "error"=>"Internal Server Error"               
        );
            $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
    }
}else{
    $response = array(
        "status"=>REST_Controller::HTTP_BAD_REQUEST,
        "error"=>"email duplicate"               
    );
        $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
}



	
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
}




public function profile_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
    if($this->input->post('email')){
        $username = $this->input->post('email');
    }else{
        $username = $row['email'];
    }
  
    $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
                if($id=$this->M_login->detailProfile($username)){
                   
        $response = array(
            "status"=>REST_Controller::HTTP_CREATED,
            "error"=>null, 
            "message"=>"Accepted",
            "response"=>$id               
        );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
            "error"=>"Internal Server Error"               
        );
            $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
    }



	
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
    
	


}

public function getuser_get($uuid){
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_login->getProfile($uuid)){
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "message"=>"Accepted",
           "response"=>$id              
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
           "error"=>"Internal Server Error"               
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
}



public function aktifasi_get($uid){
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_login->activated($uuid)){
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "message"=>"Accepted",
           "response"=>$id             
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
           "error"=>"Internal Server Error"               
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
}

public function activated_post(){

    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
    if($this->input->post('username')){
        $username = $this->input->post('username');
    }else{
        $username = $row['username'];
    }
  
    $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
                if($id=$this->M_login->statusActive($username)){
                   
        $response = array(
            "status"=>REST_Controller::HTTP_CREATED,
            "error"=>null, 
            "message"=>"Accepted",
            "response"=>$id               
        );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
            "error"=>"Internal Server Error"               
        );
            $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
    }



	
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }

}


public function validasi_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
    if($this->input->post()){
        $username = $this->input->post('username');
        $password = $this->input->post("oldpassword");
    }else{
        $username = $row['username'];
        $password = $row['oldpassword'];
    }

  
    $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {

                $status = $this->M_login->checkPassword($username,md5($password));
                if($status>0){
                   
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null, 
            "message"=>"Accepted",
            "response"=>true             
        );
            $this->set_response($response, REST_Controller::HTTP_OK);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null, 
            "message"=>"Wrong Password",
            "response"=>false           
        );
            $this->set_response($response, REST_Controller::HTTP_OK);
    }



	
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
    
	


}



}
/** End of file Controllername.php **/
