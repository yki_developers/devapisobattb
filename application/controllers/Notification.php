<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
Class Notification extends REST_Controller{
    public function __construct(){
        parent::__construct();
       $this->load->model('M_notification');
    }




    public function index_get($username){        
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_notification->getListNotifikasi($username)){
                  

       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }



    


    public function index_put($idnotif){
        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);
        $rdata = $this->put();
        


        
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_notification->updateNotifikasi($rdata,$idnotif)){
                  

       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
           "error"=>"Update Failed",
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }



    

    public function index_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        //print_r($row);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
        
           $rdata = $row;
            

        }

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
   if($xp[4]>='3'){
   
       if($result=$this->M_notification->addNotif($rdata)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);
       }
     
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"No Permission",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  
    }



    public function notifikasi_post($username){   
        
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        //print_r($row);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
        
           $rdata = $row;
            

        }
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_notification->getListNotifikasi($username,$rdata)){
                      
    
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
        }
    
}