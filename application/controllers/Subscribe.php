<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Subscribe extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
       // $this->load->model('M_subcribe');
    }


    public function index_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        } 
        $checkmail = $this->db->where("sub_email",$rdata['sub_email'])->get("hp_data_subscriber_bulletin");
        $row = $checkmail->num_rows();
if($row=='0'){
        if($response = $this->db->insert("hp_data_subscriber_bulletin",$rdata)){
            
            $this->set_response(array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"success","response"=>$response),REST_Controller::HTTP_OK);

        }else{
            $this->set_response(array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"unknown","message"=>"Internal server error","response"=>$response),REST_Controller::HTTP_OK);
        }
    }else{
        $this->set_response(array("status"=>REST_Controller::HTTP_CONFLICT,"error"=>"duplicate","message"=>"Email sudah terdaftar","response"=>"false"),REST_Controller::HTTP_CONFLICT);

    }



    }


    public function index_get(){
        $data = $this->db->get("hp_data_subscriber_bulletin")->result();

        if(isset($data)){
            $this->set_response(array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"success","response"=>$data),REST_Controller::HTTP_OK);
        }else{
            $this->set_response(array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"No Data","response"=>null),REST_Controller::HTTP_OK);
        }


    }


}