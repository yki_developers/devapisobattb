<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Rekap extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("M_rekap");
    }

    public function index_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        } 
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_rekap->listHasilScreening($rdata)){
                   
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null,
                    "message"=>"data found",
                    "response"=>$id);    
        $this->set_response($response, REST_Controller::HTTP_OK);  

      
  
  
  
        }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "message"=>"data not found",
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }





    public function index_get($idfaskes){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_rekap->listHasilPerfaskes($idfaskes)){
                       
                    $response = array(
                        "status"=>REST_Controller::HTTP_OK,
                        "error"=>null,
                        "message"=>"data found",
                        "response"=>$id);    
            $this->set_response($response, REST_Controller::HTTP_OK);  
    
          
      
      
      
            }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "message"=>"data not found",
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }


    }

    public function rekaptb16_get($indeks){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_rekap->listReportTB16($indeks)){
                       
                    $response = array(
                        "status"=>REST_Controller::HTTP_OK,
                        "error"=>null,
                        "message"=>"data found",
                        "response"=>$id);    
            $this->set_response($response, REST_Controller::HTTP_OK);  
    
          
      
      
      
            }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "message"=>"data not found",
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }

    }



    public function rekaptb16rk_get($indeks){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_rekap->listReportTB16RK($indeks)){
                       
                    $response = array(
                        "status"=>REST_Controller::HTTP_OK,
                        "error"=>null,
                        "message"=>"data found",
                        "response"=>$id);    
            $this->set_response($response, REST_Controller::HTTP_OK);  
    
          
      
      
      
            }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "message"=>"data not found",
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }

    }



    public function kontaklist_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        } 
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
        $auth = explode(" ",$headers['Authorization']);
       // $decodeBase64 = base64_decode($auth);
          // $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($this->M_auth->getAuth($auth[1])) {
           
                
               if($id=$this->M_rekap->listKontak($rdata)){
                




                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null,
                    "message"=>"data ditemukan",
                    "response"=>$id);    
        $this->set_response($response, REST_Controller::HTTP_OK);  

      
  
  
  
        }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "message"=>"data tidak ditemukan",
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }

               }else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Username or Password";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);

               }


   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }

    }



    public function kontaklist_get($p1){
    
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
        $auth = explode(" ",$headers['Authorization']);
       // $decodeBase64 = base64_decode($auth);
          // $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           //echo $p1;

           if(isset($auth[1])){
           if ($this->M_auth->getAuth($auth[1])) {
           
            if($p1=='all'){
                $id=$this->M_rekap->listKontak();
            }else{
               $id=$this->M_rekap->getlistKontak($p1);
                
            }


                
              


if($id){

                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null,
                    "message"=>"data ditemukan",
                    "response"=>$id);  
                   // print_r($rdata);  
        $this->set_response($response, REST_Controller::HTTP_OK);  

      
  
  
  
        }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "message"=>"data tidak ditemukan",
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }

               }else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Username or Password";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);

               }
            }else{

                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Username or Password";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);

            }


   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }

    }





    public function detailkontak_get($p1){
    
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $auth = explode(" ",$headers['Authorization']);
           // $decodeBase64 = base64_decode($auth);
              // $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               //echo $p1;
               if(isset($auth[1])){
               if ($this->M_auth->getAuth($auth[1])) {
               
                if($p1=='all'){
                    $id=$this->M_rekap->listKontak();
                }else{
                   $id=$this->M_rekap->getDetailKontak($p1);
                    
                }
    
    
                    
                  
    
    
    if($id){
    
                    $response = array(
                        "status"=>REST_Controller::HTTP_OK,
                        "error"=>null,
                        "message"=>"data ditemukan",
                        "response"=>$id);  
                       // print_r($rdata);  
            $this->set_response($response, REST_Controller::HTTP_OK);  
    
          
      
      
      
            }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "message"=>"data tidak ditemukan",
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }
    
                   }else{
                    $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                    $response['error'] = "Invalid Username or Password";
                    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    
                   }
                }else{
                    $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                    $response['error'] = "Invalid Authorization";
                    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                }
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
    
        }

}