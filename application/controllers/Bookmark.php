<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
Class Bookmark extends REST_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model("M_bookmark");
    }

    public function index_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $iduser = $this->input->post('username');
            $idcontent = $this->input->post('idcontent');
        }else{
            $iduser = $row['username'];
            $idcontent = $row['idcontent'];
        }

        $rdata = array("username"=>$iduser,"idcontent"=>$idcontent);


        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
               if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
       if($id=$this->M_bookmark->artikelAdd($rdata)){
           $ret = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "message"=>"Accepted",
               "response"=>$id
               
           );   
       }else{
   $ret= array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
   "error"=>"Internal Server Error");
       }
       $this->set_response($ret, REST_Controller::HTTP_OK);
       
   
               }else{
   
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "Invalid Key";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
           $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "Invalid Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
           $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	
   
   }
   
   
   

    public function index_get($iduser){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
               if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
       if($id=$this->M_bookmark->artikelList($iduser)){
           $ret = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "message"=>"Accepted",
               "response"=>$id
               
           );   
       }else{
   $ret= array("status"=>REST_Controller::HTTP_OK,
   "error"=>null, "message"=>"No Data","response"=>$id);
       }
       $this->set_response($ret, REST_Controller::HTTP_OK);
       
   
               }else{
   
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "Invalid Key";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
           $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "Invalid Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
           $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	
   
   }
   


   public function listfull_get($iduser){
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

           //return $decodedToken;
           if ($decodedToken != false) {
   $xp = explode("_",$decodedToken);
if($xp[3]==$headers['Xkey']){
   if($id=$this->M_bookmark->artikelFullList($iduser)){
       $ret = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "message"=>"Accepted",
           "response"=>$id
           
       );   
   }else{
    $ret= array("status"=>REST_Controller::HTTP_OK,
    "error"=>null, "message"=>"No Data","response"=>$id);
   }
   $this->set_response($ret, REST_Controller::HTTP_OK);
   

           }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "Invalid Key";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           
       }else{
       $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "Invalid Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }

   }else{
       $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
   return;	

}

   

   public function index_delete($bookmark,$username){
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

           //return $decodedToken;
           if ($decodedToken != false) {
   $xp = explode("_",$decodedToken);
if($xp[3]==$headers['Xkey']){
   if($id=$this->M_bookmark->artikelDel($bookmark,$username)){
       $ret = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "message"=>"Accepted",
           "response"=>$id
           
       );   
   }else{
$ret= array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
"error"=>"Internal Server Error");
   }
   $this->set_response($ret, REST_Controller::HTTP_OK);
   

           }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "Invalid Key";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           
       }else{
       $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "Invalid Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }

   }else{
       $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
   return;	

}

   


public function add_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
    if($this->input->post()){
        $idorganisasi = $this->input->post('idorganisasi');
        $iduser = $this->input->post('username');
        $idthread = $this->input->post('idthread');
    }else{
        $idorganisasi = $row['idorganisasi'];
        $iduser = $row['username'];
        $idthread = $row['idthread'];
    }

    $rdata = array("username"=>$iduser,"idthread"=>$idthread,"idorganisasi"=>$idorganisasi);


    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

           //return $decodedToken;
           if ($decodedToken != false) {
   $xp = explode("_",$decodedToken);
if($xp[3]==$headers['Xkey']){
   if($id=$this->M_bookmark->forumAdd($rdata)){
       $ret = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "message"=>"Accepted",
           "response"=>$id
           
       );   
   }else{
$ret= array("status"=>REST_Controller::HTTP_OK,
"error"=>null);
   }
   $this->set_response($ret, REST_Controller::HTTP_OK);
   

           }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "Invalid Key";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           
       }else{
       $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "Invalid Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }

   }else{
       $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
   return;	

}

public function listbyuser_get($username){
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

           //return $decodedToken;
           if ($decodedToken != false) {
   $xp = explode("_",$decodedToken);
if($xp[3]==$headers['Xkey']){
   if($id=$this->M_bookmark->userForumList($username)){
       $ret = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "message"=>"Accepted",
           "response"=>$id
           
       );   
   }else{
    $ret= array("status"=>REST_Controller::HTTP_OK,
    "error"=>null, "message"=>"No Data","response"=>$id);
   }
   $this->set_response($ret, REST_Controller::HTTP_OK);
   

           }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "Invalid Key";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           
       }else{
       $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "Invalid Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }

   }else{
       $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
   return;	
}

public function list_get($username,$idorganisasi){
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

           //return $decodedToken;
           if ($decodedToken != false) {
   $xp = explode("_",$decodedToken);
if($xp[3]==$headers['Xkey']){
   if($id=$this->M_bookmark->forumList($username,$idorganisasi)){
       $ret = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "message"=>"Accepted",
           "response"=>$id
           
       );   
   }else{
    $ret= array("status"=>REST_Controller::HTTP_OK,
    "error"=>null, "message"=>"No Data","response"=>$id);
   }
   $this->set_response($ret, REST_Controller::HTTP_OK);
   

           }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "Invalid Key";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           
       }else{
       $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "Invalid Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }

   }else{
       $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
   return;	
}


public function threadlist_get($username,$idorganisasi){
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

           //return $decodedToken;
           if ($decodedToken != false) {
   $xp = explode("_",$decodedToken);
if($xp[3]==$headers['Xkey']){
   if($id=$this->M_bookmark->threadList($username,$idorganisasi)){
       $ret = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "message"=>"Accepted",
           "response"=>$id
           
       );   
   }else{
    $ret= array("status"=>REST_Controller::HTTP_OK,
    "error"=>null, "message"=>"No Data","response"=>$id);
   }
   $this->set_response($ret, REST_Controller::HTTP_OK);
   

           }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "Invalid Key";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           
       }else{
       $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "Invalid Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }

   }else{
       $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
   return;	

}


public function del_delete($username,$idthread){
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

           //return $decodedToken;
           if ($decodedToken != false) {
   $xp = explode("_",$decodedToken);
if($xp[3]==$headers['Xkey']){
   if($id=$this->M_bookmark->forumDel($username,$idthread)){
       $ret = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "message"=>"Accepted",
           "response"=>$id
           
       );   
   }else{
$ret= array("status"=>REST_Controller::OK,
"error"=>null);
   }
   $this->set_response($ret, REST_Controller::HTTP_OK);
   

           }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "Invalid Key";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           
       }else{
       $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "Invalid Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }

   }else{
       $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
   return;	

}

}