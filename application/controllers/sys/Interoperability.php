<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Interoperability extends REST_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sys/M_interoperability');
    }

    public function index_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{

            $rdata = $row;
        }
        $headers=$this->input->request_headers();
       // print_r($headers);

        


        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
            //echo $decodedToken."<br>";

if($rdata['indeks_person_id']!='0'){

            if($dataExists = $this->M_interoperability->checkPersonId($rdata['indeks_person_id'])){
                $response['response'] = $dataExists;


            }else{

$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => "http://training.sitb.id/api_indekskasus_sb/api/v1.php?table=v_indeks_apisobattb&action=list&&q=(person_id~equals~".$rdata['indeks_person_id'].")",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => array(),
  CURLOPT_HTTPHEADER => array(
    'Authorization: Basic aW5kZWtza2FzdXNfYXBpOjRwMXMxdGI=',
    'Cookie: runnerSession=r3ovcsqdvakej7xf5yvb; pxIbfpRIxMQUyzfVp6XiA=o4f730iqk1smtmicfdefd7s9g4'
  ),
  
));

$resp= curl_exec($curl);
curl_close($curl);
$dm = json_decode($resp);

if($dm->data != []) {


if(!is_array($dm)){
//print_r($dm);
$q = array(
    "nama_kecamatan"=>$dm->data[0]->kecamatan,
    "idpropinsi"=>$dm->data[0]->kode_prov,
    "idkabupaten"=>$dm->data[0]->prov_kab
);
$kecamatan = $this->M_interoperability->getKodeKecamatan($q);
if($dm->data[0]->jenis_kelamin=='perempuan'){
    $sex = '2';
}else{
    $sex ='1';
}

if($dm->data[0]->hasil_diagnosis=='TBC SO'){
    $jenis = '1';
}else{
    $jenis='2';
}

$data = array(
    "indeks_id"=>null,
    "indeks_tb03register"=>null,
    "indeks_jenis"=>$jenis,

    "indeks_name"=>$dm->data[0]->nama,
    "indeks_propinsi"=>$dm->data[0]->kode_prov,
    "indeks_kabupaten"=>$dm->data[0]->prov_kab,
    "indeks_kecamatan"=>$kecamatan->idkecamatan,
    "indeks_puskesmas"=>null,
    "indeks_kader"=>null,
    "indeks_person_id"=>$dm->data[0]->person_id,
    "indeks_jenis_kelamin"=>$sex,
    "indeks_tanggal_lahir"=>$dm->data[0]->tgl_lahir,
    "indeks_tanggal_diagnosa"=>$dm->data[0]->tgl_hasil_diagnosis,
    "kabupaten"=>$dm->data[0]->kab_kota,
    "propinsi"=>$dm->data[0]->provinsi,
    "kecamatan"=>$dm->data[0]->kecamatan
);
}else{
$q = array(
    "nama_kecamatan"=>$dm->data[0]->kecamatan,
    "idpropinsi"=>$dm->data[0]->kode_prov,
    "idkabupaten"=>$dm->data[0]->prov_kab
);
$kecamatan = $this->M_interoperability->getKodeKecamatan($q);
if($dm->data[0]->jenis_kelamin=='perempuan'){
    $sex = '2';
}else{
    $sex ='1';
}

$data = array(
    "indeks_id"=>null,
    "indeks_tb03register"=>null,
    "indeks_jenis"=>"1",

    "indeks_name"=>$dm->data[0]->nama,
    "indeks_propinsi"=>$dm->data[0]->kode_prov,
    "indeks_kabupaten"=>$dm->data[0]->prov_kab,
    "indeks_kecamatan"=>$kecamatan->idkecamatan,
    "indeks_puskesmas"=>null,
    "indeks_kader"=>null,
    "indeks_person_id"=>$dm->data[0]->person_id,
    "indeks_jenis_kelamin"=>$sex,
    "indeks_tanggal_lahir"=>$dm->data[0]->tgl_lahir,
    "indeks_tanggal_diagnosa"=>$dm->data[0]->tgl_hasil_diagnosis,
    "kabupaten"=>$dm->data[0]->kab_kota,
    "propinsi"=>$dm->data[0]->provinsi,
    "kecamatan"=>$dm->data[0]->kecamatan
);



}
$response['response'] = $data;


}else{
    $response['response'] = array();
}

            }

$response['status'] = REST_Controller::HTTP_OK;
$response['error'] = null;
$this->set_response($response, REST_Controller::HTTP_OK);
        }else{
$response['status'] = REST_Controller::HTTP_OK;
$response['error'] = null;
$this->set_response($response, REST_Controller::HTTP_OK);
        }

//print_r($response);

            }else{
        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "Invalid Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            }


         
    }else{
    $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
}
return;
}




}