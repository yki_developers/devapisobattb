<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Multilogin extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("M_login");
    }

    public function index_post(){


        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if(!$this->input->post()){
            $sso_login = base64_decode($row['sso_login']);
            //$playerid = $row['playerid'];
        }else{
            $sso_login = base64_decode($this->input->post("sso_login"));
           
            //$playerid = $this->input->post('playerid');
        }

        $split = explode(":",$sso_login);
    
        $authUser = array(
            "username"=>$split[0],
            "email"=>$split[1]
        );
    
        $headers=$this->input->request_headers();
         if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
    
                //return $decodedToken;
                if ($decodedToken != false) {
        $xp = explode("_",$decodedToken);
    if($xp[3]==$headers['Xkey']){
        if($id=$this->M_login->getAuthUserSSO($authUser)){
            if($id->usergroup=='3'){
                $id->kdfasyankes = $this->M_login->getFaskes($split[0]);
            }elseif($id->usergroup=='10'){
                $id->kader_id = $this->M_login->getKaderId($split[0]);
            }
            $ret = array(
                "status"=>REST_Controller::HTTP_OK,
                "error"=>null,
                "message"=>"Accepted",
                "response"=>$id
                
            );   
        }else{
    $ret= array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
    "error"=>"Internal Server Error");
        }
        $this->set_response($ret, REST_Controller::HTTP_OK);
        
    
                }else{
    
            $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
            $response['error'] = "Invalid Key";
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                }
                
            }else{
            $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
            $response['error'] = "Invalid Token Authorization";
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            }
    
        }else{
            $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
            $response['error'] = "No Token Authorization";
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
        }
        return;	



    }
}