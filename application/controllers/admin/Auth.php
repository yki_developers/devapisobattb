<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Auth extends REST_controller{
    public function __construct(){
        parent::__construct();
        $this->load->model("admin/M_auth");
    }

    public function index_get(){
        $headers=$this->input->request_headers();
        if(array_key_exists('Xkey',$headers) && !empty($headers['Xkey'])){
        
        if($dataq = $this->M_auth->getToken($headers['Xkey'])){
           $src_token = $dataq->token;
                   $token['status'] = REST_Controller::HTTP_OK;
                   $token['error'] = null;
                   $token['message']= "Accepted";
                   $token['response']['token'] = AUTHORIZATION::generateToken($src_token);
                   $this->set_response($token,REST_Controller::HTTP_OK);
        }else{
               
               $token['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $token['error'] = "Invalid Key";
               //$st['token']="";
               $this->set_response($token, REST_Controller::HTTP_UNAUTHORIZED);
        }
        }
    }


    public function list_get(){
        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

            if ($decodedToken != false) {
                
                $result = $this->M_auth->getListXkey($headers['Xkey']);
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null, 
                    "response"=>$result
                    
                );
				//echo $idlab;
                $this->set_response($response, REST_Controller::HTTP_OK);
                
            }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
				
			}


     }else{
         $response = array(
             "status"=>REST_Controller::HTTP_UNAUTHORIZED,
             "error"=>"No Token Authorization",
         );
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
     }


    }

    

    public function index_delete($xkey){
        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

            if ($decodedToken != false) {
                $xp = explode("_",$decodedToken);
                if($xp[4]>='5'){

                $result = $this->M_auth->deleteXkey($xkey);
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null, 
                    "response"=>$result
                    
                );
				//echo $idlab;
                $this->set_response($response, REST_Controller::HTTP_OK);
                
            }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
				
            }
        }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Permission",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
        }


     }else{
         $response = array(
             "status"=>REST_Controller::HTTP_UNAUTHORIZED,
             "error"=>"No Token Authorization",
         );
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
     }


    }
    public function add_post(){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if(!$this->input->post()){
            $rdata = array(
                "user_id"=>$row['userid'],
                "key"=>$row['key'],
                "level"=>$row['level']
            );
        }else{
            $rdata = array(
                "user_id"=>$this->input->input->post('userid'),
                "key"=>$this->input->input->post("key"),
                "level"=>$this->input->post("level")
            );
        }
        

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
              if ($decodedToken != false) {
                   $xp = explode("_",$decodedToken);
                   if($xp[4]>='5'){
                $result = $this->M_auth->newXkey($rdata);
                   $response = array(
                       "status"=>REST_Controller::HTTP_OK,
                       "error"=>null, 
                       "response"=>$result
                       
                   );
                   //echo $idlab;
                   $this->set_response($response, REST_Controller::HTTP_OK);
                   
               
           }else{
               $response = array(
                   "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                   "error"=>"No Permission"
               );
              $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
        }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
        }
           
   
   
        }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
        }
        
    }
}
