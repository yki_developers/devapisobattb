<?php

Class Monitoring extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/M_monitoring');
    }


    public function viewartikel(){
        $data['tgl'] = $this->M_monitoring->getTanggal();
        $data['artikel'] = $this->M_monitoring->getListArtikel();
        $this->load->view("viewartikel",$data);
    }

    public function pengguna(){
        $data['userlist'] = $this->M_monitoring->getRekapPengguna();
        $this->load->view("viewpengguna",$data);
    }
}