<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;


Class Screening extends REST_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/M_screening');
    }

    public function tokengenerator_get(){
        $response = $this->M_screening->getToken();
        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function index_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        } 

        $pass = md5($rdata['program_password']);
        unset($rdata['program_password']);
        $rdata['program_password']=$pass;
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_screening->insert($rdata)){

                $datauser= array(
                    "nama_pengguna"=>$rdata['program_nama'],
                    "username"=>$rdata['program_token'],
                    "userpassword"=>$rdata['program_password'],
                    "usergroup"=>'12'
                );
                $this->M_screening->addUserKerjasama($datauser);
                

       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }




    public function datalist_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        } 
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_screening->getDataFilter($rdata)){
                

       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }


    public function index_get(){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_screening->getList()){
                    
    
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }


    }

    public function index_put($id){
        $rdata = $this->put();

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_screening->update($rdata,$id)){
                    
    
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }

    }

    public function index_delete($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_screening->delete($id)){
                    
    
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }


    }

    public function detail_get($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_screening->detail($id)){
                    
    
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
    }




    public function getdata_get($method){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {

                if($method=="pekerja"){
                    $result = $this->M_screening->getDataScreeningPekerja();
                }elseif($method=="sekolahdewasa"){
                    $result = $this->M_screening->getDataScreeningDewasaSekolah();

                }elseif($method=='sekolahanak'){
                    $result = $this->M_screening->getDataScreeningAnakSekolah();

                }
                   if($result){
                    
    
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "response"=>$result               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }


    }



    public function listtoken_get($username){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {

          $result = $this->M_screening->getListByUser($username);
                   if($result){
                    
    
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "response"=>$result               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "response"=>$result               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }



    }




    public function report_post($method){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        } 

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {

                if($method=="pekerja"){
                    $result = $this->M_screening->getDataHasilScreeningPekerja($rdata);
                }elseif($method=="sekolahdewasa"){
                    $result = $this->M_screening->getDataHasilScreeningDewasaSekolah($rdata);

                }elseif($method=='sekolahanak'){
                    $result = $this->M_screening->getDataHasilScreeningAnakSekolah($rdata);

                }elseif($method=='umum'){
                    $result = $this->M_screening->getDataHasilScreeningUmum($rdata);
                }elseif($method=='anak'){
                    $result = $this->M_screening->getDataHasilScreeningAnak($rdata);

                }elseif($method=="sekolah"){
                    $result = $this->M_screening->getDataHasilScreeningSekolah($rdata);

                }elseif($method=='odhiv'){
                    $result = $this->M_screening->getDataHasilScreeningODHIV($rdata);
                }
                   if($result){
                    
    
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "response"=>$result               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "response"=>$result

           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }


    }



}
?>