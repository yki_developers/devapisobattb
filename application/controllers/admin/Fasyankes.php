<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;


Class Fasyankes extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('admin/M_fasyankes');
        $this->load->library("images");
    }





    



    public function search_post()
    {
    
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $search = $this->input->post('search');
           $page = $this->input->post('page');
            $size = $this->input->post('size');
        }else{
            $search = $row['search'];
            $page = $row['page'];
            $size = $row['size'];
        }
        
        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

            if ($decodedToken != false) {
                
                if($idfasyankes = $this->M_fasyankes->SearchFasyankes($search,$page)){
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null, 
                    "response"=>$idfasyankes
                );
            }else{
                    $response = array(
                        "status"=>REST_Controller::HTTP_OK,
                        "error"=>null, 
                        "message"=>"no data",
                        "response"=>false
                    );
            }
				//echo $idlab;
                $this->set_response($response, REST_Controller::HTTP_OK);
                
            }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
				
			}


     }else{
         $response = array(
             "status"=>REST_Controller::HTTP_UNAUTHORIZED,
             "error"=>"No Token Authorization",
         );
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
     }

    }





    public function detail_get($idfaskes){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               if ($decodedToken != false) {
                   $idfasyankes = $this->M_fasyankes->detailFasyankes($idfaskes);
                   //echo $idlab;
                   $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null, 
                    "response"=>$idfasyankes
                    
                );
				//echo $idlab;
                $this->set_response($response, REST_Controller::HTTP_OK);
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   
               }

    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
}



public function fasyankes_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
    if($this->input->post()){
        $insertArray = $this->input->post();
    }else{
        $insertArray = $row;
    }
    
  /*  $path = "./assets/uploaded/fasyankes/";
    $thumbnail = "./assets/uploaded/fasyankes/thumbnail/";
    $foto = date("Ymd")."_".$insertArray['kdfasyankes'].".jpg";
    $nsrc = $path.$foto;
    $tsrc = $thumbnail.$foto;
    $this->images->base64tojpg($nsrc,$String64,$tsrc,512,512);
    $this->images->base64tojpg($nsrc,$String64,$nsrc,1125,450);
    $imgSrc = base_url()."assets/uploaded/fasyankes/".$foto;
    $tmbSrc = base_url()."assets/uploaded/fasyankes/thumbnail/".$foto;

    $insertArray['foto'] = $imgSrc;
    $insertArray['thumbnails']=$tmbSrc;
    */


    //$newArray = array_push($insertArray,"foto"=>$imgSrc,"thumbnail"=>$tmbSrc);

    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

           if ($decodedToken != false) {
               if($id = $this->M_faskes->insertNew($insertArray)){
                   $response = array(
                       "status"=>REST_Controller::HTTP_OK,
                       "error"=>null, 
                       "message"=>"Accepted");
                       $this->set_response($response, REST_Controller::HTTP_OK);

               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null,
                    "message"=>"Insert Error",
                    "response"=>false
                );
                    $this->set_response($response, REST_Controller::HTTP_OK);
               };
               //echo $idlab;
             
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               
           }


}else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"No Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
}
}




public function index_put($id){
    $data = file_get_contents("php://input","PUT");
    $row = json_decode($data,true);
    $insertArray = $this->put(); 

    
//print_r($this->put());
    if($this->put('foto')==""){
              
        unset($insertArray['foto']);
    }else{
    $path = "./assets/uploaded/fasyankes/";
    $thumbnail = "./assets/uploaded/fasyankes/thumbnail/";
    //$insertArray = $this->put(); 
    //$foto = date("Ymd")."_".$insertArray['kdfasyankes'].".jpg";
    $ext = explode(".",$insertArray['namafile']);
        $foto = date("Ymd")."_".$insertArray['idfasyankes'].".".$ext[1];
        $nsrc = $path.$foto;
        $tsrc = $thumbnail.$foto;
        $this->images->base64tojpg($nsrc,$insertArray['foto'],$tsrc,512,512);
        $this->images->base64tojpg($nsrc,$insertArray['foto'],$nsrc,1125,450);
        $imgSrc = base_url()."assets/uploaded/fasyankes/".$foto;
        $tmbSrc = base_url()."assets/uploaded/fasyankes/thumbnail/".$foto;
        $insertArray['foto'] = $imgSrc;
        $insertArray['thumbnails']=$tmbSrc;
        unset($insertArray['namafile']);
    }





//print_r($insertArray);
//die();
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           if ($decodedToken != false) {
               if($id = $this->M_fasyankes->updateFaskes($insertArray,$id)){
                  $response = array(
                      "status"=>REST_Controller::HTTP_OK,
                      "error"=>null, 
                      "message"=>"Accepted",
                      "response"=>$id 
                  );
                  $this->set_response($response, REST_Controller::HTTP_OK);
               }else{
                   $response = array(
                       "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 
                       "error"=>"Internal Server Error"
                   );
                   $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
               };
               //echo $idlab;
               
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               
           }


}else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"No Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
}


}



public function index_get($idkabupaten){
    $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               if ($decodedToken != false) {
                   $idfasyankes = $this->M_fasyankes->listFasyankes($idkabupaten);
                   //echo $idlab;
                   $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null, 
                    "response"=>$idfasyankes
                    
                );
				//echo $idlab;
                $this->set_response($response, REST_Controller::HTTP_OK);
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   
               }

    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
}



public function perkecamatan_get($idkecamatan){
    $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               if ($decodedToken != false) {
                   $idfasyankes = $this->M_fasyankes->listFasyankesByKecamatan($idkecamatan);
                   //echo $idlab;
                   $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null, 
                    "response"=>$idfasyankes
                    
                );
				//echo $idlab;
                $this->set_response($response, REST_Controller::HTTP_OK);
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   
               }

    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
}



public function faskeslist_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
    if($this->input->post()){
        $rdata = $this->input->post();
    }else{
        $rdata = $row;
    }

  
  
    $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               if ($decodedToken != false) {
                   $idfasyankes = $this->M_fasyankes->filterFasyankes($rdata);
                   //echo $idlab;
                   $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null, 
                    "response"=>$idfasyankes
                    
                );
				//echo $idlab;
                $this->set_response($response, REST_Controller::HTTP_OK);
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   
               }

    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
}



}