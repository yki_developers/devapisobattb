<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Kader extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/M_kader');
    }




    public function index_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        } 
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {

$cq = $this->M_kader->checkUsername($rdata['kader_username']);
$ce = $this->M_kader->checkEmail($rdata['kader_email']);

if($cq=='0'){

    if($ce=='0'){

               if($id=$this->M_kader->insert($rdata)){
                   $ndata = array(
                       "username"=>$rdata['kader_username'],
                       "userpassword"=>md5($rdata['kader_password']),
"email"=>$rdata['kader_email'],
                       "idpropinsi"=>$rdata["kader_propinsi"],
                       "idkabupaten"=>$rdata["kader_kabupaten"],
                       "idkecamatan"=>$rdata["kader_kecamatan"],
                       "idfaskes"=>$rdata["kader_puskesmas"],
                       "nama_pengguna"=>$rdata["kader_name"],
                       "usergroup"=>"10"
                   );
                
if($ins = $this->M_kader->insertUserMobile($ndata)){
    $response = array(
        "status"=>REST_Controller::HTTP_OK,
        "error"=>null, 
        "response"=>$ins             
    );
 $this->set_response($response, REST_Controller::HTTP_OK);

}else{
    $this->M_kader->delteUser($id);
    $response = array(
        "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
        "error"=>"500",
        "response"=>$id               
    );
        $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);  
}
      
  
  
  
        }else{
       $response = array(
           "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
           "error"=>"500",
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }


}else{

    $response = array(
        "status"=>REST_Controller::HTTP_BAD_REQUEST,
        "error"=>"400",
        "response"=>"Duplicate",
        "message"=>"Email ini Sudah digunakan"               
    );
        $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
}


}else{

    $response = array(
        "status"=>REST_Controller::HTTP_BAD_REQUEST,
        "error"=>"400",
        "response"=>"Duplicate",
        "message"=>"Username Sudah digunakan"               
    );
        $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
}


   # end checked



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }



   


    public function listkader_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        } 
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_kader->getListKader($rdata)){
                   
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null,
                    "message"=>"data found",
                    "response"=>$id);    
        $this->set_response($response, REST_Controller::HTTP_OK);  

      
  
  
  
        }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "message"=>"data not found",
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }



    public function index_get($idkader){
        
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_kader->detail($idkader)){
                   
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null,
                    "message"=>"data found",
                    "response"=>$id);    
        $this->set_response($response, REST_Controller::HTTP_OK);  

      
  
  
  
        }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "message"=>"data not found",
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }





    public function index_put($idkader){
        $data = file_get_contents("php://input","PUT");
        $rdata = $this->put();
        unset($rdata['kader_username']);
        unset($rdata['kader_password']);
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_kader->update($rdata,$idkader)){
                   
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null,
                    "message"=>"success",
                    "response"=>$id);    
        $this->set_response($response, REST_Controller::HTTP_OK);  

      
  
  
  
        }else{
       $response = array(
           "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
           "error"=>null,
           "message"=>"failed",
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }





    public function chpassword_put($idkader){
        $data = file_get_contents("php://input","PUT");
        $rdata = $this->put();
        $md5 = md5($rdata['kader_password']);
        $username = $rdata['kader_username'];
       unset($rdata['kader_username']);
        //unset($rdata['kader_password']);
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
              
               if($id=$this->M_kader->update($rdata,$idkader)){
                   $ndata = array("userpassword"=>$md5);
                   if($id=$this->M_kader->updatePassword($ndata,$username)){
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null,
                    "message"=>"success",
                    "response"=>$id);  
                    $this->set_response($response, REST_Controller::HTTP_OK);   
        
                }else{
                    $response = array(
                        "status"=>REST_Controller::HTTP_INTERNAL_SEREVER_ERROR,
                        "error"=>null,
                        "message"=>"update password gagal",
                        "response"=>$id);    
                        $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR); 

                }

                 
  
  
  
        }else{
       $response = array(
           "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
           "error"=>null,
           "message"=>"failed",
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }




    



    public function index_delete($idkader){
        
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {

                $kaderinfo = $this->M_kader->detail($idkader);
                   if($id=$this->M_kader->deleteUser($idkader)){
                       if($this->M_kader->deleteMobileakun($kaderinfo->kader_username)){
                        $response = array(
                            "status"=>REST_Controller::HTTP_OK,
                            "error"=>null,
                            "message"=>"data berhasil dihapus",
                            "response"=>REST_Controller::HTTP_OK);   
                            
                            $this->set_response($response, REST_Controller::HTTP_OK);  

                       }else{

                        $response = array(
                            "status"=>REST_Controller::HTTP_OK,
                            "error"=>null,
                            "message"=>"tidak ada akun mobile untuk kader ini",
                            "response"=>REST_Controller::HTTP_OK              
                        );
                            $this->set_response($response, REST_Controller::HTTP_OK);



                       }
                       
                  
            }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "message"=>"tidak ada data yang dihapus",
               "response"=>REST_Controller::HTTP_OK              
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
        }
    
    
    
    
    

    

    
}