<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Organisasi extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->library("images");
        $this->load->model('admin/M_organisasi');
    }

    public function index_post(){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
          $rdata = $this->input->post();
        }else{
            $rdata = $row;
        }
        //print_r($rdata);
        $key = "logo";
        $path = "./assets/uploaded/organisasi/";
        $ext = explode(".",$rdata['namafile']);
        $filename = date("Ymd")."_".md5(str_replace(" ","-",$rdata['nama_pendek'])).".".$ext[1];
        $src = $path.$filename;
        $this->images->base64tojpg($src,$rdata['logo'],$src,"250","250");
        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

            if ($decodedToken != false) {
               unset($rdata[$key]);
               unset($rdata['namafile']);
               $rdata['logoorganisasi'] = base_url()."assets/uploaded/organisasi/".$filename;
                //print_r($rdata);
                //die();
                if($id = $this->M_organisasi->addOrganisasi($rdata)){
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null, 
                    "message"=>"accepted",
                    "response"=>$id
                );
            }else{
                    $response = array(
                        "status"=>REST_Controller::HTTP_OK,
                        "error"=>null, 
                        "message"=>"no data",
                        "response"=>false
                    );
            }
				//echo $idlab;
                $this->set_response($response, REST_Controller::HTTP_OK);
                
            }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
				
			}


     }else{
         $response = array(
             "status"=>REST_Controller::HTTP_UNAUTHORIZED,
             "error"=>"No Token Authorization",
         );
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
     }


    }

    public function index_get(){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               if ($decodedToken != false) {
                   
                   if($idorganisasi = $this->M_organisasi->listOrganisasi()){
                   $response = array(
                       "status"=>REST_Controller::HTTP_OK,
                       "error"=>null, 
                       "message"=>"accepted",
                       "response"=>$idorganisasi
                   );
               }else{
                       $response = array(
                           "status"=>REST_Controller::HTTP_OK,
                           "error"=>null, 
                           "message"=>"no data",
                           "response"=>false
                       );
               }
                   //echo $idlab;
                   $this->set_response($response, REST_Controller::HTTP_OK);
                   
               }else{
                   $response = array(
                       "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                       "error"=>"Invalid Token Authorization",
                   );
                  $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   
               }
   
   
        }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
        }



    }

    public function index_put($id){

        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);
        $rdata = $this->put();
       // print_r($rdata);
        //die();
        if($this->put('namafile')==""){
            unset($rdata['namafile']);
        }else{
            $key = "logo";
            $path = "./assets/uploaded/organisasi/";
            $ext = explode(".",$rdata['namafile']);
            $filename = date("Ymd")."_".md5(str_replace(" ","-",$rdata['nama_pendek'])).".".$ext[1];
            $src = $path.$filename;
            $this->images->base64tojpg($src,$rdata['logo'],$src,"250","250");
            unset($rdata['namafile']);
            $rdata['logoorganisasi'] = base_url()."assets/uploaded/organisasi/".$filename;
        }
        
        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

            if ($decodedToken != false) {
                
                if($idorganisasi = $this->M_organisasi->updateOrganisasi($rdata,$id)){
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null, 
                    "message"=>"accepted",
                    "response"=>$idorganisasi
                );
            }else{
                    $response = array(
                        "status"=>REST_Controller::HTTP_OK,
                        "error"=>null, 
                        "message"=>"no data",
                        "response"=>false
                    );
            }
				//echo $idlab;
                $this->set_response($response, REST_Controller::HTTP_OK);
                
            }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
				
			}


     }else{
         $response = array(
             "status"=>REST_Controller::HTTP_UNAUTHORIZED,
             "error"=>"No Token Authorization",
         );
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
     }



    }

    public function index_delete($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               if ($decodedToken != false) {
                   
                   if($idorganisasi = $this->M_organisasi->deleteOrganisasi($id)){
                   $response = array(
                       "status"=>REST_Controller::HTTP_OK,
                       "error"=>null, 
                       "message"=>"accepted",
                       "response"=>$idorganisasi
                   );
               }else{
                       $response = array(
                           "status"=>REST_Controller::HTTP_OK,
                           "error"=>null, 
                           "message"=>"no data",
                           "response"=>false
                       );
               }
                   //echo $idlab;
                   $this->set_response($response, REST_Controller::HTTP_OK);
                   
               }else{
                   $response = array(
                       "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                       "error"=>"Invalid Token Authorization",
                   );
                  $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   
               }
   
   
        }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
        }


    }

    public function detail_get($id){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               if ($decodedToken != false) {
                   
                   if($idorganisasi = $this->M_organisasi->detailOrganisasi($id)){
                   $response = array(
                       "status"=>REST_Controller::HTTP_OK,
                       "error"=>null, 
                       "message"=>"accepted",
                       "response"=>$idorganisasi
                   );
               }else{
                       $response = array(
                           "status"=>REST_Controller::HTTP_OK,
                           "error"=>null, 
                           "message"=>"no data",
                           "response"=>false
                       );
               }
                   //echo $idlab;
                   $this->set_response($response, REST_Controller::HTTP_OK);
                   
               }else{
                   $response = array(
                       "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                       "error"=>"Invalid Token Authorization",
                   );
                  $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   
               }
   
   
        }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
        }


    }

    public function search_post(){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
          $rdata = $this->input->post('search');
        }else{
            $rdata = $row['search'];
        }
        
        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

            if ($decodedToken != false) {
                
                if($id = $this->M_organisasi->searchOrganisasi($rdata)){
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null, 
                    "message"=>"accepted",
                    "response"=>$id
                );
            }else{
                    $response = array(
                        "status"=>REST_Controller::HTTP_OK,
                        "error"=>null, 
                        "message"=>"no data",
                        "response"=>false
                    );
            }
				//echo $idlab;
                $this->set_response($response, REST_Controller::HTTP_OK);
                
            }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
				
			}


     }else{
         $response = array(
             "status"=>REST_Controller::HTTP_UNAUTHORIZED,
             "error"=>"No Token Authorization",
         );
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
     }




    }


}