<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
Class Forum extends REST_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model("admin/M_forum");
        //$this->load->library("images");
    }

    public function index_get($idorg){

$headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
   if($xp[4]>='1'){
   
       if($result=$this->M_forum->getForum($idorg))
       {
       

        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
        
        }else{
            $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
            $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
           }
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Permission",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       
           
       
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
       return;	  

    }




    public function reply_get($idthread){

        $headers=$this->input->request_headers();
                if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                       $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           
                       //return $decodedToken;
               if ($decodedToken != false) {
               $xp = explode("_",$decodedToken);
           if($xp[3]==$headers['Xkey']){
           
           if($xp[4]>='1'){
           
               if($result=$this->M_forum->getReply($idthread))
               {
               
        
                $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
                $this->set_response($response,REST_Controller::HTTP_OK);  
                
                }else{
                    $response = array(
                        "status"=>REST_Controller::HTTP_OK,
                        "error"=>null,
                        "message"=>"no data",
                        "response"=>false);
                    $this->set_response($response,REST_Controller::HTTP_OK);
                   }
                 
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"No Permission",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
                   
               
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                           }
                           
                       }else{
                        $response = array(
                            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                            "error"=>"Invalid Token Authorization",
                        );
                       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                       }
               
                   }else{
               
                    $response = array(
                        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                        "error"=>"No Token Authorization",
                    );
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
               return;	  
        
            }

            


    public function index_put($id){

        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);

       
            $rdata = array(
                "status_thread"=>$this->PUT('status_thread')
            );



        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
   if($xp[4]>='3'){
   
       if($result=$this->M_forum->updateForum($rdata,$id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
        
        }else{
            $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
            $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
           }
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Permission",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       
           
       
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
       return;

    }

}
