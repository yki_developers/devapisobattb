<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Users extends REST_Controller{
    public function __construct (){
        parent::__construct();
        $this->load->model("admin/M_users");
    }

 public function admingroup_get(){
    if($result = $this->M_users->adminGroup()){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);
    }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"No Data","response"=>false);
        $this->set_response($response,REST_Controller::HTTP_OK);
    }
    return;

 }  


public function adduseradmin_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
    if($this->input->post()){
        $rdata = $this->input->post();
    }else{
        $rdata = $row;
    }
  


    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
               $xp = explode("_",$decodedToken);
               if($xp[4]>='4'){
               $echk = $this->M_users->email_validation($rdata['email']);
               $uchk = $this->M_users->username_validation($rdata['username']); 

if($echk==true && $uchk==true){
if($rdata['usergroup']=='3'){
    $kdfasyankes = $rdata['kd_fasyankes'];
    unset($rdata['kd_fasyankes']);
}elseif($rdata['usergroup']=='4'){
    $idorganisasi = $rdata['idorganisasi'];
    unset($rdata['idorganisasi']);
}

            if($result = $this->M_users->addUser($rdata)){
                $d = $this->M_users->email_mobile($rdata['email']);
                if($d==true){
                $userMobile = array(
                    "username"=>$rdata['username'],
                    "userpassword"=>$rdata['userpassword'],
                    "nama_pengguna"=>$rdata['nama_pengguna'],
                    "email"=>$rdata['email'],
                    "usergroup"=>$rdata['usergroup'],
                    "idfaskes"=>$kdfasyankes
                );
                $this->M_users->addUserMobile($userMobile);
            }
            if($rdata['usergroup']=='3'){
                $this->M_users->addUserFasyankes(
                    array(
                        "username"=>$rdata['username'],
                        "kd_fasyankes"=>$kdfasyankes,
                        "superuser"=>"1"
                        )
                    );

            }elseif($rdata['usergroup']=='4'){

                $this->M_users->addUserOrganisasi(
                    array(
                        "username"=>$rdata['username'],
                        "idorganisasi"=>$idorganisasi,
                        "superuser"=>"1"
                        )


                    );
            }

               $response = array(
                   "status"=>REST_Controller::HTTP_OK,
                   "error"=>null,
                   "message"=>"Accepted", 
                   "response"=>$result
                   
               );
               //echo $idlab;
               $this->set_response($response, REST_Controller::HTTP_OK);
            }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null,
                    "message"=>"No Data", 
                    "response"=>false
                    
                );
                //echo $idlab;
                $this->set_response($response, REST_Controller::HTTP_OK);
            }

        }else{
            if($uchk==false && $echk==false){
                $msg = "Username dan Email sudah digunakan";
            }elseif($uchk==false && $echk==true){
                $msg = "Username Sudah digunakan";
            }elseif($uchk==true && $echk==false){
                $msg = "Alamat Email sudah terdaftar";
            }
            $response = array(
                "status"=>REST_Controller::HTTP_CONFLICT,
                "error"=>null,
                "message"=>$msg            );
           $this->set_response($response, REST_Controller::HTTP_CONFLICT);

        }

           
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_UNAUTHORIZED,
               "error"=>"No Permission"
           );
          $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
       


    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
    

}


   
public function chpassword_put($username){
    $data = file_get_contents("php://input","PUT");
    $row = json_decode($data,true);

    $rdata = $this->put();

    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
               $xp = explode("_",$decodedToken);
               if($xp[4]>='4'){
              
            if($result = $this->M_users->updateUser($rdata,array("username"=>$username))){
               $response = array(
                   "status"=>REST_Controller::HTTP_OK,
                   "error"=>null,
                   "message"=>"Accepted", 
                   "response"=>$result
                   
               );
               //echo $idlab;
               $this->set_response($response, REST_Controller::HTTP_OK);
            }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null,
                    "message"=>"No Data", 
                    "response"=>false
                    
                );
                //echo $idlab;
                $this->set_response($response, REST_Controller::HTTP_OK);
            }

        

           
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_UNAUTHORIZED,
               "error"=>"No Permission"
           );
          $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
       


    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }



}   
public function updateuseradmin_put($username){
    $data = file_get_contents("php://input","PUT");
    $row = json_decode($data,true);

    $rdata = $this->put();


    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
               $xp = explode("_",$decodedToken);
               if($xp[4]>='4'){
              
            if($result = $this->M_users->updateUser($rdata,array("username"=>$username))){
               $response = array(
                   "status"=>REST_Controller::HTTP_OK,
                   "error"=>null,
                   "message"=>"Accepted", 
                   "response"=>$result
                   
               );
               //echo $idlab;
               $this->set_response($response, REST_Controller::HTTP_OK);
            }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null,
                    "message"=>"No Data", 
                    "response"=>false
                    
                );
                //echo $idlab;
                $this->set_response($response, REST_Controller::HTTP_OK);
            }

        

           
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_UNAUTHORIZED,
               "error"=>"No Permission"
           );
          $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
       


    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }




}

 public function index_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);

    if(!$this->input->post()){
        $rdata = $row;
        $password = md5($row['userpassword']);
       

    }else{

        $rdata = $this->input->post();
        $password = md5($this->input->post('userpassword'));
       
    }
    unset($rdata['userpassword']);
    $rdata['userpassword'] = $password;

   
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
               $xp = explode("_",$decodedToken);
               if($xp[4]>='4'){

$userchecked = $this->M_users->checkUsername($rdata['username']);
$emailchecked = $this->M_users->checkEmail($rdata['email']);

if($userchecked=='0'){

    if($emailchecked=='0'){

            if($result = $this->M_users->addUser($rdata)){

                if($rdata['usergroup']=='4'){
                    $userData = array("idorganisasi"=>$rdata['idorganisasi'],"username"=>$rdata['username'],"superuser"=>$rdata['leveluser']);
                    $this->M_users->addUserOrganisasi($userData);
                }elseif($rdata['usergroup']=='3'){
                    $userData = array("kd_fasyankes"=>$rdata['kdfasyankes'],"username"=>$rdata['username'],"superuser"=>$rdata['leveluser']);
                    $this->M_users->addUserFasyankes($userData);
                }

                $userMobile = array(
                    "username"=>$rdata['username'],
                    "userpassword"=>$rdata['userpassword'],
                    "nama_pengguna"=>$rdata['nama_pengguna'],
                    "email"=>$rdata['email'],
                    "nohp"=>$rdata['nohp'],
                    "usergroup"=>$rdata['usergroup']
                );

                $this->M_users->addUserMobile($userMobile);

               $response = array(
                   "status"=>REST_Controller::HTTP_OK,
                   "error"=>null,
                   "message"=>"Accepted", 
                   "response"=>$result
                   
               );
               //echo $idlab;
               $this->set_response($response, REST_Controller::HTTP_OK);
            }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_OK,
                    "error"=>null,
                    "message"=>"No Data", 
                    "response"=>false
                    
                );
                //echo $idlab;
                $this->set_response($response, REST_Controller::HTTP_OK);
            }

        }else{
            $response = array(
                "status"=>REST_Controller::HTTP_BAD_REQUEST,
                "error"=>"400",
                "response"=>"Duplicate",
                "message"=>"Email Sudah digunakan"               
            );
                $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);


        }

        }else{
            $response = array(
                "status"=>REST_Controller::HTTP_BAD_REQUEST,
                "error"=>"400",
                "response"=>"Duplicate",
                "message"=>"Username Sudah digunakan"               
            );
                $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }






           
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_UNAUTHORIZED,
               "error"=>"No Permission"
           );
          $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
       


    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
   }


   public function index_put($username){
    $data = file_get_contents("php://input","PUT");
    $row = json_decode($data,true);

  
       
       $rdata = array(
        "usergroup" => $this->put('usergroup'),
        "nama_pengguna" => $this->put('nama_pengguna'),
        "email" => $this->put('email'),
        "nohp" => $this->put('nohp'),
        "jenis_kelamin" => $this->put('jenis_kelamin'),
        "tgl_lahir"=>$this->put('tgl_lahir')
        );

    
        $id = array("username"=>$username);  
   
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
               $xp = explode("_",$decodedToken);
               if($xp[4]>='4'){
            $result = $this->M_users->updateUser($rdata,$id);
               $response = array(
                   "status"=>REST_Controller::HTTP_OK,
                   "error"=>null,
                   "message"=>"Accepted", 
                   "response"=>$result
                   
               );
               //echo $idlab;
               $this->set_response($response, REST_Controller::HTTP_OK);
               
           
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_UNAUTHORIZED,
               "error"=>"No Permission"
           );
          $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
       


    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
   }

   
   public function password_put($username){
    $data = file_get_contents("php://input","PUT");
    $row = json_decode($data,true);

  
       
       $rdata = array(
        "userpassword" => md5($this->put('userpassword'))
        );

    
        $id = array("username"=>$username);  
   
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
               $xp = explode("_",$decodedToken);
               if($xp[4]>='4'){
            $result = $this->M_users->updateUser($rdata,$id);
               $response = array(
                   "status"=>REST_Controller::HTTP_OK,
                   "error"=>null,
                   "message"=>"Accepted", 
                   "response"=>$result
                   
               );
               //echo $idlab;
               $this->set_response($response, REST_Controller::HTTP_OK);
               
           
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_UNAUTHORIZED,
               "error"=>"No Permission"
           );
          $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
       


    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }


   }



   public function index_get($page,$size){
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
               $xp = explode("_",$decodedToken);
               if($xp[4]>='4'){
            $result = $this->M_users->listUsers($page,$size);
            $total = $this->M_users->countAll();
               $response = array(
                   "status"=>REST_Controller::HTTP_OK,
                   "error"=>null,
                   "message"=>"Accepted", 
                   "response"=>$result,
                   "total"=>$total
                   
               );
               //echo $idlab;
               $this->set_response($response, REST_Controller::HTTP_OK);
               
           
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_UNAUTHORIZED,
               "error"=>"No Permission"
           );
          $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
       


    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
   }

   
   
   
   






   
   public function index_delete($username){
     $id = array("username"=>$username);  
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
               $xp = explode("_",$decodedToken);
               if($xp[4]>='4'){
            $result = $this->M_users->delUser($id);
               $response = array(
                   "status"=>REST_Controller::HTTP_OK,
                   "error"=>null,
                   "message"=>"Accepted", 
                   "response"=>$result
               );
               //echo $idlab;
               $this->set_response($response, REST_Controller::HTTP_OK);
               
           
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_UNAUTHORIZED,
               "error"=>"No Permission"
           );
          $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
       


    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
   }


   public function detail_get($username){

    //$id = array("username"=>$username);  
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
               $xp = explode("_",$decodedToken);
               if($xp[4]>='4'){
            $result = $this->M_users->detailUser($username);
            unset($result->userpassword);
               $response = array(
                   "status"=>REST_Controller::HTTP_OK,
                   "error"=>null,
                   "message"=>"Accepted", 
                   "response"=>$result
               );
               //echo $idlab;
               $this->set_response($response, REST_Controller::HTTP_OK);
               
           
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_UNAUTHORIZED,
               "error"=>"No Permission"
           );
          $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
       


    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
   }



   public function orglist_get($idorganisasi){

    $idorg = array("idorganisasi"=>$idorganisasi);  
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
               $xp = explode("_",$decodedToken);
               if($xp[4]>='4'){
            $result = $this->M_users->orgUser($idorg);
               $response = array(
                   "status"=>REST_Controller::HTTP_OK,
                   "error"=>null,
                   "message"=>"Accepted", 
                   "response"=>$result
               );
               //echo $idlab;
               $this->set_response($response, REST_Controller::HTTP_OK);
               
           
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_UNAUTHORIZED,
               "error"=>"No Permission"
           );
          $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
       


    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
   }


   public function fasyankeslist_get($idfasyankes){

    $idfaskes = array("kd_fasyankes"=>$idfasyankes);  
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
               $xp = explode("_",$decodedToken);
               if($xp[4]>='4'){
            $result = $this->M_users->faskesUser($idfaskes);
               $response = array(
                   "status"=>REST_Controller::HTTP_OK,
                   "error"=>null,
                   "message"=>"Accepted", 
                   "response"=>$result
               );
               //echo $idlab;
               $this->set_response($response, REST_Controller::HTTP_OK);
               
           
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_UNAUTHORIZED,
               "error"=>"No Permission"
           );
          $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
       


    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
   }


   public function adminfasyankes_get($idpropinsi,$idkabupaten,$page,$size){
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
               $xp = explode("_",$decodedToken);
               if($xp[4]>='4'){
            $result = $this->M_users->adminFaskes($idpropinsi,$idkabupaten,$page,$size);
            $total = $this->M_users->countAdmFaskes($idpropinsi,$idkabupaten);
               $response = array(
                   "status"=>REST_Controller::HTTP_OK,
                   "error"=>null,
                   "message"=>"Accepted", 
                   "response"=>$result,
                   "total"=>$total
               );
               //echo $idlab;
               $this->set_response($response, REST_Controller::HTTP_OK);
               
           
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_UNAUTHORIZED,
               "error"=>"No Permission"
           );
          $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
       


    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
   }




   public function adminkomunitas_get($page,$size){
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
               $xp = explode("_",$decodedToken);
               if($xp[4]>='4'){
            $result = $this->M_users->adminKomunitas($page,$size);
            $total = $this->M_users->countKomunitas();
               $response = array(
                   "status"=>REST_Controller::HTTP_OK,
                   "error"=>null,
                   "message"=>"Accepted", 
                   "response"=>$result,
                   "total"=>$total
               );
               //echo $idlab;
               $this->set_response($response, REST_Controller::HTTP_OK);
               
           
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_UNAUTHORIZED,
               "error"=>"No Permission"
           );
          $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
       


    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
   }



   public function dinkesprop_get($propinsi,$page,$size){
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
               $xp = explode("_",$decodedToken);
               if($xp[4]>='4'){
            $result = $this->M_users->listUsersProp($propinsi,$page,$size);
            $total = $this->M_users->countProp($propinsi);
               $response = array(
                   "status"=>REST_Controller::HTTP_OK,
                   "error"=>null,
                   "message"=>"Accepted", 
                   "response"=>$result,
                   "total"=>$total
                   
               );
               //echo $idlab;
               $this->set_response($response, REST_Controller::HTTP_OK);
               
           
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_UNAUTHORIZED,
               "error"=>"No Permission"
           );
          $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
       


    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
   }




   
   public function dinkeskab_get($kab,$page,$size){
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
               $xp = explode("_",$decodedToken);
               if($xp[4]>='4'){
            $result = $this->M_users->listUsersKab($kab,$page,$size);
            $total = $this->M_users->countKab($kab);
               $response = array(
                   "status"=>REST_Controller::HTTP_OK,
                   "error"=>null,
                   "message"=>"Accepted", 
                   "response"=>$result,
                   "total"=>$total
                   
               );
               //echo $idlab;
               $this->set_response($response, REST_Controller::HTTP_OK);
               
           
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_UNAUTHORIZED,
               "error"=>"No Permission"
           );
          $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
       


    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
   }



   public function skrining_get($username){
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
          if ($decodedToken != false) {
               $xp = explode("_",$decodedToken);
            $result = $this->M_users->checkUserSkrining($username);
            //$total = $this->M_users->countAll();
               $response = array(
                   "status"=>REST_Controller::HTTP_OK,
                   "error"=>null,
                   "message"=>"Accepted", 
                   "response"=>$result   
               );
               //echo $idlab;
               $this->set_response($response, REST_Controller::HTTP_OK);
               
           
      
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
       


    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization"
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
   }


}