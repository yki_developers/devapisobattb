<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;


Class About extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('M_about');
    }


    public function index_put(){
        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);
         $rdata = $this->put();

        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);

			//return $decodedToken;
    if ($decodedToken != false) {
    $xp = explode("_",$decodedToken);
if($xp[3]==$headers['Xkey']){

if($xp[4]>='3'){

    if($result=$this->M_about->updateAbout($rdata)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
        
        }else{
            $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
            $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
           }
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Permission",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       
           
       
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
    return;	


    }


    public function index_get(){

        if($result = $this->M_about->getAbout()){
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
            $this->set_response($response,REST_Controller::HTTP_OK);
        }else{
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data","response"=>false);
            $this->set_response($response,REST_Controller::HTTP_OK);
        }
        return;

    }

    public function version_get(){
        $result = $this->M_about->getVersion();
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);

    }

    
public function  tentang_get(){
    if($result = $this->M_about->getTentang()){

        $text = json_decode($result->sys_text_tentang);
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$text);
        $this->set_response($response,REST_Controller::HTTP_OK);
    }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data","response"=>false);
        $this->set_response($response,REST_Controller::HTTP_OK);
    }
    return;
}


private function mailfeedback($rdata){

    $helper = $this->M_about->getHelper();
    $this->M_about->insertKontakMe($rdata);
        //print_r($rdata);
    $this->mail = $this->phpmailer_load->load(); 
    $this->mail->isSMTP(); 
    $this->mail->Host = "smtp.office365.com"; 
    //$this->mail->SMTPDebug = 2;
    $this->mail->SMTPAuth = true;
    $this->mail->Username = "admin.apps@yki4tbc.org";
    $this->mail->Password = 'YKItbM7j/hZx}\8/k&';
    $this->mail->SMTPSecure = 'tls';
    $this->mail->Port = 587;
   // $this->mail->addReplyTo($rdata['kontak_email'],$rdata['kontak_nama_pengirim']);
    $this->mail->setFrom('admin.apps@yki4tbc.org', 'ADMIN - SOBATTB'); 
    $this->mail->Subject = "Terima kasih SOBATTB";
    
        $this->mail->addAddress($rdata['kontak_email'],$rdata['kontak_nama_pengirim']);
 
    
    //$this->mail->addAddress("harmi.prasetyo@yki4tbc.org","harmi prasetyo"); 
   // $this->mail->addCC("harmi.prasetyo@gmail.com","harmi prasetyo"); 
  
//$html = $this->M_greeting->getHtml();
//$body= str_replace("<<First Name>>","<b>".strtoupper($nama)."</b>",$html);
  $body = "YTH : ".$rdata['kontak_nama_pengirim']."<br>\r\n";
  $body .= "<br><br><br>\r\n";
  $body .= "Terima kasih telah menghubungi kami melalui SOBATTB,<br>\r\n";
  $body .= "Sesuai masukan/saran  anda sebagaimana tertulis dibawah ini :<br>\r\n";
  
  $body .="<br><br>\r\n";
$body .= "<code>".$rdata['kontak_text']."</code>";
  $body.="<br><br>\r\n";
  $body.="akan segera kami tindak lanjuti";
  $body .="<br><br>\r\n";
  $body.="Terima kasih<br>\r\n";
  $body.="Admin SOBATTB\r\n";


$this->mail->msgHtml($body);
$this->mail->send();

}


public function kontak_post(){

    $data = file_get_contents("php://input");
    $row = json_decode($data,true);

    if(!$this->input->post()){
        $rdata =$row;
    }else{

        $rdata = $this->input->post();

    }

    $helper = $this->M_about->getHelper();
    $this->M_about->insertKontakMe($rdata);
        //print_r($rdata);
    $this->mail = $this->phpmailer_load->load(); 
    $this->mail->isSMTP(); 
    $this->mail->Host = "smtp.office365.com"; 
    //$this->mail->SMTPDebug = 2;
    $this->mail->SMTPAuth = true;
    $this->mail->Username = "admin.apps@yki4tbc.org";
    $this->mail->Password = 'YKItbM7j/hZx}\8/k&';
    $this->mail->SMTPSecure = 'tls';
    $this->mail->Port = 587;
    $this->mail->addReplyTo($rdata['kontak_email'],$rdata['kontak_nama_pengirim']);
    $this->mail->setFrom('admin.apps@yki4tbc.org', 'kontak Kami - SOBATTB'); 
    $this->mail->Subject = "Pesan Kontak SOBATTB dari pengguna";
    foreach($helper as $to){
        $this->mail->addAddress($to->helper_email,$to->helper_name);
    }
    
    //$this->mail->addAddress("harmi.prasetyo@yki4tbc.org","harmi prasetyo"); 
   // $this->mail->addCC("harmi.prasetyo@gmail.com","harmi prasetyo"); 
  
//$html = $this->M_greeting->getHtml();
//$body= str_replace("<<First Name>>","<b>".strtoupper($nama)."</b>",$html);
  $body = "Nama Pengirim : ".$rdata['kontak_nama_pengirim']."<br>\r\n";
  $body .= "E-mail : ".$rdata['kontak_email']."<br>\r\n";
  $body .= "<br><br><br>\r\n";
  $body .= $rdata['kontak_text'];

$this->mail->msgHtml($body);
if($this->mail->send()){
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"sent","response"=>true);
    $this->set_response($response,REST_Controller::HTTP_OK);
    
    $rtxt = array("kontak_email"=>$rdata['kontak_email'],"kontak_nama_pengirim"=>$rdata['kontak_nama_pengirim']);
    $this->mailfeedback($rdata);

}else{
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"not sent","response"=>false);
    $this->set_response($response,REST_Controller::HTTP_OK);

    //echo $this->mail->ErrorInfo.'Gh!E&Fy}Y54F]8ppYC$F!8';
}

}



}