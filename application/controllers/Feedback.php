<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Feedback extends REST_Controller{

    public function __construct(){
        parent::__construct();
        //$this->load->library("Images");
        $this->load->model("M_feedback");
    }

    private function tblFeddback($id=null){
        return "tbl_penilaian_faskes ".$id;
    }

    public function index_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = array(
                "username"=>$this->input->post("username"),
                "nama_pengguna"=>$this->input->post("nama_pengguna"),
                "jml_bintang"=>$this->input->post("jml_bintang"),
                "kd_fasyankes"=>$this->input->post("kd_fasyankes"),
                "nama_fasyankes"=>$this->input->post("nama_fasyankes"),
                "status_penilaian"=>"pending",
                "feedback"=>$this->input->post("feedback")
                

            );
        }else{
            $rdata = array(
                "username"=>$row["username"],
                "nama_pengguna"=>$row["nama_pengguna"],
                "jml_bintang"=>$row["jml_bintang"],
                "kd_fasyankes"=>$row["kd_fasyankes"],
                "nama_fasyankes"=>$row["nama_fasyankes"],
                "status_penilaian"=>"pending",
                "feedback"=>$row['feedback']

            );
        }
        $rdata['tgl_post'] = date("Y-m-d");


    $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
                if($id=$this->M_feedback->writeFeedback($rdata)){
        $response = array(
            "status"=>REST_Controller::HTTP_CREATED,
            "error"=>null, 
            "message"=>"Accepted"               
        );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
            "error"=>"Internal Server Error"               
        );
            $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
    }



	
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
    


    }

    public function index_get($page,$size){
        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
                if($id=$this->M_feedback->getFeedback($page,$size)){
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null, 
            "message"=>"Accepted"               
        );
        $response['response'] = $id;
            $this->set_response($response, REST_Controller::HTTP_OK);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null, 
            "message"=>"No data"               
        );
        $response['response'] = $id;               
    
            $this->set_response($response, REST_Controller::HTTP_OK);
    }



	
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
    


    }

    public function approved_get($kdfaskes){

        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
                if($id=$this->M_feedback->getFeedbackApproved($kdfaskes)){
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null, 
            "message"=>"Accepted"               
        );
        $response['response'] = $id;
            $this->set_response($response, REST_Controller::HTTP_OK);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null, 
            "message"=>"No data"               
        );
        $response['response'] = $id;               
        
            $this->set_response($response, REST_Controller::HTTP_OK);
    }



	
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }

    }

    public function index_put($idfeedback){
        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);
        $rdata = array("jml_bintang"=>$this->put("jml_bintang"),"feedback"=>$this->put("feedback"));
        

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_feedback->updateFeedback($rdata,$idfeedback)){
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "message"=>"Accepted",
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
               "error"=>"Internal Server Error"               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
   
   
   
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
   
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }


    }

    public function index_delete($idfeedback){
        $rdata = array("status_penilaian"=>"remove");
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_feedback->updateFeedback($rdata,$idfeedback)){
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "message"=>"Accepted",
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
               "error"=>"Internal Server Error"               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
   
   
   
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
   
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }



    }

    public function detail_get($idfeedback){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_feedback->getFeedbackDetail($idfeedback)){
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "message"=>"Accepted"               
           );
           $response['response'] = $id;
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "message"=>"No data"               
           );
           $response['response'] = $id;               
           
               $this->set_response($response, REST_Controller::OK);
       }
   
   
   
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
   
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
   
    }

    public function approval_put($idfeedback){

        $rdata = array("status_penilaian"=>$this->put("status_penilaian"));
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {
                   if($id=$this->M_feedback->updateFeedback($rdata,$idfeedback)){
                    $pid = $this->M_feedback->getPlayerId($idfeedback);           
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "message"=>"Accepted",
               "response"=>$pid               
           );
        //$response['response']['idpenilaian']=$idfeedback;
        $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
               "error"=>"Internal Server Error"               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
   
   
   
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
   
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }

    }


    public function listperfaskes_get($kdfasyankes){
        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
                if($id=$this->M_feedback->getFeedbackPerfaskes($kdfasyankes)){
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null, 
            "message"=>"Accepted"               
        );
        $response['response'] = $id;
            $this->set_response($response, REST_Controller::HTTP_OK);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null, 
            "message"=>"No data"               
        );
        $response['response'] = $id;               
    
            $this->set_response($response, REST_Controller::HTTP_OK);
    }



	
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
    


    }





    public function totalbintang_get($kdfaskes){
        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
                if($id=$this->M_feedback->getTotalbintang($kdfaskes)){
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null, 
            "message"=>"Accepted",  
            "response"=>$id             
        );
            $this->set_response($response, REST_Controller::HTTP_OK);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null,
            "message"=>"Accepted",
            "response"=>array("bintang"=>"0")            
        );
            $this->set_response($response, REST_Controller::OK);
    }



	
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
    }


    public function reply_post(){
        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = array(
                "username"=>$this->input->post('username'),
                "idpenilaian"=>$this->input->post("idpenilaian"),
                "kd_fasyankes"=>$this->input->post("kd_fasyankes"),
                "balasan"=>$this->input->post("balasan")

            );
        }else{

            $rdata = array(
                "username"=>$row['username'],
                "idpenilaian"=>$row["idpenilaian"],
                "kd_fasyankes"=>$row["kd_fasyankes"],
                "balasan"=>$row["balasan"]

            );

        }
        $rdata['tglReply'] = date("Y-m-d");

        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
                if($id=$this->M_feedback->replyFeedback($rdata)){
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null, 
            "message"=>"Accepted",  
            "response"=>$id             
        );
            $this->set_response($response, REST_Controller::HTTP_OK);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null,
            "message"=>"Accepted",
            "response"=>false            
        );
            $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
    }



	
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }

    }


    public function reply_get($idfeedback){
        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
                if($id=$this->M_feedback->getReply($idfeedback)){
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null, 
            "message"=>"Accepted",  
            "response"=>$id             
        );
            $this->set_response($response, REST_Controller::HTTP_OK);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null,
            "message"=>"Accepted",
            "response"=>false            
        );
            $this->set_response($response, REST_Controller::OK);
    }



	
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }


    }

    public function reply_delete($idreply){
        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
                if($id=$this->M_feedback->delReply($idreply)){
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null, 
            "message"=>"Accepted",  
            "response"=>$id             
        );
            $this->set_response($response, REST_Controller::HTTP_OK);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null,
            "message"=>"Accepted",
            "response"=>false            
        );
            $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
    }



	
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }


    }



    public function reply_put($idreply){
        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);
 
            $rdata = array(
                "username"=>$this->put('username'),
                "idpenilaian"=>$this->put("idpenilaian"),
                "kd_fasyankes"=>$this->put("kd_fasyankes"),
                "balasan"=>$this->put("balasan")

            );
       
        $rdata['tglReply'] = date("Y-m-d");

        $headers=$this->input->request_headers();
	 if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
			//return $decodedToken;
            if ($decodedToken != false) {
                if($id=$this->M_feedback->updateFeedback($$idpro,$idreply)){
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null, 
            "message"=>"Accepted",  
            "response"=>$id             
        );
            $this->set_response($response, REST_Controller::HTTP_OK);
    }else{
        $response = array(
            "status"=>REST_Controller::HTTP_OK,
            "error"=>null,
            "message"=>"Accepted",
            "response"=>false            
        );
            $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
    }



	
			}else{
                $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                $response['error'] = "Invalid Token Authorization";
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			}
	}else{

        $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        $response['error'] = "No Token Authorization";
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }

    }


}