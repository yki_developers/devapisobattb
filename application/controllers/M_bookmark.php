<?php
Class M_bookmark extends CI_Model{
    public function __construct(){
        parent::__construct();

    }

    private function tblBookmarkArtikel($id=null){
        return "tbl_bookmark_artikel ".$id;
    }

    private function tblInfotb($id){
        return "tbl_cont_infotb ".$id;
    }

    private function tblViewer($id){
        return "tbl_cont_viewer ".$id;
    }

    private function mstKategori($id){
        return "mst_cont_kategori ".$id;
    }

    public function artikelAdd($rdata){
        return $this->db->insert($this->tblBookmarkArtikel(),$rdata);
    }

    public function artikelList($userid){
        $this->db->select("a.idbookmark");
        $this->db->select("a.idcontent");
        //$this->db->select("b.judul_artikel");
        $this->db->from($this->tblBookmarkArtikel("a"));
        $this->db->join($this->tblInfotb("b"),"a.idcontent=b.idcontent");
        $this->db->where("a.username",$userid);
        return $this->db->get()->result();
    }


    public function artikelFullList($userid){
        $this->db->select("a.idbookmark,a.idcontent,b.idcategori,d.kategori,b.judul_artikel,b.headlines,date_format(b.tgl_terbit,'%d/%m/%Y') as tgl_terbit");
        $this->db->select("b.author");
        $this->db->select("b.reviewer");
        $this->db->select("date_format(b.last_edit,'%d/%m/%Y') as last_edit");
        $this->db->select("b.foto");
        $this->db->select("if(isnull(c.jml_viewer),0,c.jml_viewer) as jml_viewer");
        $this->db->from($this->tblBookmarkArtikel("a"));
        $this->db->join($this->tblInfotb("b"),"a.idcontent=b.idcontent");
        $this->db->join($this->tblViewer("c"),"b.idcontent=c.idcontent","left");
        $this->db->join($this->mstKategori("d"),"b.idcategori=d.idkategori");
        $this->db->where("a.username",$userid);
        return $this->db->get()->result();
    }

    public function artikelDel($idcontent,$userid){
        return $this->db->delete($this->tblBookmarkArtikel(),array("idcontent"=>$idcontent,"username"=>$userid));
    }




    private function tblBookmarkForum($id=null){
        return "tbl_bookmark_thread ".$id;
    }


    public function forumAdd($rdata){
        return $this->db->insert($this->tblBookmarkForum(),$rdata);
    }

    public function forumList($userid,$idorganisasi){
        $this->db->select("a.*");
        $this->db->select("b.judul_thread");
        $this->db->from($this->tblBookmarkForum("a"));
        $this->db->join("tbl_forum_thread b","a.idthread=b.idthread");
        $this->db->join("mst_organisasi c","a.idorganisasi=c.idorganisasi");

        $this->db->where("a.username",$userid);
        $this->db->where("a.idorganisasi",$idorganisasi);

        return $this->db->get()->result();
    }


    public function userForumList($userid){
        $this->db->select("a.*");
        $this->db->select("b.judul_thread");
        $this->db->from($this->tblBookmarkForum("a"));
        $this->db->join("tbl_forum_thread b","a.idthread=b.idthread");
        $this->db->join("mst_organisasi c","a.idorganisasi=c.idorganisasi");
        $this->db->where("a.username",$userid);
        return $this->db->get()->result();
    }



    public function threadList($username,$idorganisasi){
        $this->db->select("a.*");
        $this->db->select("b.*");
        $this->db->from($this->tblBookmarkForum("a"));
        $this->db->join("tbl_forum_thread b","a.idthread=b.idthread");
        $this->db->join("mst_organisasi c","a.idorganisasi=c.idorganisasi");

        $this->db->where("a.username",$username);
        $this->db->where("a.idorganisasi",$idorganisasi);

        return $this->db->get()->result();
    }

    public function forumDel($username,$idthread){
        return $this->db->delete($this->tblBookmarkForum(),array("username"=>$username,"idthread"=>$idthread));
    }

}