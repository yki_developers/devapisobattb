<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Appointment extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("M_appointment");
    }

    private function notifikasi($rdata,$playerid){
        $content = array("en"=>$rdata,"id"=>$rdata);
        $data = array(
            "app_id"=>$this->config->item("OS_API_ID"), 
   
               "include_player_ids"=>array($playerid),
   
               /* "included_segments" => array(
                   'All'
               ),*/
              // "data"=>array(),
               "contents"=>$content
           );
           $jsondata = json_encode($data);
           $ch = curl_init();
           curl_setopt($ch, CURLOPT_URL, $this->config->item('OS_API_URL'));
           curl_setopt($ch, CURLOPT_HTTPHEADER, array(
               'Content-Type: application/json; charset=utf-8', $this->config->item('OS_API_KEY')
             
           ));
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
           curl_setopt($ch, CURLOPT_HEADER, FALSE);
           curl_setopt($ch, CURLOPT_POST, TRUE);
           curl_setopt($ch, CURLOPT_POSTFIELDS, $jsondata);
           curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
           
           $response = curl_exec($ch);
           curl_close($ch);
           return $response;
    }



    public function index_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        }
      
  
        


        
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_appointment->insert($rdata)){
               
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }




    public function index_put($idapp){
        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);
        $rdata = $this->put();

        if($this->input->post('appointment_verifikasi')){

        if($rdata['appointment_verifikasi']=='1'){

            
        $xp = explode(" ",$rdata['appointment_plan']);
        //print_r($xp);
        $dp = explode("-",$xp[0]);
        $tgl = $dp['2']."/".$dp['1']."/".$dp[0];
            $app = $this->M_appointment->getAppointmentDetail($idapp);

            $text = "Kunjungan kamu telah diverifikasi, kamu dijadwalkan berkunjung pada ".$tgl.". Jangan lupa menerapkan 3M (memakai masker, mencuci tangan, menjaga jarak) saat berkunjung ke layanan";
            if($ply= $this->M_appointment->getNamaPengguna($app->appointment_username)){

              
                    $udata = array(
                        "notifikasi_username"=>$ply->username,
                        "notifikasi_playerid"=>$ply->playerid,
                        "notifikasi_text"=>$text
                    );   
                    if($this->M_appointment->insertNotifikasi($udata)){
                        $this->notifikasi($text,$ply->playerid);
                    }
                   


            }
        }
    }


        
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_appointment->update($rdata,$idapp)){
                  

       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "response"=>$id             
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }





    public function index_get($faskes){
     
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_appointment->getListAppointment($faskes)){
                  

       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }




    public function kabupaten_get($kab){
        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);
        $rdata = $this->put();
        


        
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_appointment->getListAppointmentKab($kab)){
                  

       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }




    public function propinsi_get($prop){
        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);
        $rdata = $this->put();
        


        
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_appointment->getListAppointmentProp($prop)){
                  

       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }







    public function userlist_get($username){
       
         
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($id=$this->M_appointment->getListAppointmentUser($username)){
                  

       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "response"=>$id               
       );
           $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }
    }




   






    public function filter_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        }
  
        


        
    $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if($result=$this->M_appointment->getFilter($rdata)){

       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "message"=>"Accepted",
           "response"=>$result               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "message"=>"no data",
           "response"=>$result               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }


    }


    public function faskes_post($faskes){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        }


        $headers=$this->input->request_headers();
    if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
           $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
           //return $decodedToken;
           if ($decodedToken != false) {
               if(isset($rdata['search'])){
                   $result = $this->M_appointment->searchListAppointment($faskes,$rdata);
               }else{
                $result = $this->M_appointment->newListAppointment($faskes,$rdata);

               }
               if($result){

       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null, 
           "message"=>"Accepted",
           "response"=>$result               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }else{
       $response = array(
           "status"=>REST_Controller::HTTP_OK,
           "error"=>null,
           "message"=>"no data",
           "response"=>$result               
       );
           $this->set_response($response, REST_Controller::HTTP_OK);
   }



   
           }else{
               $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
               $response['error'] = "Invalid Token Authorization";
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   }else{

       $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
       $response['error'] = "No Token Authorization";
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
   }

  

    }


    public function userlist_post($username){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        }

       
         
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
               //return $decodedToken;
               if ($decodedToken != false) {

                if(isset($rdata['search'])){
                    $id = $this->M_appointment->searchListAppointmentUser($username,$rdata);
                }else{
                    $id = $this->M_appointment->newListAppointmentUser($username,$rdata);
                }
                   if($id){
                      
    
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null, 
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_OK);
       }else{
           $response = array(
               "status"=>REST_Controller::HTTP_OK,
               "error"=>null,
               "response"=>$id               
           );
               $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
    
    
    
       
               }else{
                   $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
                   $response['error'] = "Invalid Token Authorization";
                   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       }else{
    
           $response['status'] = REST_Controller::HTTP_UNAUTHORIZED;
           $response['error'] = "No Token Authorization";
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
        }

}