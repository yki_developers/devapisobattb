<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Podcast extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_podcast');
    }

    public function themes_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        }


        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($result=$this->M_podcast->insertTheme($rdata)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


       


    }

    public function themes_get(){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($result=$this->M_podcast->getListTheme()){

        $i=0;
        foreach($result as $res){   
        $ress = $this->M_podcast->totalEpisode($res->theme_id);
        $result[$i]->total_episode = $ress;
        $i++;}
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data");
        $this->set_response($response,REST_Controller::HTTP_OK);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    }

    public function themes_put($id){

        $rdata = $this->put();


        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($result=$this->M_podcast->updateTheme($rdata,$id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data");
        $this->set_response($response,REST_Controller::HTTP_OK);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  



    }

    public function themes_delete($id){

       

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($result=$this->M_podcast->deleteTheme($id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data to be remove");
        $this->set_response($response,REST_Controller::HTTP_OK);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  



    }

    public function themesdetail_get($id){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($result=$this->M_podcast->getDetailTheme($id)){

        $i=0;
          
        $ress = $this->M_podcast->totalEpisode($result->theme_id);
        $result->total_episode = $ress;
        
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data");
        $this->set_response($response,REST_Controller::HTTP_OK);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    }






    private function notifikasi($judul){
        $content = array(
            "en"=>"Hai, Sobat TB!!,  Podcast terbaru berjudul :".$judul." sudah terbit di SOBATTB",
            "id"=>"Hai, Sobat TB!!,  Podcast terbaru berjudul :".$judul." sudah terbit di SOBATTB");
        //$id = $this->input->post('idcontent');

        $data = array(
            "app_id"=>$this->config->item('OS_API_ID'),
          // "include_player_ids"=>array('61d59ef8-20c6-4e06-8856-79e30496a23f'),

             "included_segments" => array(
                'All'
            ),
            "data"=>array("idcontent"=>"podcast"),
            "contents"=>$content
        );

        $jsondata = json_encode($data);
        //3print_r($jsondata);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic NDgyODNjZTItOTk2Zi00ZjllLTg5NDAtNjljM2E2ZWY5MWU3'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsondata);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
        $response = curl_exec($ch);
        curl_close($ch);
        
       print_r($response);

    }



    
    public function index_post(){


        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
           
        }else{
            $rdata = $row;
            
        }
       
       

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($result=$this->M_podcast->insert($rdata)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
        $this->notifikasi($rdata['podcast_title']);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  








    }

    public function index_get(){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($result=$this->M_podcast->getList()){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data");
        $this->set_response($response,REST_Controller::HTTP_OK);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  



    }

    public function index_put($id){
        $data = file_get_contents("php://input","PUT");
        $row = json_decode($data,true);

        $rdata = $this->put();


        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($result=$this->M_podcast->update($rdata,$id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data");
        $this->set_response($response,REST_Controller::HTTP_OK);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  


    }

    public function index_delete($id){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($result=$this->M_podcast->delete($id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>null,"message"=>"Internal Server Error");
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  

    }

    public function detail_get($id){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($result=$this->M_podcast->getDetail($id)){
        $ress = $this->M_podcast->totalComment($result->podcast_id);
        $result->total_comment = $ress;
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data");
        $this->set_response($response,REST_Controller::HTTP_OK);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  

    }




    public function newdetail_get($id,$username){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($result=$this->M_podcast->getDetail($id)){
        $ress = $this->M_podcast->totalComment($result->podcast_id);

        $check = $this->M_podcast->checkUserView($username,$id);
        if($check==0){
            $rdata = array(
                "dpp_username"=>$username,
                "dpp_podcast_id"=>$id,
                "dpp_date"=>date("Y-m-d H:i:s"),
                "dpp_poin_value"=>"1"
            );
            $this->M_podcast->updatePoin($rdata);
        }
        $result->total_comment = $ress;
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data");
        $this->set_response($response,REST_Controller::HTTP_OK);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  

    }







    public function episodelist_get(){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($result=$this->M_podcast->getListByTheme()){

        $i=0;
        foreach($result as $res){   
        $ress = $this->M_podcast->totalComment($res->podcast_id);
        $result[$i]->total_comment = $ress;
        $i++;}



        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data");
        $this->set_response($response,REST_Controller::HTTP_OK);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  



    }






    public function comment_post(){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        }
       
      

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($result=$this->M_podcast->insertComment($rdata)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  



    }
    public function comment_get($pid){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($result=$this->M_podcast->getComment($pid)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data");
        $this->set_response($response,REST_Controller::HTTP_OK);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  

    }
    public function comment_put($id){
        $rdata = $this->put();
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($result=$this->M_podcast->updateComment($rdata,$id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_INTERNAL_SERVER_ERROR,"error"=>"Internal Server Error");
        $this->set_response($response,REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  



    }
    public function comment_delete($id){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($result=$this->M_podcast->deleteComment($id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data");
        $this->set_response($response,REST_Controller::HTTP_OK);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  

    }
    public function commentdetail_get($id){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
               $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
   
               //return $decodedToken;
       if ($decodedToken != false) {
       $xp = explode("_",$decodedToken);
   if($xp[3]==$headers['Xkey']){
   
 
   
       if($result=$this->M_podcast->detailComment($id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Accepted","response"=>$result);
    $this->set_response($response,REST_Controller::HTTP_OK);  
    
    }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"no data");
        $this->set_response($response,REST_Controller::HTTP_OK);
       }
     

   
       
   
   }else{
    $response = array(
        "status"=>REST_Controller::HTTP_UNAUTHORIZED,
        "error"=>"Invalid Token Authorization",
    );
   $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
               
           }else{
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"Invalid Token Authorization",
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
   
       }else{
   
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"No Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
       }
       return;	  

    }

    
}