<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=artikel_viewer_".date("Ymd").".xls");
?>
<?php
$this->load->model("admin/M_monitoring");
?>

<table>
<thead>
    <tr><th>No.</th>
        <th>Judul Artikel</th>
        <?php foreach($tgl as $tg){
           
            ?>
            <th><?php echo $tg->view_tanggal;?></th>
        
            <?php }?>
       
    </tr>
    <?php
    $i=1;
    foreach($artikel as $judul){
        $idcontent = $judul->idcontent;
        ?>
    <tr><td><?php echo $i;?></td>
        <td>
<?php echo $judul->judul_artikel;?>
        </td>

        <?php foreach($tgl as $tlist){
            $tgindeks = $tlist->tgl_indeks;
            
            ?>

        <td><?php 
        $mdata = $this->M_monitoring->getListViewArtikel($tlist->view_tanggal);
        foreach($mdata as $art){
            if($art->view_artikel_id==$judul->idcontent){
                echo $art->jumlah_view;
            }
        }
       ?></td>
            <?php }?>
    </tr>
    <?php $i++;} ?>
</thead>

</table>